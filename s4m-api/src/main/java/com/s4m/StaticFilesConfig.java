package com.s4m;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class StaticFilesConfig extends WebMvcConfigurerAdapter {
    @Value("${app.storage.directory}")
    private String staticFilesPath;

    @Value("${app.storage.baseUrl}")
    private String staticFilesBaseUrl;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if (!staticFilesPath.endsWith("/")) staticFilesPath += "/";
        if (!staticFilesBaseUrl.endsWith("/")) staticFilesBaseUrl += "/";

        String pathPattern = staticFilesBaseUrl + "**";
        registry.addResourceHandler(pathPattern).addResourceLocations("file:" + staticFilesPath);
    }
}
