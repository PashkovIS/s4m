package com.s4m.Common;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Entity already exists")
public class EntityAlreadyExistsException extends RuntimeException {
}
