package com.s4m;

import com.s4m.Entities.Device.Device;
import com.s4m.Entities.User.User;
import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "request")
@Data
public class RequestContext implements Serializable {
    private Device device;
    private User user;
    private String userSessionId;

    public void clear() {
        this.device = null;
        this.user = null;
        this.userSessionId = null;
    }
}
