package com.s4m;

import com.s4m.API.WebApi.DTO.EventAttendeeCreateRequest;
import com.s4m.API.WebApi.DTO.EventCreateRequest;
import com.s4m.Background.ExchangeIntegration;
import com.s4m.Background.ExchangeTask;
import com.s4m.Entities.Event.Event;
import com.s4m.Entities.Event.EventService;
import com.s4m.Entities.EventAttendee.EventAttendee;
import com.s4m.Entities.Location.Location;
import com.s4m.Entities.Location.LocationRepository;
import com.s4m.Entities.Organization.Organization;
import com.s4m.Entities.Organization.OrganizationRepository;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.Space.SpaceRepository;
import com.s4m.Entities.User.*;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.misc.ConnectingIdType;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.service.item.Appointment;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.core.service.schema.AppointmentSchema;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.misc.ImpersonatedUserId;
import microsoft.exchange.webservices.data.property.complex.AttendeeCollection;
import microsoft.exchange.webservices.data.property.complex.EmailAddress;
import microsoft.exchange.webservices.data.property.complex.FolderId;
import microsoft.exchange.webservices.data.property.complex.ItemId;
import microsoft.exchange.webservices.data.search.CalendarView;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.EntityManagerFactory;
import java.net.URI;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

@SpringBootApplication
@EntityScan
        (
                basePackages = {"com.s4m"},
                basePackageClasses = {Jsr310JpaConverters.class}
        )
public class Application {
    private static boolean __scheduler = false;

    public static boolean scheduler() {
        return __scheduler;
    }

    private static boolean __webApi = false;

    public static boolean webApi() {
        return __webApi;
    }

    private static boolean __deviceApi = false;

    public static boolean deviceApi() {
        return __deviceApi;
    }

    public static void main(String[] args) {
        boolean initializer = false;

        // Naive implementation of application role separation.
        // TODO: Find a way to use command line parameters in ConditionContext.

        for (String arg : args) {
            switch (arg) {
                case "-master":
                case "-initializer":
                    initializer = true;
                    break;

                case "-background":
                case "-scheduler":
                    __scheduler = true;
                    break;

                case "-web-api":
                    __webApi = true;
                    break;

                case "-device-api":
                    __deviceApi = true;
                    break;
            }
        }

        if (!__scheduler && !__webApi && !__deviceApi) {
            // Handle all roles and initialize database

            __scheduler = true;
            __webApi = true;
            __deviceApi = true;

            initializer = true;
        }

        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);

        if (initializer) {

            // TODO: Place this configuraton in a proper location. Where do everything get configured?

            EntityManagerFactory emf = context.getBean(EntityManagerFactory.class);

            String dialect = emf.getProperties().get("hibernate.dialect").toString();

            if (dialect != null)
            {
                if (dialect.equals("PostgresPlusDialect") || dialect.equals("PostgreSQLDialect"))
                {
                    System.out.println("");
                    System.out.println("");
                    System.out.println("");
                    System.out.println("       I am working with PostgreSQL!");
                    System.out.println("");
                    System.out.println("");
                    System.out.println("");

                    // TODO: Add implementation
                    /*
                    EntityManager em = emf.createEntityManager();

                    em.getTransaction().begin();

                    try
                    {
                        List<String> fields = new List<String>();
                        List<Object> objects = em.createNativeQuery("SELECT column_name FROM information_schema.columns WHERE table_name = 'event' ORDER BY ordinal_position").getResultList();

                        for (Object object: objects)
                        {
                            fields.add(object.toString());
                        }

                        String merged = String.join(", ", fields);

                        em.createNativeQuery("CREATE TABLE EVENT_SAFE AS SELECT * FROM EVENT").getResultList();
                        em.createNativeQuery("ALTER TABLE EVENT_SAFE ADD COLUMN tsrange NOT NULL").getResultList();
                        em.createNativeQuery("CREATE RULE \"_RETURN\" AS ON SELECT TO EVENT DO INSTEAD SELECT  FROM BBB;").getResultList();
                        em.createNativeQuery("").getResultList();
                        em.createNativeQuery("").getResultList();
                        em.createNativeQuery("").getResultList();
                    }
                    finally {
                        em.getTransaction().commit();
                    }
                    */
                }
            }

            String adminLogin = UserService.ADMINISTRATOR;
            String defaultAdminPassword = "P@ssw0rd";
            String defaultAdminName = "Administrator";
            String defaultAdminEmail = "booking@smart4meeting.ru";

            UserRepository userRepository = context.getBean(UserRepository.class);

            ExchangeIntegration exchange = context.getBean(ExchangeIntegration.class);

            if (userRepository.findByLogin(adminLogin).isEmpty()) {
                UserService userService = context.getBean(UserService.class);

                byte[] salt = userService.generateSalt();
                byte[] hash = userService.hashPassword(defaultAdminPassword, salt);

                String hashedPassword = userService.bytesToHex(hash);
                String strSalt = userService.bytesToHex(salt);

                User admin = new User();
                admin.setFullName(defaultAdminName);
                admin.setLogin(adminLogin);
                admin.setEmail(defaultAdminEmail);
                admin.setSalt(strSalt);
                admin.setPasswordHash(hashedPassword);

                userRepository.save(admin);

                RoleService roleService = context.getBean(RoleService.class);

                roleService.setAdministrator(admin.getId());

                // TODO: Remove this before deploing to production.

                /*
                Organization organization = new Organization();
                organization.setName("Organization 1");

                context.getBean(OrganizationRepository.class).save(organization);

                {
                    Location location = new Location();
                    location.setOrganizationId(organization.getId());
                    location.setName("Location 1");
                    location.setTimezoneId("+01:00");
                    location.setServiceEmail("nikolay.a.pankov@gmail.com");

                    context.getBean(LocationRepository.class).save(location);

                    {
                        Space space = new Space();
                        space.setLocationId(location.getId());
                        space.setCapacity(18);
                        space.setName("alfa");
                        space.setSpaceTypeId("type1");
                        space.setExchangeIntegrationEmail("alfa@smart4meeting.ru");

                        context.getBean(SpaceRepository.class).save(space);

                        {
                            EventCreateRequest ecr = new EventCreateRequest();

                            ecr.setTitle("Event 1");
                            ecr.setDescription("Event 1 desc");
                            ecr.setStart(Instant.now().plus(8, ChronoUnit.HOURS));
                            ecr.setEnd(Instant.now().plus(8, ChronoUnit.HOURS).plus(1, ChronoUnit.MINUTES));
                            ecr.setOrganizerUserId(1l);
                            ecr.setGreeting("Hello");

                            EventAttendeeCreateRequest eacr = new EventAttendeeCreateRequest();
                            eacr.setUserId(admin.getId());
                            eacr.setFullName(admin.getFullName());
                            eacr.setEmail(admin.getEmail());

                            ecr.setAttendees(Arrays.asList(eacr));

                            EventService service = context.getBean(EventService.class);

                            Event event = service.create(admin, space.getId(), "portal", ecr);;
                        }
                    }

                    {
                        Space space = new Space();
                        space.setLocationId(location.getId());
                        space.setCapacity(4);
                        space.setName("Space 2 at 1");
                        space.setSpaceTypeId("type2");

                        context.getBean(SpaceRepository.class).save(space);
                    }

                    {
                        Space space = new Space();
                        space.setLocationId(location.getId());
                        space.setCapacity(100);
                        space.setName("Space 3 at 1");
                        space.setSpaceTypeId("type3");

                        context.getBean(SpaceRepository.class).save(space);
                    }
                }

                {
                    Location location = new Location();
                    location.setOrganizationId(organization.getId());
                    location.setName("Location 1");
                    location.setTimezoneId("+01:00");
                    location.setServiceEmail("nikolay.a.pankov@gmail.com");

                    context.getBean(LocationRepository.class).save(location);

                    {
                        Space space = new Space();
                        space.setLocationId(location.getId());
                        space.setCapacity(15);
                        space.setName("Space 1 at 2");
                        space.setSpaceTypeId("type1");

                        context.getBean(SpaceRepository.class).save(space);
                    }

                    {
                        Space space = new Space();
                        space.setLocationId(location.getId());
                        space.setCapacity(10);
                        space.setName("Space 2 at 2");
                        space.setSpaceTypeId("type2");

                        context.getBean(SpaceRepository.class).save(space);
                    }
                }
                */
            }

            ExchangeTask synchronizer = context.getBean(ExchangeTask.class);

            if (synchronizer != null) {
                synchronizer.start();
            }
        }
    }
}
