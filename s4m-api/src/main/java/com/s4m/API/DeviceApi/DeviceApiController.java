package com.s4m.API.DeviceApi;

import com.google.common.collect.Lists;
import com.s4m.API.DeviceApi.Auth.DeviceAuthException;
import com.s4m.API.DeviceApi.Auth.DeviceAuthForbiddenException;
import com.s4m.API.DeviceApi.Auth.DeviceAuthService;
import com.s4m.API.DeviceApi.DTO.*;
import com.s4m.Common.BadRequestException;
import com.s4m.Common.EntityNotFoundException;
import com.s4m.Common.NoAccessException;
import com.s4m.Entities.Device.Device;
import com.s4m.Entities.Device.DeviceRepository;
import com.s4m.Entities.DeviceActivation.DeviceActivationRequestResult;
import com.s4m.Entities.DeviceActivation.DeviceActivationService;
import com.s4m.Entities.Event.Event;
import com.s4m.Entities.Event.EventRepository;
import com.s4m.Entities.Event.EventService;
import com.s4m.Entities.EventAttendee.EventAttendee;
import com.s4m.Entities.EventAttendee.EventAttendeeRepository;
import com.s4m.Entities.EventResource.EventResource;
import com.s4m.Entities.Location.Location;
import com.s4m.Entities.Location.LocationRepository;
import com.s4m.Entities.Mail.MailService;
import com.s4m.Entities.Organization.Organization;
import com.s4m.Entities.Organization.OrganizationRepository;
import com.s4m.Entities.Resource.Resource;
import com.s4m.Entities.Resource.ResourceRepository;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.Space.SpaceRepository;
import com.s4m.Entities.SpaceSettings.SpaceSettings;
import com.s4m.Entities.SpaceSettings.SpaceSettingsRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.TemplateEngine;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Api(description = "API для устройства управления переговорной")
@RestController
@Conditional(DeviceApiActivator.class)
@RequestMapping("/device-api")
@Slf4j
public class DeviceApiController {
    @Autowired
    private DeviceActivationService deviceActivationService;

    @Autowired
    private DeviceAuthService deviceAuthService;

    @Autowired
    private SpaceSettingsRepository spaceSettingsRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceApiEventService deviceApiEventService;

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private EventAttendeeRepository eventAttendeeRepository;

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private MailService mailService;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private EventService eventService;

    @Value("${app.event.waitingForConfirmBeforeMinutes}")
    private int WAITING_FOR_CONFIRM_BEFORE_MINUTES = 15;

    @ApiOperation(value = "Получить статус", notes = "Получить текущий статус устройства и связанной переговорки. Статусы устройства: paired, waitForActivation. Статусы переговорной: free, waitingForConfirm, busy")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @GetMapping
    GetStatusResponse getStatus() {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) {
            String userSessionId = deviceAuthService.getUserSessionId();
            if (userSessionId == null || userSessionId.equals("")) {
                throw new DeviceAuthException();
            }

            GetStatusResponse getStatusResponse = new GetStatusResponse();
            getStatusResponse.setDeviceStatus("waitForActivation");
            return getStatusResponse;
        }

        Space space = spaceRepository.findOne(authenticatedDevice.getSpaceId());
        Location location = locationRepository.findOne(space.getLocationId());
        Organization organization = organizationRepository.findOne(location.getOrganizationId());

        GetStatusResponse getStatusResponse = new GetStatusResponse();
        getStatusResponse.setDeviceStatus("paired");
        getStatusResponse.setSpace(space);
        getStatusResponse.setLogoUrl(organization.getRoomDisplayLogoUrl());
        getStatusResponse.setTimezoneId(location.getTimezoneId());

        String serviceEmail = location.getServiceEmail();
        boolean canOrderResources = (serviceEmail != null && !serviceEmail.equals(""));
        getStatusResponse.setCanOrderResources(canOrderResources);

        Long spaceId = authenticatedDevice.getSpaceId();
        Event currentEvent = deviceApiEventService.getCurrent(spaceId);
        if (currentEvent != null) {
            String status = currentEvent.getStatus();
            if (status == null) status = "scheduled";
            if (status.equals("scheduled")) {
                getStatusResponse.setSpaceStatus("waitingForConfirm");
            } else {
                getStatusResponse.setSpaceStatus("busy");
            }

            if (currentEvent.isPrivateEvent()) {
                currentEvent.setTitle(null);
                currentEvent.setDescription(null);
                currentEvent.setOrganizerFullName(null);
                currentEvent.setOrganizerEmail(null);
                currentEvent.setOrganizerUserId(null);
                currentEvent.setCreatedByUserId(null);
            }
            getStatusResponse.setEvent(currentEvent);
        } else {
            Event nextEvent = deviceApiEventService.getNext(spaceId);
            if (nextEvent == null) {
                getStatusResponse.setSpaceStatus("free");
            } else {
                Instant maxStart = Instant.now().plus(WAITING_FOR_CONFIRM_BEFORE_MINUTES, ChronoUnit.MINUTES);
                if (nextEvent.getStart().isAfter(maxStart)) {
                    getStatusResponse.setSpaceStatus("free");
                } else {
                    getStatusResponse.setSpaceStatus("waitingForConfirm");

                    if (nextEvent.isPrivateEvent()) {
                        nextEvent.setTitle(null);
                        nextEvent.setDescription(null);
                        nextEvent.setOrganizerFullName(null);
                        nextEvent.setOrganizerEmail(null);
                        nextEvent.setOrganizerUserId(null);
                        nextEvent.setCreatedByUserId(null);
                    }
                    getStatusResponse.setEvent(nextEvent);
                }
            }
        }

        return getStatusResponse;
    }

    @ApiOperation(value = "Запрос на подключение устройства", notes = "Инициация связывания устройства с переговоркой")
    @PostMapping("/pair")
    StartPairingResponse startPairing() {
        DeviceActivationRequestResult deviceActivationRequestResult = deviceActivationService.requestActivation();

        StartPairingResponse startPairingResponse = new StartPairingResponse();
        startPairingResponse.setAuthToken(deviceActivationRequestResult.getAuthToken());
        startPairingResponse.setActivationPin(deviceActivationRequestResult.getActivationPin());
        return startPairingResponse;
    }

    @ApiOperation(value = "Отключить устройство от переговорки")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @PostMapping("/unpair")
    OpStatusResponse unpair() {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();

        Long deviceId = authenticatedDevice.getId();
        deviceRepository.delete(deviceId);

        OpStatusResponse opStatusResponse = new OpStatusResponse();
        opStatusResponse.setSuccess(true);
        return opStatusResponse;
    }

    @ApiOperation(value = "Начать встречу")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @PostMapping("/event/start")
    OpStatusResponse startNewEvent(@RequestBody StartNewEventRequest startNewEventRequest) {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();

        int duration = startNewEventRequest.getDurationInMinutes();
        if (duration < 1 || duration > 1440) throw new BadRequestException(); // Не более 1 суток

        boolean isSucceed = deviceApiEventService.start(authenticatedDevice.getSpaceId(), duration);

        OpStatusResponse opStatusResponse = new OpStatusResponse();
        opStatusResponse.setSuccess(isSucceed);
        return opStatusResponse;
    }

    @ApiOperation(value = "Закончить текущую встречу")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @PostMapping("/event/stop")
    OpStatusResponse stopCurrentEvent() {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();

        boolean isSucceed = deviceApiEventService.stop(authenticatedDevice.getSpaceId());

        OpStatusResponse opStatusResponse = new OpStatusResponse();
        opStatusResponse.setSuccess(isSucceed);
        return opStatusResponse;
    }

    @ApiOperation(value = "Подтвердить начало запланированной встречи")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @PostMapping("/event/confirm")
    OpStatusResponse confirmCurrentEvent() {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();

        boolean isSucceed = deviceApiEventService.confirm(authenticatedDevice.getSpaceId());

        OpStatusResponse opStatusResponse = new OpStatusResponse();
        opStatusResponse.setSuccess(isSucceed);
        return opStatusResponse;
    }

    @ApiOperation(value = "Продлить текущую встречу")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @PostMapping("/event/prolong")
    OpStatusResponse prolongCurrentEvent(@RequestBody ProlongCurrentEventRequest prolongCurrentEventRequest) {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();

        int duration = prolongCurrentEventRequest.getDurationInMinutes();
        if (duration < 1 || duration > 1440) throw new BadRequestException(); // Не более 1 суток

        boolean isSucceed = deviceApiEventService.prolong(authenticatedDevice.getSpaceId(), duration);

        OpStatusResponse opStatusResponse = new OpStatusResponse();
        opStatusResponse.setSuccess(isSucceed);
        return opStatusResponse;
    }

    @ApiOperation(value = "Сохранить ресурсы для встречи")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @PutMapping("/events/{itemId}/resources")
    private OpStatusResponse putEventResources(@PathVariable Long itemId, @RequestBody List<EventResource> eventResources) {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();

        Space space = spaceRepository.findOne(authenticatedDevice.getSpaceId());
        if (space == null) {
            throw new EntityNotFoundException();
        }

        Location location = locationRepository.findOne(space.getLocationId());
        if (location == null) {
            throw new EntityNotFoundException();
        }

        Event event = eventRepository.findOne(itemId);
        if (event == null) {
            throw new EntityNotFoundException();
        }

        if (event.getSpaceId() != space.getId()) {
            throw new NoAccessException();
        }

        event.setResources(eventResources);
        eventRepository.save(event);

        eventService.notifyOnResourceOrder(event, space, location, event.getResources());

        OpStatusResponse opStatusResponse = new OpStatusResponse();
        opStatusResponse.setSuccess(true);
        return opStatusResponse;
    }

    @ApiOperation(value = "Получить настройки")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @GetMapping("/settings")
    SpaceSettings getSettings() {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();

        Long spaceId = authenticatedDevice.getSpaceId();

        if (!spaceSettingsRepository.exists(spaceId)) {
            return new SpaceSettings();
        }

        SpaceSettings spaceSettings = spaceSettingsRepository.findOne(spaceId);

        return spaceSettings;
    }

    @ApiOperation(value = "Сохранить настройки")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @PutMapping("/settings")
    OpStatusResponse putSettings(@RequestBody PutSettingsRequest putSettingsRequest) {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();

        Long spaceId = authenticatedDevice.getSpaceId();
        SpaceSettings spaceSettings;
        if (!spaceSettingsRepository.exists(spaceId)) {
            spaceSettings = new SpaceSettings();
            spaceSettings.setSpaceId(spaceId);
        } else {
            spaceSettings = spaceSettingsRepository.findOne(spaceId);
        }

        if (spaceSettings.isAdminOnly()) throw new DeviceAuthForbiddenException();

        spaceSettings.setAutoUnbook(putSettingsRequest.isAutoUnbook());
        spaceSettings.setMaskMeetingTitles(putSettingsRequest.isMaskMeetingTitles());
        spaceSettings.setHideMeetingAttendees(putSettingsRequest.isHideMeetingAttendees());
        spaceSettings.setKeepMeetingOrganizerVisible(putSettingsRequest.isKeepMeetingOrganizerVisible());
        spaceSettings.setDarkenBackgroundImage(putSettingsRequest.isDarkenBackgroundImage());
        spaceSettings.setUseLightText(putSettingsRequest.isUseLightText());
        spaceSettings.setHighVisibilityMode(putSettingsRequest.isHighVisibilityMode());
        spaceSettings.setShowFrame(putSettingsRequest.isShowFrame());
        spaceSettings.setShowTimeline(putSettingsRequest.isShowTimeline());
        spaceSettings.setTransparentTopBar(putSettingsRequest.isTransparentTopBar());

        String statusBarColor = putSettingsRequest.getStatusBarColor();
        if (statusBarColor == null || statusBarColor.equals("")) {
            statusBarColor = "black";
        } else {
            statusBarColor = statusBarColor.toLowerCase();

            if (!statusBarColor.equals("black") && !statusBarColor.equals("white")) {
                throw new BadRequestException();
            }
        }
        spaceSettings.setStatusBarColor(statusBarColor);

        spaceSettingsRepository.save(spaceSettings);
        OpStatusResponse opStatusResponse = new OpStatusResponse();
        opStatusResponse.setSuccess(true);
        return opStatusResponse;
    }

    @ApiOperation(value = "Получить список встреч за сутки")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @GetMapping("/events")
    List<Event> getEvents() {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();

        Long spaceId = authenticatedDevice.getSpaceId();

        List<Event> events = deviceApiEventService.getTodayEvents(spaceId);

        if (events == null) {
            return Arrays.asList(new Event[]{});
        }

        for (Event event : events) {
            if (event.isPrivateEvent()) {
                event.setTitle(null);
                event.setDescription(null);
                event.setOrganizerUserId(null);
                event.setOrganizerEmail(null);
                event.setOrganizerFullName(null);
                event.setCreatedByUserId(null);
            }
        }

        return events;
    }

    @ApiOperation(value = "Получить информацию по встрече")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @GetMapping("/events/{itemId}")
    Event getEvent(@PathVariable Long itemId) {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();

        Long spaceId = authenticatedDevice.getSpaceId();

        if (!eventRepository.exists(itemId)) throw new EntityNotFoundException();
        Event event = eventRepository.findOne(itemId);
        if (event.getSpaceId() != spaceId) throw new NoAccessException();

        if (event.isPrivateEvent()) {
            event.setTitle(null);
            event.setDescription(null);
            event.setOrganizerUserId(null);
            event.setOrganizerEmail(null);
            event.setOrganizerFullName(null);
            event.setCreatedByUserId(null);
        }

        return event;
    }

    @ApiOperation(value = "Получить информацию по участникам встречи")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @GetMapping("/events/{itemId}/attendees")
    List<EventAttendee> getEventAttendees(@PathVariable Long itemId) {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();

        Long spaceId = authenticatedDevice.getSpaceId();

        if (!eventRepository.exists(itemId)) throw new EntityNotFoundException();
        Event event = eventRepository.findOne(itemId);
        if (event.getSpaceId() != spaceId) throw new NoAccessException();

        if (event.isPrivateEvent()) {
            return Arrays.asList(new EventAttendee[]{});
        } else {
            return eventAttendeeRepository.findByEventId(itemId);
        }
    }

    @ApiOperation(value = "Получить список переговорок в текущем местоположении")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @GetMapping("/spaces")
    List<SpaceStatus> getSpaces() {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();

        Long currentSpaceId = authenticatedDevice.getSpaceId();
        Space currentSpace = spaceRepository.findOne(currentSpaceId);
        Long currentLocationId = currentSpace.getLocationId();

        List<Space> spaces = spaceRepository.findByLocationId(currentLocationId);
        List<SpaceStatus> spaceStatuses = new ArrayList<>();

        spaces.forEach(space -> {
            Long spaceId = space.getId();
            SpaceStatus spaceStatus = new SpaceStatus();
            spaceStatus.setSpace(space);

            Event currentEvent = deviceApiEventService.getCurrent(spaceId);
            if (currentEvent == null) {
                Event nextEvent = deviceApiEventService.getNext(spaceId);
                spaceStatus.setFree(true);
                if (nextEvent != null) {
                    spaceStatus.setUntil(nextEvent.getStart());
                }
            } else {
                spaceStatus.setFree(false);
                Instant until = deviceApiEventService.getBusyUntill(spaceId);
                spaceStatus.setUntil(until);
            }

            spaceStatuses.add(spaceStatus);
        });

        return spaceStatuses;
    }

    @ApiOperation(value = "Получить список ресурсов (каталог)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "X-Auth", value = "Authorization token", dataType = "string", paramType = "header")
    })
    @GetMapping("/resources")
    List<Resource> getResources() {
        Device authenticatedDevice = deviceAuthService.getAuthenticatedDevice();
        if (authenticatedDevice == null) throw new DeviceAuthException();
        return Lists.newArrayList(resourceRepository.findAll());
    }
}
