package com.s4m.API.DeviceApi.DTO;

import lombok.Data;

@Data
public class ProlongCurrentEventRequest {
    private int durationInMinutes;
}
