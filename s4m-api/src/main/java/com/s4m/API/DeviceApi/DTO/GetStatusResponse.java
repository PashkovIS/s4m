package com.s4m.API.DeviceApi.DTO;

import com.s4m.Entities.Event.Event;
import com.s4m.Entities.Location.Location;
import com.s4m.Entities.Organization.Organization;
import com.s4m.Entities.Space.Space;
import lombok.Data;

@Data
public class GetStatusResponse {
    private String deviceStatus;
    private String spaceStatus;
    private Event event;
    private Space space;
    private String logoUrl;
    private String timezoneId;
    private boolean canOrderResources;
}
