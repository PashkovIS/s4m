package com.s4m.API.DeviceApi.DTO;

import lombok.Data;

@Data
public class StartPairingResponse {
    private String authToken;
    private String activationPin;
}
