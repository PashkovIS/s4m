package com.s4m.API.DeviceApi.DTO;

import com.s4m.Entities.Space.Space;
import lombok.Data;

import java.time.Instant;

@Data
public class SpaceStatus {
    private boolean isFree;
    private Instant until;
    private Space space;
}
