package com.s4m.API.DeviceApi.DTO;

import lombok.Data;

@Data
public class OpStatusResponse {
    private boolean success;
}
