package com.s4m.API.DeviceApi.DTO;

import lombok.Data;

@Data
public class StartNewEventRequest {
    private int durationInMinutes;
}
