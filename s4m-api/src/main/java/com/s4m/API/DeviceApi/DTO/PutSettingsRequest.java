package com.s4m.API.DeviceApi.DTO;

import lombok.Data;

@Data
public class PutSettingsRequest {
    private boolean autoUnbook;
    private boolean maskMeetingTitles;
    private boolean hideMeetingAttendees;
    private boolean keepMeetingOrganizerVisible;
    private boolean darkenBackgroundImage;
    private boolean useLightText;
    private boolean highVisibilityMode;
    private boolean showFrame;
    private boolean showTimeline;
    private String statusBarColor;
    private boolean transparentTopBar;
}
