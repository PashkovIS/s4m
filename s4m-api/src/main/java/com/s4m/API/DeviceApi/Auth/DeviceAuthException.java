package com.s4m.API.DeviceApi.Auth;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "Device not authorized")
public class DeviceAuthException extends RuntimeException {
}
