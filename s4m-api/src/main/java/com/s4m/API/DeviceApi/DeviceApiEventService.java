package com.s4m.API.DeviceApi;

import com.s4m.Background.ExchangeIntegration;
import com.s4m.Common.BadRequestException;
import com.s4m.Common.ConflictException;
import com.s4m.Entities.Event.Event;
import com.s4m.Entities.Event.EventRepository;
import com.s4m.Entities.Event.EventService;
import com.s4m.Entities.EventAttendee.EventAttendee;
import com.s4m.Entities.EventAttendee.EventAttendeeRepository;
import com.s4m.Entities.EventResource.EventResource;
import com.s4m.Entities.EventResource.EventResourceAttribute;
import com.s4m.Entities.Location.Location;
import com.s4m.Entities.Location.LocationRepository;
import com.s4m.Entities.Mail.Mail;
import com.s4m.Entities.Mail.MailService;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.Space.SpaceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class DeviceApiEventService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private EventAttendeeRepository eventAttendeeRepository;

    @Autowired
    private ExchangeIntegration exchangeIntegration;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private MailService mailService;

    public Event getCurrent(Long spaceId) {
        Instant now = Instant.now();
        List<Event> events = eventRepository.findCurrentBySpaceId(spaceId, Date.from(now));
        if (events.isEmpty()) return null;
        return events.get(0);
    }

    public Event getNext(Long spaceId) {
        Instant now = Instant.now();
        List<Event> events = eventRepository.findNextBySpaceId(spaceId, Date.from(now));
        if (events.isEmpty()) return null;
        return events.get(0);
    }

    public boolean start(Long spaceId, int durationInMinutes) {
        Instant start = Instant.now();
        Instant end = start.plus(durationInMinutes, ChronoUnit.MINUTES);

        Space space = spaceRepository.findOne(spaceId);
        if (space == null) {
            throw new BadRequestException();
        }

        Event newEvent = new Event();
        newEvent.setCreated(Instant.now());
        newEvent.setSpaceId(spaceId);
        newEvent.setSource("device");
        newEvent.setStart(start);
        newEvent.setStatus("started");
        newEvent.setEnd(end);

        if (eventService.hasConflict(newEvent)) {
            return false;
        }

        if (exchangeIntegration != null) {
            //if (!exchangeIntegration.updateAppointment(space, newEvent, new ArrayList<EventAttendee>(), true)) {
            if (!exchangeIntegration.createAppointment(space, newEvent, new ArrayList<EventAttendee>(), true)) {



                return false;
            }
        }

        eventRepository.save(newEvent);
        return true;
    }

    public boolean stop(Long spaceId) {
        Event currentEvent = getCurrent(spaceId);
        if (currentEvent == null) return false;

        currentEvent.setStatus("finished");
        currentEvent.setEnd(Instant.now());
        eventRepository.save(currentEvent);

        return true;
    }

    public boolean confirm(Long spaceId) {
        Event event = getCurrent(spaceId);
        if (event != null) {
            event.setStatus("started");
            eventRepository.save(event);
            return true;
        }

        event = getNext(spaceId);
        if (event == null) {
            return false;
        }

        Instant maxStart = Instant.now().plus(10, ChronoUnit.MINUTES);
        if (event.getStart().isAfter(maxStart)) return false;

        event.setStart(Instant.now());
        event.setStatus("started");
        eventRepository.save(event);

        return true;
    }

    public boolean prolong(Long spaceId, int durationInMinutes) {
        Event currentEvent = getCurrent(spaceId);
        if (currentEvent == null) return false;

        Space space = spaceRepository.findOne(spaceId);
        if (space == null) {
            throw new BadRequestException();
        }

        Instant saved = currentEvent.getEnd();
        Instant end = currentEvent.getEnd().plus(durationInMinutes, ChronoUnit.MINUTES);
        currentEvent.setEnd(end);

        List<EventAttendee> attendees = eventAttendeeRepository.findByEventId(currentEvent.getId());

        if (eventService.hasConflict(currentEvent)) {
            return false;
        }

        if (exchangeIntegration != null) {
            if (!exchangeIntegration.updateAppointment(space, currentEvent, attendees, true)) {

                // Rollback
                currentEvent.setEnd(saved);

                if (!exchangeIntegration.updateAppointment(space, currentEvent, attendees, true)) {

                    Location location = locationRepository.findOne(space.getLocationId());

                    Context context = new Context(Locale.forLanguageTag("ru"));
                    context.setVariable("locationName", location.getName());
                    context.setVariable("spaceName", space.getName());
                    context.setVariable("eventStart", Date.from(currentEvent.getStart().atZone(ZoneId.of(location.getTimezoneId())).toInstant())); // WTF?!
                    context.setVariable("eventTitle", currentEvent.getTitle());
                    context.setVariable("organizerName", currentEvent.getOrganizerFullName());
                    context.setVariable("organizerMail", currentEvent.getOrganizerEmail());

                    String body = templateEngine.process("conflict", context);

                    Mail mail = new Mail();
                    mail.setRecipient(location.getServiceEmail());
                    mail.setSubject("Конфликт при переносе встречи");
                    mail.setHtml(body);
                    mail.setTimestampCreated(Instant.now());

                    mailService.enqueue(mail);
                }

                return false;
            }

            try {
                eventRepository.save(currentEvent);
            } catch (Exception e) {
                // TODO: Suppress only UniqueConstraintvalitionException from database here.
                log.error("Exception thrown after ExchangeIntegration completed successfully", e);
            }
        } else {
            eventRepository.save(currentEvent);
        }

        return true;
    }

    public Instant getBusyUntill(Long spaceId) {
        Instant now = Instant.now();
        long count = 100L;
        int delta = 5;
        List<Event> events = eventRepository.findAllEndingAfterBySpaceId(spaceId, Date.from(now), count);

        // Ищем свободное время, больше чем delta минут
        Instant until = null;
        int num = 1;
        ListIterator<Event> iter = events.listIterator();
        if (iter.hasNext()) {
            Event e1 = null;
            Event e2 = iter.next();
            until = e2.getEnd();

            while (iter.hasNext()) {
                ++num;
                e1 = e2;
                e2 = iter.next();

                Instant dt = until.plus(delta, ChronoUnit.MINUTES);
                if (!e2.getStart().isAfter(dt)) {
                    until = e2.getEnd();
                } else {
                    break;
                }
            }

            // Если просмотрели count событий, считаем, что свободного времени не будет никогда
            if (num == count) until = null;
        }

        return until;
    }

    public List<Event> getTodayEvents(Long spaceId) {
        Space space = spaceRepository.findOne(spaceId);
        Long locationId = space.getLocationId();
        Location location = locationRepository.findOne(locationId);

        Instant from = Instant.now();

        String timezoneId = location.getTimezoneId();
        if (timezoneId != null && !timezoneId.equals("")) {
            ZoneId zoneId = ZoneId.of(timezoneId);
            ZoneOffset offset = zoneId.getRules().getOffset(from);
            from = LocalDateTime.ofInstant(from, zoneId).truncatedTo(ChronoUnit.DAYS).toInstant(offset);
        } else {
            from = from.truncatedTo(ChronoUnit.DAYS);
        }

        Instant to = from.plus(1, ChronoUnit.DAYS);

        return eventRepository.findFromToBySpaceId(spaceId, Date.from(from), Date.from(to));
    }
}
