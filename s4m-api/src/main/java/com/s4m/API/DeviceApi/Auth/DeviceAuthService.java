package com.s4m.API.DeviceApi.Auth;

import com.s4m.Common.EntityNotFoundException;
import com.s4m.Entities.Device.Device;
import com.s4m.Entities.Device.DeviceRepository;
import com.s4m.Entities.DeviceActivation.DeviceActivation;
import com.s4m.Entities.DeviceActivation.DeviceActivationRepository;
import com.s4m.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class DeviceAuthService {
    @Autowired
    private RequestContext requestContext;

    @Autowired
    private DeviceActivationRepository deviceActivationRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Value("${app.device.authHeaderName}")
    private String authHeaderName;

    @Value("${app.device.activationTimeoutMinutes}")
    private int ACTIVATION_REQUEST_TIMEOUT_MINUTES;

    public void authenticateRequest(HttpServletRequest request) {
        String authToken = request.getHeader(authHeaderName);
        if (authToken == null || authToken.equals("")) {
            requestContext.clear();
            return;
        }

        List<Device> devices = deviceRepository.findByAuthToken(authToken);
        if (devices.isEmpty()) {
            requestContext.clear();
            List<DeviceActivation> deviceActivations = deviceActivationRepository.findByToken(authToken);
            if (!deviceActivations.isEmpty()) {
                DeviceActivation deviceActivation = deviceActivations.get(0);
                if (deviceActivation.getCreated().plus(ACTIVATION_REQUEST_TIMEOUT_MINUTES, ChronoUnit.MINUTES).isBefore(Instant.now())) {
                    deviceActivation.setStatus("expired");
                    deviceActivationRepository.save(deviceActivation);
                } else {
                    requestContext.setUserSessionId(authToken);
                }
            }
        } else {
            Device authenticatedDevice = devices.get(0);
            authenticatedDevice.setLastAccessed(Instant.now());
            deviceRepository.save(authenticatedDevice);
            requestContext.setDevice(authenticatedDevice);
        }
    }

    public Device getAuthenticatedDevice() {
        return requestContext.getDevice();
    }

    public String getUserSessionId() {
        return requestContext.getUserSessionId();
    }
}
