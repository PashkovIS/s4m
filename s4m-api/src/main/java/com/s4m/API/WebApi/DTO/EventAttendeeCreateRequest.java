package com.s4m.API.WebApi.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.*;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventAttendeeCreateRequest {
    @Column
    private String fullName;

    @Column
    private String email;

    @Column
    private Long userId;
}
