package com.s4m.API.WebApi.Auth;

import com.s4m.Common.BadRequestException;
import com.s4m.Entities.User.User;
import com.s4m.Entities.User.UserRepository;
import com.s4m.Entities.User.UserService;
import com.s4m.Entities.UserSession.UserSession;
import com.s4m.Entities.UserSession.UserSessionRepository;
import com.s4m.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
public class UserAuthService {
    @Autowired
    private RequestContext requestContext;

    @Autowired
    private UserSessionRepository userSessionRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Value("${app.user.authCookieName}")
    private String authCookieName;

    @Value("${app.user.sessionTimeout}")
    private int timeoutInSeconds;

    public void authenticateRequest(HttpServletRequest request) {
        if (request.getCookies() == null) {
            requestContext.clear();
            return;
        }

        Optional<Cookie> cookie = Arrays.stream(request.getCookies())
                .filter(x -> x.getName().equals(authCookieName))
                .findFirst();
        String sessionId = cookie.isPresent() ? cookie.get().getValue() : null;
        if (sessionId == null || sessionId.equals("")) {
            requestContext.clear();
            return;
        }

        UserSession userSession = userSessionRepository.findOne(sessionId);
        if (userSession == null) {
            requestContext.clear();
            return;
        }

        Instant now = Instant.now();
        Instant expirationDate = userSession.getLastAccessed().plus(timeoutInSeconds, ChronoUnit.SECONDS);

        if (expirationDate.isBefore(now)) {
            userSessionRepository.delete(sessionId);
            requestContext.clear();
            return;
        }

        Long userId = userSession.getUserId();
        if (!userRepository.exists(userId)) {
            userSessionRepository.delete(sessionId);
            requestContext.clear();
            return;
        }

        userSession.setLastAccessed(Instant.now());
        userSessionRepository.save(userSession);

        User authenticatedUser = userRepository.findOne(userId);
        requestContext.setUser(authenticatedUser);
        requestContext.setUserSessionId(sessionId);
    }

    public User getAuthenticatedUser() {
        return requestContext.getUser();
    }

    public User login(String login, String password, HttpServletResponse response) {
        if (password == null || password.equals("")) throw new BadRequestException();

        List<User> users = userRepository.findByLogin(login);
        if (users.isEmpty()) return null;
        User user = users.get(0);

        byte[] currentSalt = userService.hexToBytes(user.getSalt());
        String currentPasswordHash = user.getPasswordHash();

        byte[] hash = userService.hashPassword(password, currentSalt);
        String passwordHash = userService.bytesToHex(hash);

        if (!currentPasswordHash.equals(passwordHash)) return null;

        String sessionId = UUID.randomUUID().toString();
        UserSession userSession = new UserSession();
        userSession.setSessionId(sessionId);
        userSession.setCreated(Instant.now());
        userSession.setLastAccessed(Instant.now());
        userSession.setUserId(user.getId());
        userSessionRepository.save(userSession);

        Cookie authCookie = new Cookie(authCookieName, sessionId);
        authCookie.setHttpOnly(true);
        authCookie.setPath("/");
        response.addCookie(authCookie);

        return user;
    }

    public void logout(HttpServletResponse response) {
        String sessionId = requestContext.getUserSessionId();
        if (sessionId != null) {
            if (userSessionRepository.exists(sessionId)) {
                userSessionRepository.delete(sessionId);
            }

            Cookie authCookie = new Cookie(authCookieName, null);
            authCookie.setHttpOnly(true);
            authCookie.setPath("/");
            authCookie.setMaxAge(0);
            response.addCookie(authCookie);
        }

        requestContext.clear();
    }
}
