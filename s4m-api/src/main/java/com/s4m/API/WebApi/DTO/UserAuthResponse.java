package com.s4m.API.WebApi.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.s4m.Entities.User.Role;
import com.s4m.Entities.User.User;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserAuthResponse {
    private boolean isAuthorized;
    private User user;
    private List<Role> roles;
}
