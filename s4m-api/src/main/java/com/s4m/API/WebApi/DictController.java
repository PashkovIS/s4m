package com.s4m.API.WebApi;

import com.s4m.API.WebApi.Auth.UserAuthException;
import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.Entities.Timezone.Timezone;
import com.s4m.Entities.Timezone.TimezoneService;
import com.s4m.Entities.User.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(description = "Справочники")
@RestController
@Conditional(WebApiActivator.class)
@RequestMapping("/web-api/dict")
@Slf4j
public class DictController {
    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private TimezoneService timezoneService;

    @ApiOperation(value = "Получить список временных зон")
    @GetMapping("/timezones")
    private List<Timezone> getTimezones() {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        return timezoneService.getAllZones();
    }
}
