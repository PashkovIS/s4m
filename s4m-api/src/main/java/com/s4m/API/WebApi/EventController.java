package com.s4m.API.WebApi;

import com.s4m.API.WebApi.Auth.UserAuthException;
import com.s4m.API.WebApi.Auth.UserAuthForbiddenException;
import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.API.WebApi.DTO.EventUpdateRequest;
import com.s4m.Background.ExchangeIntegration;
import com.s4m.Common.BadRequestException;
import com.s4m.Common.EntityNotFoundException;
import com.s4m.Entities.Event.Event;
import com.s4m.Entities.Event.EventRepository;
import com.s4m.Entities.Event.EventService;
import com.s4m.Entities.EventAttendee.EventAttendee;
import com.s4m.Entities.EventAttendee.EventAttendeeRepository;
import com.s4m.Entities.EventResource.EventResource;
import com.s4m.Entities.Location.Location;
import com.s4m.Entities.Location.LocationRepository;
import com.s4m.Entities.Mail.MailService;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.Space.SpaceRepository;
import com.s4m.Entities.User.RoleService;
import com.s4m.Entities.User.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.TemplateEngine;

import java.sql.Date;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

@Api(description = "Встречи")
@RestController
@Conditional(WebApiActivator.class)
@RequestMapping("/web-api/events")
@Slf4j
public class EventController {
    @Autowired
    private EventService eventService;

    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventAttendeeRepository eventAttendeeRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private MailService mailService;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private ExchangeIntegration exchangeIntegration;

    @ApiOperation(value = "")
    @GetMapping
    private List<Event> search(@RequestParam(required = true) String email) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        List<Event> found = null;

        if (email != null) {
            found = eventRepository.findCurrentByAttendeeEmail(email, Date.from(Instant.now()));
        }

        if (found == null) {
            found = Arrays.asList(new Event[]{});
        }

        return eventService.setEventFieldAccessForUser(authenticatedUser.getId(), found);
    }

    @ApiOperation(value = "Получить информацию по встрече")
    @GetMapping("/{itemId}")
    private Event get(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!eventRepository.exists(itemId)) throw new EntityNotFoundException();
        Event event = eventRepository.findOne(itemId);
        return eventService.setEventFieldAccessForUser(authenticatedUser.getId(), event);
    }

    @ApiOperation(value = "Обновить информацию по встрече")
    @PutMapping("/{itemId}")
    private Event update(@PathVariable Long itemId, @RequestBody EventUpdateRequest item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        Event event = eventRepository.findOne(itemId);
        if (event == null) {
            throw new EntityNotFoundException();
        }

        if (!eventService.canEdit(authenticatedUser.getId(), event)) {
            throw new UserAuthForbiddenException();
        }

        Event editedEvent = eventService.update(itemId, item);
        return eventService.setEventFieldAccessForUser(authenticatedUser.getId(), editedEvent);
    }

    @ApiOperation(value = "Удалить встречу")
    @DeleteMapping("/{itemId}")
    private void delete(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        Event event = eventRepository.findOne(itemId);
        if (event == null) {
            throw new EntityNotFoundException();
        }

        if (!eventService.canEdit(authenticatedUser.getId(), event)) {
            throw new UserAuthForbiddenException();
        }

        eventRepository.delete(itemId);

        if (exchangeIntegration != null) {
            exchangeIntegration.cancelAppointment(spaceRepository.findOne(event.getSpaceId()), event, true);
        }
    }

    @ApiOperation(value = "Получить список участников встречи")
    @GetMapping("/{itemId}/attendees")
    private List<EventAttendee> getAttendees(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        Event event = eventRepository.findOne(itemId);
        if (event == null) throw new EntityNotFoundException();

        if (event.isPrivateEvent() && !eventService.canEdit(authenticatedUser.getId(), event)) {
            return Arrays.asList(new EventAttendee[]{});
        }

        return eventAttendeeRepository.findByEventId(itemId);
    }

    @ApiOperation(value = "Добавить участника встречи")
    @PostMapping("/{itemId}/attendees")
    private EventAttendee createAttendees(@PathVariable Long itemId, @RequestBody EventAttendee item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        Event event = eventRepository.findOne(itemId);
        if (event == null) {
            throw new EntityNotFoundException();
        }

        if (!eventService.canEdit(authenticatedUser.getId(), event)) {
            throw new UserAuthForbiddenException();
        }

        item.setEventId(itemId);

        if (exchangeIntegration != null) {
            exchangeIntegration.addAttendee(spaceRepository.findOne(event.getSpaceId()), event, item, true);
        }

        return eventAttendeeRepository.save(item);
    }

    @ApiOperation(value = "Удалить участника встречи")
    @PostMapping("/{eventId}/attendees/{itemId}")
    private void removeAttendees(@PathVariable Long eventId, @PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        EventAttendee attendee = eventAttendeeRepository.findOne(itemId);
        if (attendee == null || attendee.getEventId() != eventId) {
            throw new EntityNotFoundException();
        }

        Event event = eventRepository.findOne(itemId);
        if (event == null) {
            throw new EntityNotFoundException();
        }

        if (!eventService.canEdit(authenticatedUser.getId(), event)) {
            // TODO: Can attendee remove himself?
            if (attendee.getId() != authenticatedUser.getId()) {
                throw new UserAuthForbiddenException();
            }
        }

        if (exchangeIntegration != null) {
            exchangeIntegration.removeAttendee(spaceRepository.findOne(event.getSpaceId()), event, attendee, true);
        }

        eventAttendeeRepository.delete(itemId);
    }

    @ApiOperation(value = "Сохранить ресурсы для встречи")
    @PutMapping("/{eventId}/resources")
    private void putEventResources(@PathVariable Long eventId, @RequestBody List<EventResource> eventResources) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        Event event = eventRepository.findOne(eventId);
        if (event == null) {
            throw new EntityNotFoundException();
        }

        if (!event.getStatus().equals("scheduled") && !event.getStatus().equals("started")) {
            throw new BadRequestException();
        }

        Space space = spaceRepository.findOne(event.getSpaceId());
        Location location = locationRepository.findOne(space.getLocationId());

        event.setResources(eventResources);
        eventRepository.save(event);

        eventService.notifyOnResourceOrder(event, space, location, eventResources);
    }
}
