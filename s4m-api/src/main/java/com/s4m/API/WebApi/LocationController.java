package com.s4m.API.WebApi;

import com.google.common.collect.Lists;
import com.s4m.API.WebApi.Auth.UserAuthException;
import com.s4m.API.WebApi.Auth.UserAuthForbiddenException;
import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.Common.EntityNotFoundException;
import com.s4m.Entities.Event.Event;
import com.s4m.Entities.Event.EventRepository;
import com.s4m.Entities.Event.EventService;
import com.s4m.Entities.Location.Location;
import com.s4m.Entities.Location.LocationRepository;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.Space.SpaceRepository;
import com.s4m.Entities.User.RoleService;
import com.s4m.Entities.User.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "Местоположения")
@RestController
@Conditional(WebApiActivator.class)
@RequestMapping("/web-api/locations")
@Slf4j
public class LocationController {
    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private LocationRepository repository;

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private EventService eventService;

    @ApiOperation(value = "Получить список местоположений")
    @GetMapping
    List<Location> getAll() {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        return Lists.newArrayList(repository.findAll());
    }

    @ApiOperation(value = "Получить информацию о местоположении")
    @GetMapping("/{itemId}")
    private Location get(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!repository.exists(itemId)) throw new EntityNotFoundException();
        return repository.findOne(itemId);
    }

    @ApiOperation(value = "Обновить информацию о местоположении")
    @PutMapping("/{itemId}")
    private Location update(@PathVariable Long itemId, @RequestBody Location item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!repository.exists(itemId)) throw new EntityNotFoundException();
        item.setId(itemId);
        return repository.save(item);
    }

    @ApiOperation(value = "Удалить местоположение")
    @DeleteMapping("/{itemId}")
    private void delete(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!repository.exists(itemId)) throw new EntityNotFoundException();
        repository.delete(itemId);
    }

    @ApiOperation(value = "Получить список переговорных в местоположении")
    @GetMapping("/{itemId}/spaces")
    private List<Space> getSpaces(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        return Lists.newArrayList(spaceRepository.findByLocationId(itemId));
    }

    @ApiOperation(value = "Создать новую переговорную для местоположения")
    @PostMapping("/{itemId}/spaces")
    private Space createSpace(@PathVariable Long itemId, @RequestBody Space item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        item.setLocationId(itemId);
        return spaceRepository.save(item);
    }

    @ApiOperation(value = "Получить список встреч для местоположения")
    @GetMapping("/{itemId}/events")
    private List<Event> getEvents(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        List<Event> events = Lists.newArrayList(eventRepository.findByLocationId(itemId));
        return eventService.setEventFieldAccessForUser(authenticatedUser.getId(), events);
    }
}
