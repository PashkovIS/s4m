package com.s4m.API.WebApi;

import com.google.common.collect.Lists;
import com.s4m.API.WebApi.Auth.UserAuthException;
import com.s4m.API.WebApi.Auth.UserAuthForbiddenException;
import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.Common.BadRequestException;
import com.s4m.Common.EntityNotFoundException;
import com.s4m.Common.StorageException;
import com.s4m.Entities.Event.Event;
import com.s4m.Entities.Event.EventRepository;
import com.s4m.Entities.Event.EventService;
import com.s4m.Entities.Image.Image;
import com.s4m.Entities.Image.ImageService;
import com.s4m.Entities.Location.Location;
import com.s4m.Entities.Location.LocationRepository;
import com.s4m.Entities.Organization.Organization;
import com.s4m.Entities.Organization.OrganizationRepository;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.Space.SpaceRepository;
import com.s4m.Entities.User.RoleService;
import com.s4m.Entities.User.User;
import ecstaj.ArgumentInvalidException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Api(description = "Организации")
@RestController
@Conditional(WebApiActivator.class)
@RequestMapping("/web-api/organizations")
@Slf4j
public class OrganizationController {
    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private OrganizationRepository repository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private ImageService imageService;

    @ApiOperation(value = "Получить список организаций")
    @GetMapping
    List<Organization> search() {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        return Lists.newArrayList(repository.findAll());
    }

    @ApiOperation(value = "Получить информацию по организации")
    @GetMapping("/{itemId}")
    Organization get(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!repository.exists(itemId)) throw new EntityNotFoundException();
        return repository.findOne(itemId);
    }

    @ApiOperation(value = "Создать организацию")
    @PostMapping
    Organization create(@RequestBody Organization item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        return repository.save(item);
    }

    @ApiOperation(value = "Обновить информацию по организации")
    @PutMapping("/{itemId}")
    Organization update(@PathVariable Long itemId, @RequestBody Organization item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!repository.exists(itemId)) throw new EntityNotFoundException();
        item.setId(itemId);
        return repository.save(item);
    }

    @ApiOperation(value = "Удалить организацию")
    @DeleteMapping("/{itemId}")
    void delete(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!repository.exists(itemId)) throw new EntityNotFoundException();
        repository.delete(itemId);
    }

    @ApiOperation(value = "Получить список местоположений по организации")
    @GetMapping("/{itemId}/locations")
    List<Location> getLocations(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        return Lists.newArrayList(locationRepository.findByOrganizationId(itemId));
    }

    @ApiOperation(value = "Создать новое местоположение для организации")
    @PostMapping("/{itemId}/locations")
    Location createLocation(@PathVariable Long itemId, @RequestBody Location item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        try {
            ZoneId zoneId = ZoneId.of(item.getTimezoneId());
        } catch (Exception e) {
            throw new BadRequestException();
        }

        item.setOrganizationId(itemId);
        return locationRepository.save(item);
    }

    @ApiOperation(value = "Получить список встреч для организации")
    @GetMapping("/{itemId}/events")
    List<Event> getEvents(@PathVariable Long itemId, @RequestParam Instant dateFrom, @RequestParam Instant dateTo) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        List<Event> events = Lists.newArrayList(eventRepository.findFromToByOrganizationId(itemId, Date.from(dateFrom), Date.from(dateTo)));
        return eventService.setEventFieldAccessForUser(authenticatedUser.getId(), events);
    }

    @ApiOperation(value = "Получить список своих встреч для организации")
    @GetMapping("/{itemId}/events/my")
    List<Event> getMyEvents(@PathVariable Long itemId, @RequestParam Instant dateFrom, @RequestParam Instant dateTo) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        List<Event> events = Lists.newArrayList(eventRepository.findFromToByAttendeeAndOrganizationId(itemId, Date.from(dateFrom), Date.from(dateTo), authenticatedUser.getId()));
        return eventService.setEventFieldAccessForUser(authenticatedUser.getId(), events);
    }

    @ApiOperation(value = "Получить список переговорных по организации")
    @GetMapping("/{itemId}/spaces")
    List<Space> getSpaces(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        return Lists.newArrayList(spaceRepository.findByOrganizationId(itemId));
    }

    private Image addLogo(Long organizationId, MultipartFile file, String logoType) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (file.isEmpty()) {
            throw new BadRequestException();
        }

        Organization organization = repository.findOne(organizationId);

        if (organization == null) {
            throw new EntityNotFoundException();
        }

        try {
            imageService.deleteByTypeAndReference(logoType, organizationId);
        } catch (IOException e) {
            throw new StorageException();
        }

        Image image;

        try {
            image = imageService.create(logoType, organizationId, file.getOriginalFilename(), file.getInputStream());
        } catch (ArgumentInvalidException e) {
            throw new BadRequestException();
        } catch (IOException e) {
            throw new StorageException();
        }

        switch (logoType) {
            case "organizationLogo":
                organization.setLogo(image.getFileName());
                break;
            case "organizationFullLogo":
                organization.setFullLogo(image.getFileName());
                break;
            case "organizationRoomDisplayLogo":
                organization.setRoomDisplayLogo(image.getFileName());
                break;
        }

        repository.save(organization);

        return image;
    }

    private void deleteLogo(Long organizationId, String logoType) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        Organization organization = repository.findOne(organizationId);

        if (organization == null) {
            throw new EntityNotFoundException();
        }

        try {
            imageService.deleteByTypeAndReference(logoType, organizationId);
        } catch (IOException e) {
            throw new StorageException();
        }

        switch (logoType) {
            case "organizationLogo":
                organization.setLogo(null);
                break;
            case "organizationFullLogo":
                organization.setFullLogo(null);
                break;
            case "organizationRoomDisplayLogo":
                organization.setRoomDisplayLogo(null);
                break;
        }
        repository.save(organization);
    }

    @ApiOperation(value = "Добавить лого организации")
    @PostMapping("/{itemId}/logo")
    private Image postLogoImage(@PathVariable Long itemId, @RequestParam("file") MultipartFile file) {
        return addLogo(itemId, file, "organizationLogo");
    }

    @ApiOperation(value = "Удалить лого организации")
    @DeleteMapping("/{itemId}/logo")
    private void deleteLogoImage(@PathVariable Long itemId) {
        deleteLogo(itemId, "organizationLogo");
    }

    @ApiOperation(value = "Добавить полное лого организации")
    @PostMapping("/{itemId}/logo-full")
    private Image postFullLogoImage(@PathVariable Long itemId, @RequestParam("file") MultipartFile file) {
        return addLogo(itemId, file, "organizationFullLogo");
    }

    @ApiOperation(value = "Удалить полное лого организации")
    @DeleteMapping("/{itemId}/logo-full")
    private void deleteFullLogoImage(@PathVariable Long itemId) {
        deleteLogo(itemId, "organizationFullLogo");
    }

    @ApiOperation(value = "Добавить лого организации для room display")
    @PostMapping("/{itemId}/logo-room-display")
    private Image postRoomDisplayLogoImage(@PathVariable Long itemId, @RequestParam("file") MultipartFile file) {
        return addLogo(itemId, file, "organizationRoomDisplayLogo");
    }

    @ApiOperation(value = "Удалить лого организации для room display")
    @DeleteMapping("/{itemId}/logo-room-display")
    private void deleteRoomDisplayLogoImage(@PathVariable Long itemId) {
        deleteLogo(itemId, "organizationRoomDisplayLogo");
    }
}
