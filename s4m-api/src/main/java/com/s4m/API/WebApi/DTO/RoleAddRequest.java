package com.s4m.API.WebApi.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleAddRequest {
    private String role;
    private Long location;
}
