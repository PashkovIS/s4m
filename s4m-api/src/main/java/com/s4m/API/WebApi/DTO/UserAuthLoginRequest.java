package com.s4m.API.WebApi.DTO;

import lombok.Data;

@Data
public class UserAuthLoginRequest {
    private String login;
    private String password;
}
