package com.s4m.API.WebApi.DTO;

import lombok.Data;

@Data
public class UserUpdateRequest {
    private String fullName;
    private String email;
}
