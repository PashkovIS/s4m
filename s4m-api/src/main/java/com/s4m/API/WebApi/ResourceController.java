package com.s4m.API.WebApi;

import com.google.common.collect.Lists;
import com.s4m.API.WebApi.Auth.UserAuthException;
import com.s4m.API.WebApi.Auth.UserAuthForbiddenException;
import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.Common.BadRequestException;
import com.s4m.Common.EntityNotFoundException;
import com.s4m.Common.StorageException;
import com.s4m.Entities.Image.Image;
import com.s4m.Entities.Image.ImageService;
import com.s4m.Entities.Resource.Resource;
import com.s4m.Entities.Resource.ResourceRepository;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.User.RoleService;
import com.s4m.Entities.User.User;
import ecstaj.ArgumentInvalidException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Api(description = "Справочник ресурсов")
@RestController
@Conditional(WebApiActivator.class)
@RequestMapping("/web-api/resources")
@Slf4j
public class ResourceController {
    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private RoleService roleService;

    @ApiOperation(value = "Получить список ресурсов")
    @GetMapping
    List<Resource> search() {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        return Lists.newArrayList(resourceRepository.findAll());
    }

    @ApiOperation(value = "Получить информацию по ресурсу")
    @GetMapping("/{itemId}")
    Resource get(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!resourceRepository.exists(itemId)) throw new EntityNotFoundException();
        return resourceRepository.findOne(itemId);
    }

    @ApiOperation(value = "Создать ресурс")
    @PostMapping
    Resource create(@RequestBody Resource item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        return resourceRepository.save(item);
    }

    @ApiOperation(value = "Обновить информацию по ресурсу")
    @PutMapping("/{itemId}")
    Resource update(@PathVariable Long itemId, @RequestBody Resource item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!resourceRepository.exists(itemId)) throw new EntityNotFoundException();
        item.setId(itemId);
        return resourceRepository.save(item);
    }

    @ApiOperation(value = "Удалить ресурс")
    @DeleteMapping("/{itemId}")
    void delete(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!resourceRepository.exists(itemId)) throw new EntityNotFoundException();
        resourceRepository.delete(itemId);
    }

    @Autowired
    private ImageService _imageService;

    @ApiOperation(value = "Добавить фото ресурса")
    @PostMapping("/{resourceId}/image")
    private Image postImage(@PathVariable Long resourceId, @RequestParam("file") MultipartFile file) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (file.isEmpty()) {
            throw new BadRequestException();
        }

        Resource resource = resourceRepository.findOne(resourceId);

        if (resource == null) {
            throw new EntityNotFoundException();
        }

        // There can be only one image per space.

        try {
            _imageService.deleteByTypeAndReference("resource", resourceId);
        } catch (IOException e) {
            throw new StorageException();
        }

        Image image;

        try {
            image = _imageService.create("resource", resourceId, file.getOriginalFilename(), file.getInputStream());
        } catch (ArgumentInvalidException e) {
            throw new BadRequestException();
        } catch (IOException e) {
            throw new StorageException();
        }

        resource.setImage(image.getFileName());

        resourceRepository.save(resource);

        return image;
    }

    @ApiOperation(value = "Удалить фото ресурса")
    @DeleteMapping("/{resourceId}/image")
    private void deleteImage(@PathVariable Long resourceId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        Resource resource = resourceRepository.findOne(resourceId);

        if (resource == null) {
            throw new EntityNotFoundException();
        }

        try {
            _imageService.deleteByTypeAndReference("resource", resourceId);
        } catch (IOException e) {
            throw new StorageException();
        }

        resource.setImage(null);

        resourceRepository.save(resource);
    }
}
