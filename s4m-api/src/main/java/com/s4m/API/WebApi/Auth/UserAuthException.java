package com.s4m.API.WebApi.Auth;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "User not authorized")
public class UserAuthException extends RuntimeException {
}
