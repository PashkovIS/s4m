package com.s4m.API.WebApi;

import com.google.common.collect.Lists;
import com.s4m.API.WebApi.Auth.UserAuthException;
import com.s4m.API.WebApi.Auth.UserAuthForbiddenException;
import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.API.WebApi.DTO.EventCreateRequest;
import com.s4m.Common.BadRequestException;
import com.s4m.Common.EntityNotFoundException;
import com.s4m.Common.StorageException;
import com.s4m.Entities.Device.Device;
import com.s4m.Entities.Device.DeviceRepository;
import com.s4m.Entities.Event.Event;
import com.s4m.Entities.Event.EventRepository;
import com.s4m.Entities.Event.EventService;
import com.s4m.Entities.EventAttendee.EventAttendeeRepository;
import com.s4m.Entities.Image.Image;
import com.s4m.Entities.Image.ImageService;
import com.s4m.Entities.Location.Location;
import com.s4m.Entities.Location.LocationRepository;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.Space.SpaceRepository;
import com.s4m.Entities.SpaceSettings.SpaceSettings;
import com.s4m.Entities.SpaceSettings.SpaceSettingsRepository;
import com.s4m.Entities.User.RoleService;
import com.s4m.Entities.User.User;
import ecstaj.ArgumentInvalidException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Api(description = "Переговорные")
@RestController
@Conditional(WebApiActivator.class)
@RequestMapping("/web-api/spaces")
@Slf4j
public class SpaceController {
    @Autowired
    private EventService eventService;

    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private SpaceSettingsRepository spaceSettingsRepository;

    @Autowired
    private EventAttendeeRepository eventAttendeeRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private LocationRepository locationRepository;

    @ApiOperation(value = "Получить список всех переговорных")
    @GetMapping
    private List<Space> getSpaces() {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        return Lists.newArrayList(spaceRepository.findAll());
    }

    @ApiOperation(value = "Поиск переговорной")
    @GetMapping("/search")
    private List<Space> search(
            @RequestParam Long organizationId,
            @RequestParam(required = false) List<Long> locationId,
            @RequestParam(required = false) Integer capacity,
            @RequestParam(required = false) List<String> spaceTypeId,
            @RequestParam Instant startDateTime,
            @RequestParam Integer durationInMinutes,
            @RequestParam(required = false) String spaceName) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        List<Space> found = new LinkedList<>();
        List<Space> filtered = new LinkedList<>();

        // Get all spaces by organization or by selected locations
        if (locationId == null || locationId.isEmpty()) {
            found.addAll(spaceRepository.findByOrganizationId(organizationId));
        } else {
            for (Long lId : locationId) {
                if (lId == null) {
                    continue;
                }
                found.addAll(spaceRepository.findByLocationId(lId));
            }
        }

        // Prepare selected spaceTypes Map
        Map<String, Boolean> selectedSpaceTypes = null;
        if (spaceTypeId != null && !spaceTypeId.isEmpty()) {
            selectedSpaceTypes = new HashMap<>();
            for (String stId : spaceTypeId) {
                if (stId != null) {
                    selectedSpaceTypes.put(stId, true);
                }
            }
        }

        // Prepare spaceName search pattern
        Pattern spaceNamePattern = null;
        if (spaceName != null) {
            spaceNamePattern = Pattern.compile(spaceName, Pattern.LITERAL | Pattern.CASE_INSENSITIVE);
        }

        for (Space space : found) {
            boolean isSuitable = (capacity == null || space.getCapacity() >= capacity) &&
                    (selectedSpaceTypes == null || selectedSpaceTypes.containsKey(space.getSpaceTypeId()));

            if (isSuitable && spaceNamePattern != null) {
                Matcher matcher = spaceNamePattern.matcher(space.getName());
                isSuitable = matcher.find();
            }

            if (isSuitable) {
                List<Event> overlapping = eventRepository.getOverlappingBySpaceId(
                        space.getId(),
                        0L,
                        Date.from(startDateTime),
                        Date.from(startDateTime.plus(durationInMinutes, ChronoUnit.MINUTES)));
                isSuitable = overlapping.isEmpty();
            }

            if (isSuitable) {
                filtered.add(space);
            }
        }

        return filtered;
    }

    @ApiOperation(value = "Получить информацию по переговорной")
    @GetMapping("/{itemId}")
    private Space get(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        Space space = spaceRepository.findOne(itemId);

        if (space == null) {
            throw new EntityNotFoundException();
        }

        return space;
    }

    @ApiOperation(value = "Обновить информацию по переговорной")
    @PutMapping("/{itemId}")
    Space update(@PathVariable Long itemId, @RequestBody Space item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!spaceRepository.exists(itemId)) throw new EntityNotFoundException();
        item.setId(itemId);
        return spaceRepository.save(item);
    }

    @ApiOperation(value = "Удалить переговорную")
    @DeleteMapping("/{itemId}")
    void delete(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!spaceRepository.exists(itemId)) throw new EntityNotFoundException();

        spaceRepository.delete(itemId);
    }

    @ApiOperation(value = "Получить список встреч для переговорной")
    @GetMapping("/{itemId}/events")
    private List<Event> getEvents(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        List<Event> events = Lists.newArrayList(eventRepository.findBySpaceId(itemId));
        return eventService.setEventFieldAccessForUser(authenticatedUser.getId(), events);
    }

    @ApiOperation(value = "Запланировать встречу в переговорной")
    @PostMapping("/{itemId}/events")
    private Event createEvent(@PathVariable Long itemId, @RequestBody EventCreateRequest item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) {
            throw new UserAuthException();
        }

        Event event = eventService.create(authenticatedUser, itemId, "portal", item);

        Space space = spaceRepository.findOne(event.getSpaceId());
        Location location = locationRepository.findOne(space.getLocationId());
        eventService.notifyOnResourceOrder(event, space, location, item.getResources());

        return event;
    }

    @ApiOperation(value = "Получить список связанных устройств")
    @GetMapping("/{itemId}/devices")
    List<Device> getDevices(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        return Lists.newArrayList(deviceRepository.findBySpaceId(itemId));
    }

    @ApiOperation(value = "Получить настройки переговорной")
    @GetMapping("/{itemId}/settings")
    SpaceSettings getSettings(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        SpaceSettings settings = spaceSettingsRepository.findOne(itemId);

        if (settings == null) {
            settings = new SpaceSettings();
        }

        return settings;
    }

    @ApiOperation(
            value = "Сохранить настройки переговорной",
            notes = "Для поля statusBarColor допустимыми значениями являются { \"black\", \"white\" }")
    @PutMapping("/{itemId}/settings")
    SpaceSettings putSettings(@PathVariable Long itemId, @RequestBody SpaceSettings item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        item.setSpaceId(itemId);

        String statusBarColor = item.getStatusBarColor();

        if (statusBarColor == null || statusBarColor.equals("")) {
            statusBarColor = "black";
        } else {
            statusBarColor = statusBarColor.toLowerCase();

            if (!statusBarColor.equals("black") && !statusBarColor.equals("white")) {
                throw new BadRequestException();
            }
        }

        item.setStatusBarColor(statusBarColor);

        spaceSettingsRepository.save(item);

        return item;
    }

    @Autowired
    private ImageService _imageService;

    //
    // Nginx must add a mapping for paths ${root}/storage/images/* to be handled as static media files.
    //

    @ApiOperation(value = "Добавить фото переговорной")
    @PostMapping("/{spaceId}/image")
    private Image postImage(@PathVariable Long spaceId, @RequestParam("file") MultipartFile file) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (file.isEmpty()) {
            throw new BadRequestException();
        }

        Space space = spaceRepository.findOne(spaceId);

        if (space == null) {
            throw new EntityNotFoundException();
        }

        // There can be only one image per space.

        try {
            _imageService.deleteByTypeAndReference("space", spaceId);
        } catch (IOException e) {
            throw new StorageException();
        }

        Image image;

        try {
            image = _imageService.create("space", spaceId, file.getOriginalFilename(), file.getInputStream());
        } catch (ArgumentInvalidException e) {
            throw new BadRequestException();
        } catch (IOException e) {
            throw new StorageException();
        }

        space.setImage(image.getFileName());

        spaceRepository.save(space);

        return image;
    }

    @ApiOperation(value = "Удалить фото переговорной")
    @DeleteMapping("/{spaceId}/image")
    private void deleteImage(@PathVariable Long spaceId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        Space space = spaceRepository.findOne(spaceId);

        if (space == null) {
            throw new EntityNotFoundException();
        }

        try {
            _imageService.deleteByTypeAndReference("space", spaceId);
        } catch (IOException e) {
            throw new StorageException();
        }

        space.setImage(null);

        spaceRepository.save(space);
    }
}
