package com.s4m.API.WebApi.DTO;

import lombok.Data;

@Data
public class UserSetPasswordRequest {
    private String password;
}
