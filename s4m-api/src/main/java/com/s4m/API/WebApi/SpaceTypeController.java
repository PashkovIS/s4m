package com.s4m.API.WebApi;

import com.google.common.collect.Lists;
import com.s4m.API.WebApi.Auth.UserAuthException;
import com.s4m.API.WebApi.Auth.UserAuthForbiddenException;
import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.API.WebApi.DTO.EventCreateRequest;
import com.s4m.Common.BadRequestException;
import com.s4m.Common.ConflictException;
import com.s4m.Common.EntityNotFoundException;
import com.s4m.Entities.Device.Device;
import com.s4m.Entities.Device.DeviceRepository;
import com.s4m.Entities.Event.Event;
import com.s4m.Entities.Event.EventRepository;
import com.s4m.Entities.Event.EventService;
import com.s4m.Entities.EventAttendee.EventAttendee;
import com.s4m.Entities.EventAttendee.EventAttendeeRepository;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.Space.SpaceRepository;
import com.s4m.Entities.SpaceSettings.SpaceSettings;
import com.s4m.Entities.SpaceSettings.SpaceSettingsRepository;
import com.s4m.Entities.SpaceType.SpaceType;
import com.s4m.Entities.SpaceType.SpaceTypeRepository;
import com.s4m.Entities.User.RoleService;
import com.s4m.Entities.User.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "Типы помещений")
@RestController
@Conditional(WebApiActivator.class)
@RequestMapping("/web-api/space-types")
@Slf4j
public class SpaceTypeController {
    @Autowired
    private SpaceTypeRepository spaceTypeRepository;

    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private RoleService roleService;

    @ApiOperation(value = "Получить список типов помещений")
    @GetMapping
    List<SpaceType> search() {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        return Lists.newArrayList(spaceTypeRepository.findAll());
    }

    @ApiOperation(value = "Получить информацию по типу помещения")
    @GetMapping("/{itemId}")
    SpaceType get(@PathVariable String itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        return spaceTypeRepository.findOne(itemId);
    }

    @ApiOperation(value = "Создать тип помещения")
    @PostMapping
    SpaceType create(@RequestBody SpaceType item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        String id = item.getId();
        String name = item.getName();
        if (id == null || id.equals("")) throw new BadRequestException();
        if (name == null || name.equals("")) throw new BadRequestException();
        if (spaceTypeRepository.exists(id)) throw new BadRequestException();

        return spaceTypeRepository.save(item);
    }

    @ApiOperation(value = "Обновить информацию по типу помещения")
    @PutMapping("/{itemId}")
    SpaceType update(@PathVariable String itemId, @RequestBody SpaceType item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!spaceTypeRepository.exists(itemId)) throw new EntityNotFoundException();
        item.setId(itemId);
        return spaceTypeRepository.save(item);
    }

    @ApiOperation(value = "Удалить тип помещения")
    @DeleteMapping("/{itemId}")
    void delete(@PathVariable String itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!spaceTypeRepository.exists(itemId)) throw new EntityNotFoundException();

        spaceTypeRepository.delete(itemId);
    }
}
