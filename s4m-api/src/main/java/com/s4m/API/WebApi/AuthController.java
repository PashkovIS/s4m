package com.s4m.API.WebApi;

import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.API.WebApi.DTO.UserAuthLoginRequest;
import com.s4m.API.WebApi.DTO.UserAuthResponse;
import com.s4m.Entities.User.Role;
import com.s4m.Entities.User.RoleRepository;
import com.s4m.Entities.User.User;
import com.s4m.Entities.User.UserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Api(description = "Пользовательская авторизация")
@RestController
@Conditional(WebApiActivator.class)
@RequestMapping("/web-api/auth")
@Slf4j
public class AuthController {
    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private RoleRepository roleRepository;

    @ApiOperation(value = "Создание новой пользовательской сессии")
    @PostMapping("/login")
    UserAuthResponse login(@RequestBody UserAuthLoginRequest item, HttpServletResponse response) {
        userAuthService.logout(response);

        UserAuthResponse userAuthResponse = new UserAuthResponse();

        User authenticatedUser = userAuthService.login(item.getLogin(), item.getPassword(), response);
        Boolean isAuthorized = (authenticatedUser != null);
        userAuthResponse.setAuthorized(isAuthorized);
        userAuthResponse.setUser(authenticatedUser);

        if (authenticatedUser != null) {
            List<Role> roles = roleRepository.findByUserId(authenticatedUser.getId());
            userAuthResponse.setRoles(roles);
        }

        return userAuthResponse;
    }

    @ApiOperation(value = "Завершение текущей пользовательской сессии")
    @PostMapping("/logout")
    UserAuthResponse logout(HttpServletResponse response) {
        userAuthService.logout(response);
        UserAuthResponse authResponse = new UserAuthResponse();
        authResponse.setAuthorized(false);
        return authResponse;
    }

    @ApiOperation(value = "Получение информации по текущей пользовательской сессии")
    @PostMapping("/check")
    UserAuthResponse check() {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        UserAuthResponse userAuthResponse = new UserAuthResponse();
        userAuthResponse.setAuthorized(authenticatedUser != null);
        userAuthResponse.setUser(authenticatedUser);

        if (authenticatedUser != null) {
            List<Role> roles = roleRepository.findByUserId(authenticatedUser.getId());
            userAuthResponse.setRoles(roles);
        }

        return userAuthResponse;
    }
}
