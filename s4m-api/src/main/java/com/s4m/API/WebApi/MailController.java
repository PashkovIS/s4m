package com.s4m.API.WebApi;

import com.google.common.collect.Lists;
import com.s4m.API.WebApi.Auth.UserAuthException;
import com.s4m.API.WebApi.Auth.UserAuthForbiddenException;
import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.API.WebApi.DTO.EventCreateRequest;
import com.s4m.Background.BackgroundExecutor;
import com.s4m.Common.BadRequestException;
import com.s4m.Common.EntityNotFoundException;
import com.s4m.Common.StorageException;
import com.s4m.Entities.Device.Device;
import com.s4m.Entities.Device.DeviceRepository;
import com.s4m.Entities.Event.Event;
import com.s4m.Entities.Event.EventRepository;
import com.s4m.Entities.Event.EventService;
import com.s4m.Entities.EventAttendee.EventAttendeeRepository;
import com.s4m.Entities.Image.Image;
import com.s4m.Entities.Image.ImageService;
import com.s4m.Entities.Mail.MailService;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.Space.SpaceRepository;
import com.s4m.Entities.SpaceSettings.SpaceSettings;
import com.s4m.Entities.SpaceSettings.SpaceSettingsRepository;
import com.s4m.Entities.User.RoleService;
import com.s4m.Entities.User.User;
import ecstaj.ArgumentInvalidException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Api(description = "Рассылки")
@RestController
@Conditional(WebApiActivator.class)
@RequestMapping("/web-api/mailing")
@Slf4j
public class MailController {
    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private MailService mailService;

    @Autowired
    private BackgroundExecutor backgroundExecutor;

    @ApiOperation(value = "Разослать письма из очереди")
    @GetMapping("/force")
    private void sendAll() {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        backgroundExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mailService.sendAll();
            }
        });
    }
}
