package com.s4m.API.WebApi;

import com.s4m.API.WebApi.Auth.UserAuthException;
import com.s4m.API.WebApi.Auth.UserAuthForbiddenException;
import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.API.WebApi.DTO.DeviceUpdateRequest;
import com.s4m.Common.EntityNotFoundException;
import com.s4m.Entities.Device.Device;
import com.s4m.Entities.Device.DeviceRepository;
import com.s4m.Entities.DeviceActivation.DeviceActivationConfirmRequest;
import com.s4m.Entities.DeviceActivation.DeviceActivationService;
import com.s4m.Entities.User.RoleService;
import com.s4m.Entities.User.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.*;

@Api(description = "Устройства управления переговорными")
@RestController
@Conditional(WebApiActivator.class)
@RequestMapping("/web-api/devices")
@Slf4j
public class DeviceController {
    private final UserAuthService userAuthService;
    private final DeviceRepository deviceRepository;
    private final DeviceActivationService deviceActivationService;

    public DeviceController(UserAuthService userAuthService,
                            DeviceRepository deviceRepository,
                            DeviceActivationService deviceActivationService) {
        this.userAuthService = userAuthService;
        this.deviceRepository = deviceRepository;
        this.deviceActivationService = deviceActivationService;
    }

    @Autowired
    private RoleService roleService;

    @ApiOperation(value = "Получить информацию об устройстве")
    @GetMapping("/{itemId}")
    Device get(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!deviceRepository.exists(itemId)) throw new EntityNotFoundException();
        return deviceRepository.findOne(itemId);
    }

    @ApiOperation(value = "Удалить устройство")
    @DeleteMapping("/{itemId}")
    void delete(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!deviceRepository.exists(itemId)) throw new EntityNotFoundException();
        deviceRepository.delete(itemId);
    }

    @ApiOperation(value = "Подтвердить подключение устройства", notes = "Подтверждение связывания устройства с переговоркой")
    @PostMapping("/activate")
    public Device confirmActivation(@RequestBody DeviceActivationConfirmRequest request) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        return deviceActivationService.confirmActivation(request);
    }

    @ApiOperation(value = "Переименовать устройство")
    @PutMapping("/{itemId}")
    Device put(@PathVariable Long itemId, @RequestBody DeviceUpdateRequest deviceUpdateRequest) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!deviceRepository.exists(itemId)) throw new EntityNotFoundException();
        Device device = deviceRepository.findOne(itemId);
        device.setName(deviceUpdateRequest.getDeviceName());
        return deviceRepository.save(device);
    }
}
