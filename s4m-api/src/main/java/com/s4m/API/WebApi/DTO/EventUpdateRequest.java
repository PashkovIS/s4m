package com.s4m.API.WebApi.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.s4m.Entities.EventResource.EventResource;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventUpdateRequest {
    private String title;
    private String description;
    private Instant start;
    private Instant end;
    private List<EventAttendeeCreateRequest> attendees;
    private String greeting;
    private boolean privateEvent;
    private Boolean allDay;
    private List<EventResource> resources;
}
