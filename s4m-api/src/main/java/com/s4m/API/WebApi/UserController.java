package com.s4m.API.WebApi;

import com.google.common.collect.Lists;
import com.s4m.API.WebApi.Auth.UserAuthException;
import com.s4m.API.WebApi.Auth.UserAuthForbiddenException;
import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.API.WebApi.DTO.*;
import com.s4m.Common.*;
import com.s4m.Entities.Image.Image;
import com.s4m.Entities.Image.ImageService;
import com.s4m.Entities.User.*;
import ecstaj.ArgumentInvalidException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Api(description = "Пользователи")
@RestController
@Conditional(WebApiActivator.class)
@RequestMapping("/web-api/users")
@Slf4j
public class UserController {
    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ImageService imageService;

    @ApiOperation(value = "Получить информацию по пользователю")
    @GetMapping("/{itemId}")
    private User get(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!userRepository.exists(itemId)) throw new EntityNotFoundException();
        return userRepository.findOne(itemId);
    }

    @ApiOperation(value = "Получить список пользователей")
    @GetMapping
    private List<User> getList() {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        return Lists.newLinkedList(userRepository.findAll());
    }

    @ApiOperation(value = "Найти пользователей по email или имени")
    @GetMapping("/search")
    private List<User> search(String q) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        return userService.search(q);
    }

    @ApiOperation(value = "Создать нового пользователя", notes = "Доступно только администратору")
    @PostMapping
    private User create(@RequestBody UserCreateRequest request) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        List<User> users = userRepository.findByLogin(request.getLogin());
        if (!users.isEmpty()) throw new EntityAlreadyExistsException();

        String password = request.getPassword();
        if (password == null || password.equals("")) throw new BadRequestException();

        User user = new User();
        user.setLogin(request.getLogin());
        user.setFullName(request.getFullName());
        user.setEmail(request.getEmail());

        byte[] salt = userService.generateSalt();
        byte[] hash = userService.hashPassword(password, salt);
        user.setPasswordHash(userService.bytesToHex(hash));
        user.setSalt(userService.bytesToHex(salt));

        userRepository.save(user);

        return user;
    }

    @ApiOperation(value = "Установить пароль для пользователя", notes = "Доступно только администратору")
    @PostMapping("/{itemId}/password")
    private User setPassword(@PathVariable Long itemId, @RequestBody UserSetPasswordRequest item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!userRepository.exists(itemId)) throw new EntityNotFoundException();

        User user = userRepository.findOne(itemId);

        String password = item.getPassword();
        byte[] salt = userService.generateSalt();
        byte[] hash = userService.hashPassword(password, salt);
        user.setPasswordHash(userService.bytesToHex(hash));
        user.setSalt(userService.bytesToHex(salt));

        return userRepository.save(user);
    }

    @ApiOperation(value = "Сменить пароль для пользователя", notes = "Доступно только администратору или авторизованному пользователю (может поменять свой пароль)")
    @PutMapping("/{itemId}/password")
    private User changePassword(@PathVariable Long itemId, @RequestBody UserChangePasswordRequest item) {
        if (!userRepository.exists(itemId)) throw new EntityNotFoundException();

        if (item.getOldPassword() == null
                || item.getOldPassword().equals("")
                || item.getNewPassword() == null
                || item.getNewPassword().equals(""))
            throw new BadRequestException();

        User user = userRepository.findOne(itemId);
        byte[] currentSalt = userService.hexToBytes(user.getSalt());
        String currentPasswordHash = user.getPasswordHash();

        String oldPassword = item.getOldPassword();
        byte[] oldHash = userService.hashPassword(oldPassword, currentSalt);
        String oldPasswordHash = userService.bytesToHex(oldHash);

        if (!currentPasswordHash.equals(oldPasswordHash)) {
            throw new UserAuthForbiddenException();
        }

        String password = item.getNewPassword();
        byte[] salt = userService.generateSalt();
        byte[] hash = userService.hashPassword(password, salt);
        user.setPasswordHash(userService.bytesToHex(hash));
        user.setSalt(userService.bytesToHex(salt));

        return userRepository.save(user);
    }

    @ApiOperation(value = "Обновить информацию по пользователю", notes = "Доступно только администратору или авторизованному пользователю (может поменять свой пароль)")
    @PutMapping("/{itemId}")
    private User update(@PathVariable Long itemId, @RequestBody UserUpdateRequest item) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (!userRepository.exists(itemId)) throw new EntityNotFoundException();

        User user = userRepository.findOne(itemId);
        user.setFullName(item.getFullName());
        user.setEmail(item.getEmail());

        return userRepository.save(user);
    }

    @ApiOperation(value = "Удалить пользователя", notes = "Доступно только администратору")
    @DeleteMapping("/{itemId}")
    private void delete(@PathVariable Long itemId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        User user = userRepository.findOne(itemId);

        if (user == null) {
            throw new EntityNotFoundException();
        }

        if (user.getLogin().equals(UserService.ADMINISTRATOR)) {
            throw new ForbiddenException("Unable to delete admin account");
        }

        userRepository.delete(itemId);
    }

    // Role-related methods:

    @Autowired
    private RoleService roleService;

    @ApiOperation(value = "Получить список ролей пользователя", notes = "Доступно только администратору")
    @GetMapping("/{userId}/roles")
    private List<Role> getRoles(@PathVariable Long userId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        User user = userRepository.findOne(userId);

        if (user == null) {
            throw new EntityNotFoundException();
        }

        return roleService.listRoles(user.getId());
    }

    private Role addRole(User user, RoleAddRequest request) {
        String role = request.getRole();

        if (role == null) {
            throw new BadRequestException();
        }

        role = role.toLowerCase();

        switch (role) {
            case Role.ADMINISTRATOR:
                return roleService.setAdministrator(user.getId());

            case Role.OFFICE_MANAGER:
                Long location = request.getLocation();

                if (location == null) {
                    throw new BadRequestException();
                }

                return roleService.setOfficeManagerFor(user.getId(), location);

            default:
                throw new BadRequestException();
        }
    }

    @ApiOperation(
            value = "Добавить роль пользователю",
            notes = "Доступно только администратору. Для поля 'role' допустимы значения { \"admin\", \"office manager\" }. Если 'role'=\"office manager\", то поле 'location' должно присутствовать.")
    @PostMapping("/{userId}/roles")
    private Role postRole(@PathVariable Long userId, @RequestBody RoleAddRequest request) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        User user = userRepository.findOne(userId);

        if (user == null) {
            throw new EntityNotFoundException();
        }

        return addRole(user, request);
    }

    @ApiOperation(value = "Удалить роль пользователя", notes = "Доступно только администратору")
    @DeleteMapping("/{userId}/roles/{roleId}")
    private void deleteRole(@PathVariable Long userId, @PathVariable Long roleId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        User user = userRepository.findOne(userId);

        if (user == null) {
            throw new EntityNotFoundException();
        }

        roleService.unsetRole(roleId);
    }

    @ApiOperation(value = "Добавить фото пользователя")
    @PostMapping("/{userId}/image")
    private Image postImage(@PathVariable Long userId, @RequestParam("file") MultipartFile file) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        if (file.isEmpty()) {
            throw new BadRequestException();
        }

        User user = userRepository.findOne(userId);

        if (user == null) {
            throw new EntityNotFoundException();
        }

        // There can be only one image per space.

        try {
            imageService.deleteByTypeAndReference("user", userId);
        } catch (IOException e) {
            throw new StorageException();
        }

        Image image;

        try {
            image = imageService.create("user", userId, file.getOriginalFilename(), file.getInputStream());
        } catch (ArgumentInvalidException e) {
            throw new BadRequestException();
        } catch (IOException e) {
            throw new StorageException();
        }

        user.setImage(image.getFileName());

        userRepository.save(user);

        return image;
    }

    @ApiOperation(value = "Удалить фото пользователя")
    @DeleteMapping("/{userId}/image")
    private void deleteImage(@PathVariable Long userId) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!roleService.isAdministrator(authenticatedUser.getId())) {
            throw new UserAuthForbiddenException();
        }

        User user = userRepository.findOne(userId);

        if (user == null) {
            throw new EntityNotFoundException();
        }

        try {
            imageService.deleteByTypeAndReference("user", userId);
        } catch (IOException e) {
            throw new StorageException();
        }

        user.setImage(null);

        userRepository.save(user);
    }
}
