package com.s4m.Entities.Mail;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

/**
 * Created by nikolay on 3/19/17.
 */
@Data
@Entity
public class Mail {
    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false, updatable = false)
    private String recipient;

    @Column(nullable = false, updatable = false)
    private String subject;

    @Lob
    @Column
    private String text;

    @Lob
    @Column
    private String html;

    @Column(nullable = false, updatable = false)
    private Instant timestampCreated;

    @Column
    private Instant timestampSent;

    @Column
    private String status = "queued";

    @Lob
    @Column
    private String comment;

    @Column(nullable = true, insertable = true, updatable = true)
    private Long tag = null;
}
