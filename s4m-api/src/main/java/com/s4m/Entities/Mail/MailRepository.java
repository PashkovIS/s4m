package com.s4m.Entities.Mail;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by nikolay on 3/19/17.
 */
@Service
public interface MailRepository extends CrudRepository<Mail, Long> {

    @Modifying
    @Transactional
    @Query(value = "UPDATE MAIL SET tag = ?1 WHERE tag IS NULL", nativeQuery = true)
    void markWithTag(long tag);

    @Query(value = "SELECT * FROM MAIL WHERE tag = ?1", nativeQuery = true)
    Iterable<Mail> findAllByTag(long tag);
}
