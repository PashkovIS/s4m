package com.s4m.Entities.Mail;

import com.sun.mail.smtp.SMTPTransport;
import ecstaj.ArgumentRangeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

/**
 * Created by nikolay on 3/19/17.
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MailRepository mailRepository;

    public void enqueue(Mail mail) {
        mailRepository.save(mail);
    }

    public void send(long mailId) {
        Mail mail = mailRepository.findOne(mailId);

        if (mail == null) {
            throw new ArgumentRangeException("Index not found");
        }

        send(mail);
    }

    @Value("${app.mailing.SmtpFrom}")
    private String smtpFrom;
    @Value("${app.mailing.SmtpServer}")
    private String smtpServer;
    @Value("${app.mailing.SmtpPort}")
    private int smtpPort;
    @Value("${app.mailing.SmtpSsl}")
    private boolean smtpSsl;
    @Value("${app.mailing.SmtpTls}")
    private boolean smtpTls;
    @Value("${app.mailing.SmtpDebug}")
    private boolean smtpDebug;
    @Value("${app.mailing.SmtpLogin}")
    private String smtpLogin;
    @Value("${app.mailing.SmtpPassword}")
    private String smtpPassword;

    public void send(Mail mail) {
        SMTPTransport t = null;

        Properties props = System.getProperties();

        props.put("mail.smtp.auth", "true");
        if (smtpSsl) {
            props.put("mail.smtp.ssl.enable", "true");
        }
        if (smtpTls) {
            props.put("mail.smtp.starttls.enable","true");
        }

        Session session = Session.getInstance(props, null);

        if (smtpDebug) {
            session.setDebug(true);
        }

        try {

            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(smtpFrom));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getRecipient(), false));
            msg.setSubject(mail.getSubject());

            String text = mail.getText();
            String html = mail.getHtml();

            if (html != null)
            {
                msg.setText(html);
                msg.setHeader("Content-Type", "text/html;charset=\"UTF-8\"");
            }
            else
            {
                msg.setText(text);
                msg.setHeader("Content-Type", "text/plain;charset=\"UTF-8\"");
            }

            msg.setSentDate(new Date());

            t = (SMTPTransport) session.getTransport("smtp");

            t.connect(smtpServer, smtpPort, smtpLogin, smtpPassword);
            t.sendMessage(msg, msg.getAllRecipients());

            mail.setStatus("sent");
            mail.setComment(t.getLastServerResponse());
            mail.setTimestampSent(Instant.now());
        } catch (MessagingException e) {
            log.error("Exception while sending mail message", e);
            mail.setStatus("error");
            mail.setComment(e.getMessage());
            mail.setTimestampSent(null);
        } finally {
            if (t != null) {
                try {
                    t.close();
                } catch (Exception e) {
                    log.error("Exception while closing SMTP connection", e);
                }
            }
        }

        mailRepository.save(mail);
    }

    private static Random random = new Random(Instant.now().toEpochMilli());

    public void sendAll() {

        // Compile pseudo-random tag from timestamp for The Greater Good!
        long tag = Instant.now().truncatedTo(ChronoUnit.SECONDS).toEpochMilli() + random.nextInt(1000);

        mailRepository.markWithTag(tag);

        for (Mail mail : mailRepository.findAllByTag(tag)) {
            send(mail);
        }
    }
}
