package com.s4m.Entities.Event;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface EventRepository extends CrudRepository<Event, Long> {
    @Query(value = "SELECT * FROM EVENT WHERE SPACE_ID = ?1", nativeQuery = true)
    List<Event> findBySpaceId(Long spaceId);

    @Query(value = "SELECT * FROM EVENT WHERE SPACE_ID = ?1 AND START <= ?2 AND END >= ?2 AND (STATUS = 'scheduled' OR STATUS = 'started') ORDER BY START", nativeQuery = true)
    List<Event> findCurrentBySpaceId(Long spaceId, Date now);

    @Query(value = "SELECT * FROM EVENT E WHERE START <= ?2 AND END >= ?2 AND (STATUS = 'scheduled' OR STATUS = 'started') AND (SELECT COUNT(*) FROM EVENT_ATTENDEE EA WHERE EA.EVENT_ID = E.ID AND EMAIL = ?1) > 0 ORDER BY START", nativeQuery = true)
    List<Event> findCurrentByAttendeeEmail(String email, Date now);

    @Query(value = "SELECT * FROM EVENT WHERE SPACE_ID = ?1 AND START >= ?2 AND (STATUS = 'scheduled' OR STATUS = 'started') ORDER BY START LIMIT 1", nativeQuery = true)
    List<Event> findNextBySpaceId(Long spaceId, Date now);

    @Query(value = "SELECT * FROM EVENT WHERE SPACE_ID = ?1 AND END >= ?2 AND (STATUS = 'scheduled' OR STATUS = 'started') ORDER BY START LIMIT ?3", nativeQuery = true)
    List<Event> findAllEndingAfterBySpaceId(Long spaceId, Date now, long count);

    @Query(value = "SELECT * FROM EVENT WHERE SPACE_ID = ?1 AND END >= ?2 AND START < ?3 AND (STATUS = 'scheduled' OR STATUS = 'started') ORDER BY START", nativeQuery = true)
    List<Event> findFromToBySpaceId(Long spaceId, Date from, Date to);

    @Query(value = "SELECT * FROM EVENT WHERE SPACE_ID = ?1 AND ID != ?2 AND ((?3 >= START AND ?3 < END) OR (?4 > START AND ?4 <= END) OR (?3 <= START AND ?4 >= END)) AND (STATUS = 'scheduled' OR STATUS = 'started')", nativeQuery = true)
    List<Event> getOverlappingBySpaceId(Long spaceId, Long eventId, Date from, Date to);

    @Query(value = "SELECT E.* FROM EVENT E INNER JOIN SPACE S ON S.ID = E.SPACE_ID WHERE S.LOCATION_ID = ?1 AND E.STATUS <> 'cancelled'", nativeQuery = true)
    List<Event> findByLocationId(Long locationId);

    @Query(value = "SELECT E.* FROM EVENT E INNER JOIN SPACE S ON S.ID = E.SPACE_ID INNER JOIN LOCATION L ON L.ID = S.LOCATION_ID WHERE L.ORGANIZATION_ID = ?1 AND E.STATUS <> 'cancelled'", nativeQuery = true)
    List<Event> findByOrganizationId(Long organizationId);

    @Query(value = "SELECT E.* FROM EVENT E INNER JOIN SPACE S ON S.ID = E.SPACE_ID INNER JOIN LOCATION L ON L.ID = S.LOCATION_ID WHERE L.ORGANIZATION_ID = ?1 AND E.END >= ?2 AND E.START < ?3 AND E.STATUS <> 'cancelled'", nativeQuery = true)
    List<Event> findFromToByOrganizationId(Long organizationId, Date from, Date to);

    @Query(value = "SELECT E.* FROM EVENT E INNER JOIN SPACE S ON S.ID = E.SPACE_ID INNER JOIN LOCATION L ON L.ID = S.LOCATION_ID INNER JOIN EVENT_ATTENDEE EA ON EA.EVENT_ID = E.ID WHERE L.ORGANIZATION_ID = ?1 AND E.END >= ?2 AND E.START < ?3 AND EA.USER_ID = ?4 AND E.STATUS <> 'cancelled'", nativeQuery = true)
    List<Event> findFromToByAttendeeAndOrganizationId(Long organizationId, Date from, Date to, Long userId);

    @Query(value = "SELECT E.* FROM EVENT E INNER JOIN SPACE S ON S.ID = E.SPACE_ID INNER JOIN SPACE_SETTINGS SS ON SS.SPACE_ID = S.ID WHERE SS.AUTO_UNBOOK = true AND E.STATUS = 'scheduled' AND E.START <= ?", nativeQuery = true)
    List<Event> findUnbookable(Date threshold);

    @Transactional
    @Modifying
    @Query(value = "UPDATE EVENT SET STATUS = 'finished' WHERE (STATUS = 'scheduled' OR STATUS = 'started') AND END <= ?", nativeQuery = true)
    void setFinished(Date now);

    @Query(value = "SELECT E.* FROM EVENT E WHERE E.EXCHANGE_INTEGRATION_ID = ?1", nativeQuery = true)
    List<Event> findAllByExchangeIntegrationId(String exchangeIntegrationId);
}
