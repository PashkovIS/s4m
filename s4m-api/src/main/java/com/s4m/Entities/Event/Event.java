package com.s4m.Entities.Event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.s4m.Entities.EventResource.EventResource;
import com.s4m.Entities.JpaConverterJson;
import com.s4m.Entities.User.User;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
@Table(name = "EVENT")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Event {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private Instant created;

    @Column
    private Long createdByUserId;

    @Column
    private String title;

    @Column
    private String description;

    @Column
    private String source;

    @Column
    private Instant start;

    @Column
    private Instant end;

    @Column
    private Boolean allDay;

    //@Column(name = "during", columnDefinition = "tsrange", nullable = false)
    //private String during;
    //public String getDuring()
    //{
    //    return "[" + getStart().toString() + "," + getEnd().toString() + ")";
    //}
    //@Column(columnDefinition = "tsrange", nullable = false)
    //private String during;

    @Column
    private String status;

    @Column
    private long spaceId;

    @Column
    private Long organizerUserId;

    @Column
    private String organizerEmail;

    @Column
    private String organizerFullName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organizerUserId", insertable = false, updatable = false)
    @JsonIgnore
    private User organizerUser;

    public String getOrganizerImageUrl() {
        if (organizerUser == null) return null;
        return organizerUser.getImageUrl();
    }

    @Column
    private String greeting;

    @Column
    private boolean privateEvent = false;

    @Column
    private String exchangeIntegrationId = null;

    @Column(unique = true, updatable = false)
    @JsonIgnore
    private String exchangeIntegrationCorelation = null;

    @Column(columnDefinition = "TEXT")
    @Convert(converter = JpaConverterJson.class)
    private List<EventResource> resources;
}
