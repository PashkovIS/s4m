package com.s4m.Entities.Event;

import com.google.common.collect.Lists;
import com.s4m.API.WebApi.Auth.UserAuthException;
import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.API.WebApi.DTO.EventAttendeeCreateRequest;
import com.s4m.API.WebApi.DTO.EventCreateRequest;
import com.s4m.API.WebApi.DTO.EventUpdateRequest;
import com.s4m.Background.ExchangeIntegration;
import com.s4m.Common.BadRequestException;
import com.s4m.Common.ConflictException;
import com.s4m.Common.EntityNotFoundException;
import com.s4m.Entities.EventAttendee.EventAttendee;
import com.s4m.Entities.EventAttendee.EventAttendeeRepository;
import com.s4m.Entities.EventResource.EventResource;
import com.s4m.Entities.EventResource.EventResourceAttribute;
import com.s4m.Entities.Location.Location;
import com.s4m.Entities.Location.LocationRepository;
import com.s4m.Entities.Mail.Mail;
import com.s4m.Entities.Mail.MailService;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.Space.SpaceRepository;
import com.s4m.Entities.User.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class EventService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EventAttendeeRepository eventAttendeeRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ExchangeIntegration exchangeIntegration;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private MailService mailService;

    public boolean hasConflict(Event event) {
        Long spaceId = event.getSpaceId();
        Long eventId = event.getId();
        Instant start = event.getStart();
        Instant end = event.getEnd();

        List<Event> events = eventRepository.getOverlappingBySpaceId(spaceId, eventId, Date.from(start), Date.from(end));

        return !events.isEmpty();
    }

    // TODO: Remove EventCreateRequest from Entity service method signature or move it into Entity package.
    @Transactional
    public Event create(User organizer, Long spaceId, String source, EventCreateRequest request) {

        Space space = spaceRepository.findOne(spaceId);

        if (space == null) {
            throw new BadRequestException();
        }

        Event event = new Event();
        event.setCreated(Instant.now());
        if (organizer != null) event.setCreatedByUserId(organizer.getId());
        event.setTitle(request.getTitle());
        event.setDescription(request.getDescription());
        event.setSource(source);
        event.setStatus("scheduled");
        event.setSpaceId(spaceId);
        event.setGreeting(request.getGreeting());
        event.setPrivateEvent(request.isPrivateEvent());
        event.setAllDay(request.getAllDay());
        event.setResources(request.getResources());

        Boolean allDay = request.getAllDay();

        Instant start = request.getStart();
        Instant end;

        if (allDay == null || !allDay) {
            start = start.truncatedTo(ChronoUnit.MINUTES);

            end = request.getEnd();
            end = end.truncatedTo(ChronoUnit.MINUTES);

            if (end.equals(start)) {
                throw new BadRequestException();
            }

            if (end.isBefore(start)) {
                throw new BadRequestException();
            }
        } else {
            Location location = locationRepository.findOne(space.getLocationId());

            if (location == null) {
                throw new BadRequestException();
            }

            String timezoneId = location.getTimezoneId();
            if (timezoneId != null && !timezoneId.equals("")) {
                ZoneId zoneId = ZoneId.of(timezoneId);
                ZoneOffset offset = zoneId.getRules().getOffset(start);
                start = LocalDateTime.ofInstant(start, zoneId).truncatedTo(ChronoUnit.DAYS).toInstant(offset);
            } else {
                start = start.truncatedTo(ChronoUnit.DAYS);
            }

            end = start.plus(1, ChronoUnit.DAYS);
        }

        event.setStart(start);
        event.setEnd(end);

        Long organizerUserId = request.getOrganizerUserId();
        if (organizerUserId == null) {
            String organizerEmail = request.getOrganizerEmail();
            if (organizerEmail == null || organizerEmail.equals("")) throw new BadRequestException();
            event.setOrganizerEmail(organizerEmail);
            event.setOrganizerFullName(request.getOrganizerFullName());
        } else {
            User organizerUser = userRepository.findOne(organizerUserId);
            if (organizerUser == null) throw new BadRequestException();
            event.setOrganizerUserId(organizerUserId);
            event.setOrganizerEmail(organizerUser.getEmail());
            event.setOrganizerFullName(organizerUser.getFullName());
        }

        List<EventAttendee> attendees = new LinkedList<EventAttendee>();

        List<EventAttendeeCreateRequest> attendeesRequest = request.getAttendees();
        if (attendeesRequest != null && !attendeesRequest.isEmpty()) {
            for (EventAttendeeCreateRequest eacr : attendeesRequest) {
                attendees.add(getAttendeeFromRequest(eacr));
            }
        }

        if (exchangeIntegration != null) {
            if (!exchangeIntegration.createAppointment(space, event, attendees, true)) {
                throw new ConflictException();
            }

            try {
                eventRepository.save(event);
            } catch (Exception e) {
                // TODO: Suppress only UniqueConstraintvalitionException from database here.
                log.error("Exception thrown after ExchangeIntegration completed successfully", e);
            }
        } else {
            if (hasConflict(event)) {
                throw new ConflictException();
            }
            eventRepository.save(event);
        }


        for (EventAttendee attendee : attendees) {
            attendee.setEventId(event.getId());
            eventAttendeeRepository.save(attendee);
        }

        return event;
    }

    @Transactional
    public Event update(Long itemId, EventUpdateRequest request) {
        User authenticatedUser = userAuthService.getAuthenticatedUser();
        if (authenticatedUser == null) throw new UserAuthException();

        if (!eventRepository.exists(itemId)) {
            throw new EntityNotFoundException();
        }

        Event event = eventRepository.findOne(itemId);

        // If event is finished or cancelled - update is forbidden
        if (!event.getStatus().equals("scheduled") && !event.getStatus().equals("started")) {
            throw new BadRequestException();
        }

        Space space = spaceRepository.findOne(event.getSpaceId());

        event.setGreeting(request.getGreeting());

        // Cannot change this field on started events
        if (event.getStatus().equals("scheduled")) event.setAllDay(request.getAllDay());
        Boolean allDay = event.getAllDay();

        // Cannot change this field on started events
        Instant start = event.getStart();
        if (event.getStatus().equals("scheduled")) start = request.getStart();

        Instant end;

        if (allDay == null || !allDay) {
            start = start.truncatedTo(ChronoUnit.MINUTES);

            end = request.getEnd();
            end = end.truncatedTo(ChronoUnit.MINUTES);

            if (end.equals(start)) {
                throw new BadRequestException();
            }

            if (end.isBefore(start)) {
                throw new BadRequestException();
            }
        } else {
            Location location = locationRepository.findOne(space.getLocationId());

            if (location == null) {
                throw new BadRequestException();
            }

            String timezoneId = location.getTimezoneId();
            if (timezoneId != null && !timezoneId.equals("")) {
                ZoneId zoneId = ZoneId.of(timezoneId);
                ZoneOffset offset = zoneId.getRules().getOffset(start);
                start = LocalDateTime.ofInstant(start, zoneId).truncatedTo(ChronoUnit.DAYS).toInstant(offset);
            } else {
                start = start.truncatedTo(ChronoUnit.DAYS);
            }

            end = start.plus(1, ChronoUnit.DAYS);
        }

        if (Instant.now().isAfter(end)) {
            event.setStatus("finished");
        }

        Instant savedStart = event.getStart();
        Instant savedEnd = event.getEnd();

        event.setStart(start);
        event.setEnd(end);

        long userId = authenticatedUser.getId();
        boolean isCreator = (event.getCreatedByUserId() != null && userId == event.getCreatedByUserId());
        boolean isOrganizer = (event.getOrganizerUserId() != null && userId == event.getOrganizerUserId());
        boolean isAdmin = roleService.isAdministrator(userId);

        // Only creator, organizer and admin can edit privateEvent flag
        if (isCreator || isOrganizer || isAdmin) {
            event.setPrivateEvent(request.isPrivateEvent());
        }

        List<EventAttendee> eventAttendees = eventAttendeeRepository.findByEventId(event.getId());

        // Only creator, organizer and admin can edit this fields for private events
        if (!event.isPrivateEvent() || isCreator || isOrganizer || isAdmin) {
            event.setTitle(request.getTitle());
            event.setDescription(request.getDescription());

            // Attendees
            if (eventAttendees == null) eventAttendees = new ArrayList<>();
            Map<String, EventAttendee> mapCurrent = new HashMap<>();
            for (EventAttendee x : eventAttendees) {
                mapCurrent.put(x.getEmail().toLowerCase(), x);
            }

            List<EventAttendeeCreateRequest> requestAttendees = request.getAttendees();
            if (requestAttendees == null) requestAttendees = new ArrayList<>();
            Map<String, EventAttendeeCreateRequest> mapRequest = new HashMap<>();
            for (EventAttendeeCreateRequest x : requestAttendees) {
                mapRequest.put(x.getEmail().toLowerCase(), x);
            }

            for (EventAttendee x : eventAttendees) {
                if (!mapRequest.containsKey(x.getEmail().toLowerCase())) {
                    eventAttendeeRepository.delete(x);
                }
            }

            for (EventAttendeeCreateRequest x : requestAttendees) {
                if (!mapCurrent.containsKey(x.getEmail().toLowerCase())) {
                    EventAttendee attendee = getAttendeeFromRequest(x);
                    attendee.setEventId(event.getId());
                    eventAttendeeRepository.save(attendee);
                }
            }
        }

        if (hasConflict(event)) {
            throw new ConflictException();
        }

        if (exchangeIntegration != null) {
            if (!exchangeIntegration.updateAppointment(space, event, eventAttendees, true)) {

                event.setStart(savedStart);
                event.setEnd(savedEnd);

                if (!exchangeIntegration.updateAppointment(space, event, eventAttendees, true)) {

                    Location location = locationRepository.findOne(space.getLocationId());

                    Context context = new Context(Locale.forLanguageTag("ru"));
                    context.setVariable("locationName", location.getName());
                    context.setVariable("spaceName", space.getName());
                    context.setVariable("eventStart", Date.from(event.getStart().atZone(ZoneId.of(location.getTimezoneId())).toInstant())); // WTF?!
                    context.setVariable("eventTitle", event.getTitle());
                    context.setVariable("organizerName", event.getOrganizerFullName());
                    context.setVariable("organizerMail", event.getOrganizerEmail());

                    String body = templateEngine.process("conflict", context);

                    Mail mail = new Mail();
                    mail.setRecipient(location.getServiceEmail());
                    mail.setSubject("Конфликт при переносе встречи");
                    mail.setHtml(body);
                    mail.setTimestampCreated(Instant.now());

                    mailService.enqueue(mail);
                }

                throw new ConflictException();
            }
        }

        eventRepository.save(event);

        return event;
    }

    private EventAttendee getAttendeeFromRequest(EventAttendeeCreateRequest request) {
        EventAttendee eventAttendee = new EventAttendee();

        Long userId = request.getUserId();
        if (userId != null) {
            User organizer = userRepository.findOne(userId);
            if (organizer == null) throw new BadRequestException();

            eventAttendee.setUserId(userId);
            eventAttendee.setFullName(organizer.getFullName());
            eventAttendee.setEmail(organizer.getEmail());
        } else {
            eventAttendee.setFullName(request.getFullName());
            eventAttendee.setEmail(request.getEmail());
        }

        return eventAttendee;
    }

    public List<Event> unbookExpired(int autounbookTimeoutSeconds) {
        // TODO: Find a way to use SQL 'UPDATE ... RETURN UPDATED' here. Postgresql can handle it natively.

        Instant threshold = Instant.now().minus(autounbookTimeoutSeconds, ChronoUnit.SECONDS);

        List<Event> unbooked = new LinkedList<Event>();
        List<Event> unbookable = eventRepository.findUnbookable(Date.from(threshold));

        if (unbookable != null && unbookable.size() > 0) {
            for (Event event : unbookable) {
                event.setStatus("cancelled");

                if (exchangeIntegration != null) {
                    exchangeIntegration.cancelAppointment(spaceRepository.findOne(event.getSpaceId()), event, true);
                }

                eventRepository.save(event);
                unbooked.add(event);
            }
        }

        return unbooked;
    }

    public void setFinished() {
        eventRepository.setFinished(Date.from(Instant.now()));
    }

    public Event setEventFieldAccessForUser(long userId, Event event) {
        List<Event> list = new ArrayList<>();
        list.add(event);
        return setEventFieldAccessForUser(userId, list).get(0);
    }

    public List<Event> setEventFieldAccessForUser(long userId, List<Event> events) {
        // Prepare user roles map
        List<Role> roles = roleRepository.findByUserId(userId);
        Map<String, Boolean> rolesMap = new HashMap<>();
        for (Role r : roles) {
            String key = r.getLocation() == null ? r.getRole() : r.getRole() + '/' + r.getLocation();
            rolesMap.put(key, true);
        }

        if (rolesMap.containsKey(Role.ADMINISTRATOR)) return events;

        // Prepare space-to-location map
        List<Space> spaces = Lists.newArrayList(spaceRepository.findAll());
        Map<Long, Long> spacesMap = new HashMap<>();
        for (Space s : spaces) {
            spacesMap.put(s.getId(), s.getLocationId());
        }

        for (Event event : events) {
            if (event.isPrivateEvent()) {
                boolean isCreator = (event.getCreatedByUserId() != null && userId == event.getCreatedByUserId());
                boolean isOrganizer = (event.getOrganizerUserId() != null && userId == event.getOrganizerUserId());

                boolean hasFullAccess = (isCreator || isOrganizer);
                if (!hasFullAccess) {
                    event.setTitle(null);
                    event.setDescription(null);

                    Long locationId = spacesMap.get(event.getSpaceId());
                    String key = Role.OFFICE_MANAGER + '/' + locationId;
                    if (!rolesMap.containsKey(key)) {
                        event.setOrganizerFullName(null);
                        event.setOrganizerEmail(null);
                        event.setOrganizerUserId(null);
                        event.setCreatedByUserId(null);
                    }
                }
            }
        }

        return events;
    }

    public boolean canEdit(long userId, Event event) {
        boolean isCreator = (event.getCreatedByUserId() != null && userId == event.getCreatedByUserId());
        boolean isOrganizer = (event.getOrganizerUserId() != null && userId == event.getOrganizerUserId());
        if (isCreator || isOrganizer) return true;

        boolean isAdmin = roleService.isAdministrator(userId);
        if (isAdmin) return true;

        Space space = spaceRepository.findOne(event.getSpaceId());
        return roleService.isOfficeManagerFor(userId, space.getLocationId());
    }

    public void notifyOnResourceOrder(Event event, Space space, Location location, List<EventResource> eventResources) {
        if (eventResources == null || eventResources.isEmpty()) return;

        String serviceEmail = location.getServiceEmail();
        if (serviceEmail == null || serviceEmail.equals("")) return;

        Context context = new Context(Locale.forLanguageTag("ru"));
        context.setVariable("locationName", location.getName());
        context.setVariable("spaceName", space.getName());
        context.setVariable("eventStart", java.util.Date.from(event.getStart().atZone(ZoneId.of(location.getTimezoneId())).toInstant())); // WTF?!
        context.setVariable("eventPrivate", event.isPrivateEvent());
        context.setVariable("eventTitle", event.getTitle());
        context.setVariable("organizerName", event.getOrganizerFullName());
        context.setVariable("organizerMail", event.getOrganizerEmail());

        List<String> orders = new LinkedList<>();

        for (EventResource resource : eventResources) {
            StringBuilder builder = new StringBuilder();
            builder.append(resource.getName());

            List<EventResourceAttribute> attributes = resource.getAttributes();
            if (attributes != null) {
                builder.append("<ul>");
                for (EventResourceAttribute era : attributes) {
                    Integer count = era.getCount();
                    if (count != null && count > 0) {
                        builder.append("<li>");
                        builder.append(era.getName());
                        builder.append(" (");
                        builder.append(count);
                        builder.append(")</li>");
                    }
                }
                builder.append("</ul>");
            }

            orders.add(builder.toString());
        }

        context.setVariable("orders", orders);

        String body = templateEngine.process("order", context);

        Mail mail = new Mail();
        mail.setRecipient(serviceEmail);
        mail.setSubject("Заказ для встречи");
        mail.setHtml(body);
        mail.setTimestampCreated(Instant.now());

        mailService.enqueue(mail);
    }
}
