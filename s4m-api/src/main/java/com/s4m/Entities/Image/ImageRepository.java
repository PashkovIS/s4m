package com.s4m.Entities.Image;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ImageRepository extends CrudRepository<Image, Long> {
    @Query(value = "SELECT * FROM IMAGE WHERE type = ?1 AND reference = ?2", nativeQuery = true)
    List<Image> findByTypeAndReference(String type, long reference);
}
