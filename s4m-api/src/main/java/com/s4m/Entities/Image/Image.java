package com.s4m.Entities.Image;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

@Entity
@Data
@Table(name = "IMAGE")
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class Image {
    @Column(nullable = false, insertable = false, updatable = false)
    @Id
    @GeneratedValue
    @JsonProperty()
    private long id;

    @Column(insertable = false, updatable = false)
    @CreatedDate
    @JsonProperty
    private Date created;

    @Column(nullable = false)
    @JsonProperty()
    private String type;

    @Column(nullable = false)
    @JsonProperty()
    private long reference;

    @Column(nullable = false)
    @JsonProperty()
    private String fileName;
}
