package com.s4m.Entities.Image;

import ecstaj.ArgumentFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Service
public class ImageService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static String baseUrl;

    @Value("${app.storage.baseUrl}")
    public void setBaseUrl(String url) {
        baseUrl = url;
    }

    public static String getBaseUrl() {
        if (!baseUrl.endsWith("/")) baseUrl += "/";
        return baseUrl;
    }

    @Autowired
    private ImageRepository _imageRepository;

    @Value("${app.storage.directory}")
    private String _storage;

    public List<Image> findByTypeAndReference(String type, long reference) {
        List<Image> images = _imageRepository.findByTypeAndReference(type, reference);

        if (images == null) {
            return Arrays.asList(new Image[]{});
        } else {
            return images;
        }
    }

    public Image findById(long id) {
        return _imageRepository.findOne(id);
    }

    public Image create(String type, long reference, String filename, InputStream input) throws IOException {
        // TODO: Improve directory structure.
        // TODO: Add separate file manager (?).
        // TODO: Read image parameters.
        // TODO: Add image crop and different sizes. Use ImageMagick?

        // Get Image file name
        String[] split = filename.split("\\.");
        if (split.length < 2) {
            throw new ArgumentFormatException("No file extension provided");
        }
        String imageFileName = UUID.randomUUID().toString() + "." + split[split.length - 1];


        // Get path to storage
        Path path = Paths.get(_storage);
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (FileAlreadyExistsException e) {
                // Do nothing
            }
        }
        path = path.resolve(imageFileName);

        // Save file to storage
        try (OutputStream output = Files.newOutputStream(path, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
            byte[] tmp = new byte[64 * 1024];

            int l = 0;
            int n = 0;

            while ((l = input.read(tmp)) > 0) {
                output.write(tmp, 0, l);
                n += l;
            }

            log.info("File '{}' uploaded to '{}' ({} bytes written)", filename, path.toString(), n);
        }

        Image image = new Image();
        image.setType(type);
        image.setReference(reference);
        image.setFileName(imageFileName);

        _imageRepository.save(image);

        return image;
    }

    public List<Image> deleteByTypeAndReference(String type, long reference) throws IOException {
        List<Image> deletable = _imageRepository.findByTypeAndReference(type, reference);
        List<Image> deleted = new LinkedList<Image>();
        Path path = Paths.get(_storage);

        for (Image image : deletable) {
            Path imagePath = path.resolve(image.getFileName());
            if (Files.deleteIfExists(imagePath)) {
                deleted.add(image);
            }

            _imageRepository.delete(image.getId());
        }

        return deleted;
    }

    public Image deleteById(long id) throws IOException {
        Image deletable = _imageRepository.findOne(id);
        Path path = Paths.get(_storage);

        if (deletable != null) {
            Path imagePath = path.resolve(deletable.getFileName());
            if (Files.deleteIfExists(imagePath)) {
                return deletable;
            }
        }

        return null;
    }

    public byte[] load(String fileName) throws IOException {
        Path path = Paths.get(_storage);
        path = path.resolve(fileName);
        byte[] data = new byte[(int) Files.size(path)];

        try (InputStream input = Files.newInputStream(path)) {
            input.read(data, 0, data.length);
        }

        return data;
    }
}
