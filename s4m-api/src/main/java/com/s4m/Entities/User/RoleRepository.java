package com.s4m.Entities.User;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoleRepository extends CrudRepository<Role, Long> {
    @Query(value = "SELECT * FROM ROLE WHERE USER_ID = ?1", nativeQuery = true)
    List<Role> findByUserId(long userId);

    @Query(value = "SELECT * FROM ROLE WHERE USER_ID = ?1 AND ROLE = ?2", nativeQuery = true)
    List<Role> findByUserIdAndRole(long userId, String role);

    @Query(value = "DELETE FROM ROLE WHERE USER_ID = ?1 AND ROLE = ?2", nativeQuery = true)
    List<Role> deleteByUserIdAndRole(long userId, String role);

    @Query(value = "SELECT * FROM ROLE WHERE USER_ID = ?1 AND ROLE = ?2 AND LOCATION = ?3", nativeQuery = true)
    List<Role> findByUserIdAndRoleAndLocation(long userId, String role, long location);

    @Query(value = "DELETE FROM ROLE WHERE USER_ID = ?1 AND ROLE = ?2 AND LOCATION = ?3", nativeQuery = true)
    List<Role> deleteByUserIdAndRoleAndLocation(long userId, String role, long location);
}
