package com.s4m.Entities.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.s4m.Entities.Image.ImageService;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "USER")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String login;

    @Column
    private String fullName;

    @Column
    private String email;

    @JsonIgnore
    @Column
    private String passwordHash;

    @JsonIgnore
    @Column
    private String salt;

    @Column
    @JsonIgnore
    private String image;

    public String getImageUrl() {
        if (image == null) return null;
        return ImageService.getBaseUrl() + image;
    }
}
