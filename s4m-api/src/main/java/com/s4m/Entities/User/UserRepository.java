package com.s4m.Entities.User;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    @Query(value = "SELECT * FROM USER WHERE LOGIN = ?1", nativeQuery = true)
    List<User> findByLogin(String login);

    @Query(value = "SELECT * FROM USER WHERE EMAIL = ?1", nativeQuery = true)
    User findOneByEmail(String email);

    @Query(value = "SELECT * FROM USER WHERE lower(FULL_NAME) LIKE ?1 OR lower(EMAIL) LIKE ?1", nativeQuery = true)
    List<User> search(String searchSring);
}
