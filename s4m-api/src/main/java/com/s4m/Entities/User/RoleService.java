package com.s4m.Entities.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {

    @Autowired
    private RoleRepository _roleRepository;

    public List<Role> listRoles (long userId)
    {
        return _roleRepository.findByUserId(userId);
    }

    public void unsetRole (long roleId)
    {
        _roleRepository.delete(roleId);
    }

    public Role setAdministrator (long userId) {
        List<Role> roles = _roleRepository.findByUserIdAndRole(userId, Role.ADMINISTRATOR);
        if (roles.isEmpty()) {
            Role role = new Role();
            role.setUserId(userId);
            role.setRole(Role.ADMINISTRATOR);

            _roleRepository.save(role);

            return role;
        } else {
            return roles.get(0);
        }
    }

    public void unsetAdministrator(long userId) {
        _roleRepository.deleteByUserIdAndRole(userId, Role.ADMINISTRATOR);
    }

    public boolean isAdministrator (long userId) {
        List<Role> roles = _roleRepository.findByUserIdAndRole(userId, Role.ADMINISTRATOR);

        if (roles.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public Role setOfficeManagerFor (long userId, long location) {
        List<Role> roles = _roleRepository.findByUserIdAndRoleAndLocation(userId, Role.OFFICE_MANAGER, location);

        if (roles.isEmpty()) {
            Role role = new Role();
            role.setUserId(userId);
            role.setRole(Role.OFFICE_MANAGER);
            role.setLocation(location);

            _roleRepository.save(role);

            return role;
        } else {
            return roles.get(0);
        }
    }

    public void unsetOfficeManagerFor (long userId, long location) {
        _roleRepository.deleteByUserIdAndRoleAndLocation(userId, Role.OFFICE_MANAGER, location);
    }

    public void unsetOfficeManager (long userId) {
        _roleRepository.deleteByUserIdAndRole(userId, Role.OFFICE_MANAGER);
    }

    public boolean isOfficeManagerFor (long userId, long location) {
        List<Role> roles = _roleRepository.findByUserIdAndRoleAndLocation(userId, Role.OFFICE_MANAGER, location);

        if (roles.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
}
