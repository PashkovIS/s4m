package com.s4m.Entities.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "ROLE")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Role {
    public static final String ADMINISTRATOR = "admin";
    public static final String OFFICE_MANAGER = "office manager";

    @Column(nullable = false, insertable = false, updatable = false)
    @Id
    @GeneratedValue
    @JsonProperty
    private long id;

    @Column(nullable = false, updatable = false)
    @JsonIgnore
    private long userId;

    @Column(nullable = false, updatable = false)
    private String role;

    @Column
    private Long location;
}
