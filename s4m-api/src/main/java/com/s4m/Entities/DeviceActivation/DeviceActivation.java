package com.s4m.Entities.DeviceActivation;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "DEVICE_ACTIVATION")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeviceActivation {

    @Id
    @GeneratedValue
    private String id;

    @Column
    private String status;

    @Column
    private String pin;

    @Column
    private String authToken;

    @Column(name = "created", updatable = false)
    private Instant created;
}
