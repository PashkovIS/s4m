package com.s4m.Entities.DeviceActivation;

import lombok.Data;

@Data
public class DeviceActivationConfirmRequest {
    private Long spaceId;
    private String activationPin;
    private String deviceName;
}
