package com.s4m.Entities.DeviceActivation;

import lombok.Data;

@Data
public class DeviceActivationRequestResult {
    private String authToken;
    private String activationPin;
}
