package com.s4m.Entities.DeviceActivation;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DeviceActivationRepository extends CrudRepository<DeviceActivation, Long> {
    @Query(value = "SELECT * FROM DEVICE_ACTIVATION WHERE PIN = ?1 AND STATUS = 'waitForActivation'", nativeQuery = true)
    List<DeviceActivation> findByPin(String pin);

    @Query(value = "SELECT * FROM DEVICE_ACTIVATION WHERE AUTH_TOKEN = ?1 AND STATUS = 'waitForActivation'", nativeQuery = true)
    List<DeviceActivation> findByToken(String token);
}
