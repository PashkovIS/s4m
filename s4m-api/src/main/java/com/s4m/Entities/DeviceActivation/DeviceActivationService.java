package com.s4m.Entities.DeviceActivation;

import com.s4m.Common.EntityNotFoundException;
import com.s4m.Entities.Device.Device;
import com.s4m.Entities.Device.DeviceRepository;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.Space.SpaceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j
@Service
public class DeviceActivationService {
    @Autowired
    private DeviceActivationRepository repository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private SpaceRepository spaceRepository;

    @Value("${app.device.pinSymbols}")
    private String pinSymbols;

    @Value("${app.device.pinLength}")
    private Integer pinLength;

    private String generateAuthToken() {
        String authToken = UUID.randomUUID().toString();
        return authToken;
    }

    private String generateActivationPin() {
        StringBuilder builder = new StringBuilder();
        int maxIndex = pinSymbols.length();

        for (int i = 0; i < pinLength; ++i) {
            int randomIndex = ThreadLocalRandom.current().nextInt(0, maxIndex);

            char symbol = pinSymbols.charAt(randomIndex);
            builder.append(symbol);
        }

        return builder.toString();
    }

    public DeviceActivationRequestResult requestActivation() {
        String authToken = generateAuthToken();
        String activationPin = generateActivationPin();

        DeviceActivation newActivationRequest = new DeviceActivation();
        newActivationRequest.setCreated(Instant.now());
        newActivationRequest.setStatus("waitForActivation");
        newActivationRequest.setAuthToken(authToken);
        newActivationRequest.setPin(activationPin);

        repository.save(newActivationRequest);

        DeviceActivationRequestResult resp = new DeviceActivationRequestResult();
        resp.setAuthToken(authToken);
        resp.setActivationPin(activationPin);
        return resp;
    }

    private DeviceActivation getDeviceActivationByPin(String activationPin) {
        List<DeviceActivation> deviceActivations = repository.findByPin(activationPin);
        if (deviceActivations.isEmpty()) throw new EntityNotFoundException();
        return deviceActivations.get(0);
    }

    @Value("${app.device.activationTimeoutMinutes}")
    private int ACTIVATION_REQUEST_TIMEOUT_MINUTES;

    public Device confirmActivation(DeviceActivationConfirmRequest request) {
        Space space = spaceRepository.findOne(request.getSpaceId());

        if (space == null) {
            throw new EntityNotFoundException();
        }

        DeviceActivation deviceActivation = getDeviceActivationByPin(request.getActivationPin());

        if (deviceActivation.getCreated().plus(ACTIVATION_REQUEST_TIMEOUT_MINUTES, ChronoUnit.MINUTES).isBefore(Instant.now())) {
            deviceActivation.setStatus("expired");
            repository.save(deviceActivation);

            throw new EntityNotFoundException();
        } else {
            deviceActivation.setStatus("activated");
            repository.save(deviceActivation);
        }

        Device device = new Device();
        device.setAuthToken(deviceActivation.getAuthToken());
        device.setSpaceId(request.getSpaceId());
        device.setCreated(Instant.now());
        device.setName(request.getDeviceName());
        deviceRepository.save(device);

        return device;
    }
}
