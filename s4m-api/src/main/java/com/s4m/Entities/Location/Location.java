package com.s4m.Entities.Location;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.s4m.Entities.Organization.Organization;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "LOCATION")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Location {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String address;

    @Column
    private Float latitude;

    @Column
    private Float longitude;

    @Column
    private Float altitude;

    @Column
    private long organizationId;

    @Column
    private String timezoneId;

    @Column
    private String serviceEmail;
}
