package com.s4m.Entities.Location;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LocationRepository extends CrudRepository<Location, Long> {
    @Query(value = "SELECT * FROM LOCATION WHERE ORGANIZATION_ID = ?1", nativeQuery = true)
    List<Location> findByOrganizationId(Long organizationId);
}
