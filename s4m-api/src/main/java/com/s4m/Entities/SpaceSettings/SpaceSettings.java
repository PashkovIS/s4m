package com.s4m.Entities.SpaceSettings;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SPACE_SETTINGS")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SpaceSettings {
    @Id
    @JsonIgnore
    private long spaceId;

    @Column
    private boolean autoUnbook;

    @Column
    private boolean maskMeetingTitles;

    @Column
    private boolean hideMeetingAttendees;

    @Column
    private boolean keepMeetingOrganizerVisible;

    @Column
    private boolean darkenBackgroundImage;

    @Column
    private boolean useLightText;

    @Column
    private boolean highVisibilityMode;

    @Column
    private boolean showFrame;

    @Column
    private boolean showTimeline;

    @Column
    private boolean adminOnly;

    @Column
    private String statusBarColor = "black";

    @Column
    private boolean transparentTopBar = true;
}
