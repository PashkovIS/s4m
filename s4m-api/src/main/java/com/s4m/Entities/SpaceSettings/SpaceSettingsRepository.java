package com.s4m.Entities.SpaceSettings;

import org.springframework.data.repository.CrudRepository;

public interface SpaceSettingsRepository extends CrudRepository<SpaceSettings, Long> {
}
