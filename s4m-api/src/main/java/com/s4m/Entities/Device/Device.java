package com.s4m.Entities.Device;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "DEVICE")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Device {
    @Id
    @GeneratedValue
    private long id;

    @JsonIgnore
    @Column
    private String authToken;

    @Column
    private long spaceId;

    @Column
    private Instant created;

    @Column
    private Instant lastAccessed;

    @Column
    private String name;
}
