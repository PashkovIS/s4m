package com.s4m.Entities.Device;

import com.s4m.Entities.Device.Device;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DeviceRepository extends CrudRepository<Device, Long> {
    @Query(value = "SELECT * FROM DEVICE WHERE SPACE_ID = ?1", nativeQuery = true)
    List<Device> findBySpaceId(Long spaceId);

    @Query(value = "SELECT * FROM DEVICE WHERE AUTH_TOKEN = ?1", nativeQuery = true)
    List<Device> findByAuthToken(String authToken);
}
