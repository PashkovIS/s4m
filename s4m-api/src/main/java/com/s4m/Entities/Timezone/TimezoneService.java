package com.s4m.Entities.Timezone;

import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;

@Service
public class TimezoneService {

    private final Collection<ZoneId> AVAILABLE;

    public TimezoneService() {
        Set<ZoneId> all = new HashSet<ZoneId>();

        for (String zone : ZoneId.getAvailableZoneIds()) {
            if (zone.contains("Etc/GMT") ||
                    zone.contains("SystemV") ||
                    zone.contains("PST8PDT") ||
                    zone.contains("CST6CDT") ||
                    zone.contains("EST5EDT") ||
                    zone.contains("WET") ||
                    zone.contains("Universal") ||
                    zone.contains("UTC") ||
                    zone.contains("UCT") ||
                    zone.contains("GMT") ||
                    zone.contains("MET") ||
                    zone.contains("CET") ||
                    zone.contains("EET") ||
                    zone.contains("W-SU") ||
                    zone.contains("PRC") ||
                    zone.contains("ROK") ||
                    zone.contains("NZ")) {
                continue;
            }

            all.add(ZoneId.of(zone));
        }

        AVAILABLE = Collections.unmodifiableCollection(all);
    }

    public List<Timezone> getAllZonesFor(Instant timestamp) {
        List<Timezone> result = new ArrayList<>();

        // Unable to cache the resulting list due to daylight saving time
        // rules in different time zones for particular timestamp

        for (ZoneId zoneId : AVAILABLE) {
            ZoneOffset offset = zoneId.getRules().getOffset(timestamp);

            Timezone timezone = new Timezone();
            timezone.setCode(zoneId.getId());
            timezone.setOffset(offset.toString().replaceAll("Z", "+00:00"));
            timezone.setSeconds(offset.getTotalSeconds());

            result.add(timezone);
        }

        result.sort((a, b) -> {
            int sign = Integer.signum(a.getSeconds() - b.getSeconds());

            if (sign != 0) {
                return sign;
            }

            return a.getCode().compareTo(b.getCode());
        });

        return result;
    }

    public List<Timezone> getAllZones() {
        return getAllZonesFor(Instant.now());
    }
}
