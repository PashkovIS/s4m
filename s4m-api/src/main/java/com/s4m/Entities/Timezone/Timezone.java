package com.s4m.Entities.Timezone;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class Timezone {
    private String code;
    private String offset;

    @JsonIgnore
    private int seconds;
}
