package com.s4m.Entities.Space;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface SpaceRepository extends CrudRepository<Space, Long> {
    @Query(value = "SELECT * FROM SPACE WHERE LOCATION_ID = ?1", nativeQuery = true)
    List<Space> findByLocationId(Long locationId);

    @Query(value = "SELECT S.* FROM SPACE S INNER JOIN LOCATION L ON S.LOCATION_ID = L.ID WHERE L.ORGANIZATION_ID = ?1", nativeQuery = true)
    List<Space> findByOrganizationId(Long organizationId);
}
