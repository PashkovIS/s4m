package com.s4m.Entities.Space;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.s4m.Entities.Image.ImageService;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "SPACE")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Space {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private long capacity;

    @Column
    private boolean disabled;

    @Column
    private long locationId;

    @Column
    private String spaceTypeId;

    @Column
    private Float discoveryRadius;

    @Column
    private String exchangeIntegrationEmail = null;

    @Column
    private String exchangeIntegrationSubscription = null;

    @Column
    @JsonIgnore
    private String image;

    public String getImageUrl() {
        if (image == null) return null;
        return ImageService.getBaseUrl() + image;
    }
}
