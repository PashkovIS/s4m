package com.s4m.Entities.UserSession;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "USER_SESSION")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserSession {
    @Id
    private String sessionId;

    @Column
    private Instant created;

    @Column
    private Instant lastAccessed;

    @Column
    private long userId;
}
