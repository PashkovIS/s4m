package com.s4m.Entities.EventResource;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.s4m.Entities.JpaConverterJson;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventResource {
    private String code;
    private String name;
    private List<EventResourceAttribute> attributes;
}
