package com.s4m.Entities.EventResource;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventResourceAttribute {
    private String code;
    private String name;
    private Integer count;
}
