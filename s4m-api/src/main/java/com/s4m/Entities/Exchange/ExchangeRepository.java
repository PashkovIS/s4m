package com.s4m.Entities.Exchange;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by nikolay on 4/8/17.
 */
public interface ExchangeRepository extends CrudRepository<ExchangeSync, Long> {

    @Query(value = "SELECT * FROM EXCHANGE_SYNC ORDER BY LAST_TIMESTAMP DESC", nativeQuery = true)
    List<ExchangeSync> findAllOrdered ();
}
