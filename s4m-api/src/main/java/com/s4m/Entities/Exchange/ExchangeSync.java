package com.s4m.Entities.Exchange;

import lombok.Data;
import lombok.Generated;

import javax.persistence.*;
import java.time.Instant;

/**
 * Created by nikolay on 4/8/17.
 */
@Data
@Entity
@Table(name = "EXCHANGE_SYNC")
public class ExchangeSync {
    @Id
    @GeneratedValue
    private long id;
    @Column
    private Instant lastTimestamp;
}
