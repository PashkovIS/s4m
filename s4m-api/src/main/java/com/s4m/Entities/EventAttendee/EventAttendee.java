package com.s4m.Entities.EventAttendee;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.s4m.Entities.User.User;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "EVENT_ATTENDEE")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventAttendee {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private long eventId;

    @Column
    private String fullName;

    @Column
    private String email;

    @Column
    private Long userId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", insertable = false, updatable = false)
    @JsonIgnore
    private User user;

    public String getImageUrl() {
        if (user == null) return null;
        return user.getImageUrl();
    }
}
