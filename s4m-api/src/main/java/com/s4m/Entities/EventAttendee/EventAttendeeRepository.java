package com.s4m.Entities.EventAttendee;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface EventAttendeeRepository extends CrudRepository<EventAttendee, Long> {
    @Query(value = "SELECT * FROM EVENT_ATTENDEE WHERE EVENT_ID = ?1", nativeQuery = true)
    List<EventAttendee> findByEventId(long eventId);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM EVENT_ATTENDEE WHERE EVENT_ID = ?1", nativeQuery = true)
    void deleteByEventId(long eventId);
}
