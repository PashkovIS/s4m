package com.s4m.Entities.Resource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.s4m.Entities.Image.ImageService;
import com.s4m.Entities.JpaConverterJson;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "RESOURCE")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Resource {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String code;

    @Column
    private String name;

    @Column(columnDefinition = "TEXT")
    @Convert(converter = JpaConverterJson.class)
    private List<ResourceAttribute> attributes;
    
    @Column
    @JsonIgnore
    private String image;
    
    public String getImageUrl() {
        if (image == null) return null;
        return ImageService.getBaseUrl() + image;
    }
}
