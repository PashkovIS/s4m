package com.s4m.Entities.Resource;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResourceAttribute {
    private String code;
    private String name;
}
