package com.s4m.Entities.Organization;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.s4m.Entities.Image.ImageService;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "ORGANIZATION")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Organization {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String name;

    @Column
    @JsonIgnore
    private String logo;

    @Column
    @JsonIgnore
    private String fullLogo;

    @Column
    @JsonIgnore
    private String roomDisplayLogo;

    public String getLogoUrl() {
        return logo == null ? null : ImageService.getBaseUrl() + logo;
    }

    public String getFullLogoUrl() {
        return fullLogo == null ? null : ImageService.getBaseUrl() + fullLogo;
    }

    public String getRoomDisplayLogoUrl() {
        return roomDisplayLogo == null ? null : ImageService.getBaseUrl() + roomDisplayLogo;
    }
}
