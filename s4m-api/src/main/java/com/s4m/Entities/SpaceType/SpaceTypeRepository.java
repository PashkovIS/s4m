package com.s4m.Entities.SpaceType;

import org.springframework.data.repository.CrudRepository;

public interface SpaceTypeRepository extends CrudRepository<SpaceType, String> {
}
