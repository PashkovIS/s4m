package com.s4m.Entities.SpaceType;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "SPACE_TYPE")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SpaceType {
    @Id
    private String id;

    @Column
    private String name;
}
