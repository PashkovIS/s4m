package com.s4m.Background;

import com.s4m.Entities.Event.Event;
import com.s4m.Entities.Event.EventRepository;
import com.s4m.Entities.EventAttendee.EventAttendee;
import com.s4m.Entities.EventAttendee.EventAttendeeRepository;
import com.s4m.Entities.Exchange.ExchangeRepository;
import com.s4m.Entities.Exchange.ExchangeSync;
import com.s4m.Entities.Space.Space;
import com.s4m.Entities.Space.SpaceRepository;
import com.s4m.Entities.User.User;
import com.s4m.Entities.User.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by nikolay on 3/29/17.
 */
@Component
@Conditional(ExchangeTaskActivator.class)
public class ExchangeTask {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final Executor _executor = Executors.newSingleThreadExecutor();

    @Autowired
    private ExchangeIntegration exchangeIntegration;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventAttendeeRepository eventAttendeeRepository;

    @Autowired
    private ExchangeRepository exchangeRepository;

    private final class EventHandler implements ExchangeIntegration.AppointmentEventHandler {
        @Override
        public void process(Space space, Event changed, List<EventAttendee> attendees) {
            Event event;

            List<Event> found = eventRepository.findAllByExchangeIntegrationId(changed.getExchangeIntegrationId());

            if (!found.isEmpty()) {
                event = found.get(0);
            }
            else {
                event = null;
            }

            if (event != null) {
                event.setTitle(changed.getTitle());
                event.setStart(changed.getStart());
                event.setEnd(changed.getEnd());

                String email = event.getOrganizerEmail();

                User user = userRepository.findOneByEmail(email);

                if (user != null) {
                    event.setOrganizerUserId(user.getId());
                    event.setOrganizerFullName(user.getFullName());
                }

                event.setOrganizerEmail(email);

                if ("cancelled".equals(changed.getStatus())) {
                    event.setStatus("cancelled");
                }
            } else {
                event = changed;

                String email = event.getOrganizerEmail();

                User user = userRepository.findOneByEmail(email);

                if (user != null) {
                    event.setOrganizerUserId(user.getId());
                    event.setOrganizerFullName(user.getFullName());
                }

                event.setSource("exchange");

                event.setStatus("scheduled");
            }

            if (event.getStart().isBefore(Instant.now())) {
                if (event.getEnd().isBefore(Instant.now())) {
                    event.setStatus("finished");
                } else {
                    if (!"cancelled".equals(event.getStatus())) {
                        event.setStatus("started");
                    }
                }
            }

            eventRepository.save(event);

            eventAttendeeRepository.deleteByEventId(event.getId());

            for (EventAttendee attendee: attendees) {
                attendee.setEventId(event.getId());

                String email = attendee.getEmail();

                User user = userRepository.findOneByEmail(email);

                if (user != null) {
                    attendee.setUserId(user.getId());
                    attendee.setFullName(user.getFullName());
                }

                eventAttendeeRepository.save(attendee);
            }
        }
    }

    private void poll (final Instant from, final Instant to) {

        try {
            exchangeIntegration.poll(spaceRepository.findAll(), Date.from(from), Date.from(to), new EventHandler());
        }
        catch (Exception e) {
            log.error("Some exception occured", e);
        }

        ExchangeSync sync = new ExchangeSync();
        sync.setLastTimestamp(to);
        exchangeRepository.save(sync);

        long delta = Instant.now().getEpochSecond() - to.getEpochSecond();

        if (delta < 15) {
            try {
                Thread.currentThread().sleep((15 - delta) * 1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt(); return;
            }
        }

        _executor.execute(new Runnable() {
            @Override
            public void run() {
                poll (to, Instant.now());
            }
        });
    }

    private void sync (final Instant now) {

        Instant from = now.minus(365, ChronoUnit.DAYS);

        List<ExchangeSync> syncs = exchangeRepository.findAllOrdered();
        if (!syncs.isEmpty()){
            from = syncs.get(0).getLastTimestamp();
        }

        exchangeIntegration.poll(spaceRepository.findAll(), Date.from(from), Date.from(now), new EventHandler());
    }

    public void start () {

        _executor.execute(new Runnable() {
            @Override
            public void run() {

                final Instant now = Instant.now();

                sync(now);

                // Wait 5 seconds before going forward

                try {
                    Thread.currentThread().sleep(5 * 1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt(); return;
                }

                _executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        poll (now, Instant.now());
                    }
                });
            }
        });
    }
}
