package com.s4m.Background;

import com.s4m.Application;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * Created by nikolay on 3/19/17.
 */
public class ExchangeTaskActivator implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        return Application.scheduler();
    }
}
