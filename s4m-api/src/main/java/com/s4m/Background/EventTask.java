package com.s4m.Background;

import com.s4m.Entities.Event.Event;
import com.s4m.Entities.Event.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Conditional(EventTaskActivator.class)
public class EventTask {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    // Naive implementation of unbooking background task

    @Value("${app.event.autoUnbookTimeoutMinutes}")
    private int AUTO_UNBOOK_TIMEOUT_MINUTES = 5;
    private int AUTO_UNBOOK_TIMEOUT_SECONDS = AUTO_UNBOOK_TIMEOUT_MINUTES * 60;

    @Autowired
    private EventService _eventService;

    @Scheduled(/*initialDelay = 5 * 60 * 1000,*/ fixedDelay = 60 * 1000)
    private void unbook() {
        log.debug("Unbook scheduled task started");

        List<Event> unbooked = _eventService.unbookExpired(AUTO_UNBOOK_TIMEOUT_SECONDS);

        if (unbooked.size() > 0) {
            for (Event event : unbooked) {
                log.info("Event {} was unbooked due to timeout expiration", event.getId());
            }
        }

        log.debug("Unbook scheduled task done");
    }

    @Scheduled(fixedDelay = 60 * 1000)
    private void finish() {
        _eventService.setFinished();
    }
}
