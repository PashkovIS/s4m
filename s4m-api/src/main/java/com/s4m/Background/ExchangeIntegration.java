package com.s4m.Background;

import com.s4m.Entities.Event.Event;
import com.s4m.Entities.EventAttendee.EventAttendee;
import com.s4m.Entities.Space.Space;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.misc.ConnectingIdType;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.notification.EventType;
import microsoft.exchange.webservices.data.core.enumeration.property.*;
import microsoft.exchange.webservices.data.core.enumeration.search.LogicalOperator;
import microsoft.exchange.webservices.data.core.enumeration.service.ConflictResolutionMode;
import microsoft.exchange.webservices.data.core.enumeration.service.SendInvitationsMode;
import microsoft.exchange.webservices.data.core.enumeration.service.SendInvitationsOrCancellationsMode;
import microsoft.exchange.webservices.data.core.service.item.Appointment;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.core.service.item.MeetingResponse;
import microsoft.exchange.webservices.data.core.service.schema.AppointmentSchema;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.misc.ImpersonatedUserId;
import microsoft.exchange.webservices.data.misc.OutParam;
import microsoft.exchange.webservices.data.notification.ItemEvent;
import microsoft.exchange.webservices.data.notification.PullSubscription;
import microsoft.exchange.webservices.data.property.complex.Attendee;
import microsoft.exchange.webservices.data.property.complex.FolderId;
import microsoft.exchange.webservices.data.property.complex.ItemId;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import microsoft.exchange.webservices.data.property.definition.ExtendedPropertyDefinition;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.ItemView;
import microsoft.exchange.webservices.data.search.filter.SearchFilter;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.time.Instant;
import java.util.*;

/**
 * Created by nikolay on 3/29/17.
 */
@Service
public class ExchangeIntegration {

    private static final UUID EXCHANGE_EXTENDED_PROPERTY_S4M_CORELATION_ID = UUID.fromString("00F4A153-A778-4B0F-B27D-B1062D62E5C1");
    private static final UUID EXCHANGE_EXTENDED_PROPERTY_S4M_ORGANIZER_ID = UUID.fromString("3C10C8F4-93D2-4E55-86BB-C592AE7A9AA8");
    private static final UUID EXCHANGE_EXTENDED_PROPERTY_S4M_ORGANIZER_NAME = UUID.fromString("D36147DF-7C2E-47EE-A9ED-1AB4CA4A0BA8");
    private static final UUID EXCHANGE_EXTENDED_PROPERTY_S4M_ORGANIZER_EMAIL = UUID.fromString("45533F17-967B-46C9-82DB-EED20A13A60B");

    @Value("${app.exchange.uri}")
    private String EXCHANGE_URI;
    @Value("${app.exchange.login}")
    private String EXCHANGE_LOGIN;
    @Value("${app.exchange.password}")
    private String EXCHANGE_PASSWORD;
    @Value("${app.exchange.domain}")
    private String EXCHANGE_DOMAIN;

    // Refer to https://github.com/OfficeDev/ews-java-api/wiki/Getting-Started-Guide#using-the-library

    public boolean createAppointment(Space space, Event event, Iterable<EventAttendee> attendees, boolean notify) {

        // Work with integrated spaces only

        if (space.getExchangeIntegrationEmail() == null) {
            return true;
        }

        event.setExchangeIntegrationCorelation (UUID.randomUUID().toString());

        try (ExchangeService exchange = new ExchangeService(ExchangeVersion.Exchange2010)) {
            exchange.setUrl(
                    new URI(EXCHANGE_URI));
            exchange.setCredentials(
                    new WebCredentials(EXCHANGE_LOGIN, EXCHANGE_PASSWORD));

            if (event.getOrganizerEmail() != null) {
                if (event.getOrganizerEmail().endsWith(EXCHANGE_DOMAIN)) {
                    exchange.setImpersonatedUserId(
                            new ImpersonatedUserId(ConnectingIdType.SmtpAddress, event.getOrganizerEmail()));
                }
            }

            PullSubscription subscription = null;

            try {
                subscription = exchange.subscribeToPullNotifications(Arrays.asList(new FolderId(WellKnownFolderName.Inbox)), 30, null, EventType.NewMail);

                Appointment appointment = new Appointment(exchange);

                appointment.setLocation(space.getName());
                appointment.setSubject(event.getTitle());
                appointment.setBody(new MessageBody(event.getDescription()));
                appointment.setStart(Date.from(event.getStart()));
                appointment.setEnd(Date.from(event.getEnd()));

                ExtendedPropertyDefinition corelationId = new ExtendedPropertyDefinition(EXCHANGE_EXTENDED_PROPERTY_S4M_CORELATION_ID, "S4MCorrelationId", MapiPropertyType.String);
                ExtendedPropertyDefinition organizerId = new ExtendedPropertyDefinition(EXCHANGE_EXTENDED_PROPERTY_S4M_ORGANIZER_ID, "S4MOrganizerId", MapiPropertyType.String);
                ExtendedPropertyDefinition organizerName = new ExtendedPropertyDefinition(EXCHANGE_EXTENDED_PROPERTY_S4M_ORGANIZER_NAME, "S4MOrganizerName", MapiPropertyType.String);
                ExtendedPropertyDefinition organizerEmail = new ExtendedPropertyDefinition(EXCHANGE_EXTENDED_PROPERTY_S4M_ORGANIZER_EMAIL, "S4MOrganizerEmail", MapiPropertyType.String);

                appointment.setExtendedProperty(corelationId, event.getExchangeIntegrationCorelation());
                if (event.getOrganizerUserId() != null) {
                    appointment.setExtendedProperty(organizerId, event.getOrganizerUserId().toString());
                }
                if (event.getOrganizerEmail() != null) {
                    appointment.setExtendedProperty(organizerName, event.getOrganizerEmail());
                }
                if (event.getOrganizerFullName() != null) {
                    appointment.setExtendedProperty(organizerEmail, event.getOrganizerFullName());
                }

                appointment.getResources().add(space.getExchangeIntegrationEmail());

                for (EventAttendee attendee : attendees) {
                    if (attendee.getEmail() == null) {
                        continue;
                    }
                    appointment.getRequiredAttendees().add(new Attendee(attendee.getFullName(), attendee.getEmail()));
                }

                if (notify) {
                    appointment.save(SendInvitationsMode.SendOnlyToAll);
                } else {
                    appointment.save(SendInvitationsMode.SendToNone);
                }

                ItemId identity = appointment.getId();

                if (identity != null) {
                    event.setExchangeIntegrationId(identity.toString());
                }

                AWAIT:
                while (true) {
                    Thread.currentThread().sleep(1000);

                    for (ItemEvent ie : subscription.getEvents().getItemEvents()) {
                        EmailMessage em = EmailMessage.bind(exchange, ie.getItemId());

                        if (em instanceof MeetingResponse && em.getSender().getAddress().equals(space.getExchangeIntegrationEmail())) {
                            MeetingResponse mr = (MeetingResponse) em;

                            if (mr.getAssociatedAppointmentId().equals(identity)) {
                                MeetingResponseType mrt = mr.getResponseType();

                                if (mrt == MeetingResponseType.Accept) {
                                    break AWAIT;
                                }

                                if (mrt == MeetingResponseType.Decline) {
                                    cancelAppointment(space, event, true);
                                    return false;
                                }
                            }
                        }
                    }

                    Thread.currentThread().sleep(2000);
                }
            } finally {
                if (subscription != null) {
                    try {
                        subscription.unsubscribe();
                    } finally {

                    }
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return true;
    }

    public boolean updateAppointment(Space space, Event event, Iterable<EventAttendee> attendees, boolean notify) {

        if (event.getExchangeIntegrationId() == null) {
            return true;
        }

        // Work with integrated spaces only

        if (space.getExchangeIntegrationEmail() == null) {
            return true;
        }

        try (ExchangeService exchange = new ExchangeService(ExchangeVersion.Exchange2010)) {
            exchange.setUrl(
                    new URI(EXCHANGE_URI));
            exchange.setCredentials(
                    new WebCredentials(EXCHANGE_LOGIN, EXCHANGE_PASSWORD));

            if (event.getOrganizerEmail() != null) {
                if (event.getOrganizerEmail().endsWith(EXCHANGE_DOMAIN)) {
                    exchange.setImpersonatedUserId(
                            new ImpersonatedUserId(ConnectingIdType.SmtpAddress, event.getOrganizerEmail()));
                }
            }

            PullSubscription subscription = null;

            try {
                subscription = exchange.subscribeToPullNotifications(Arrays.asList(new FolderId(WellKnownFolderName.Inbox)), 30, null, EventType.NewMail);

                Appointment appointment;

                try {
                    appointment = Appointment.bind(exchange, new ItemId(event.getExchangeIntegrationId()));
                } catch (Exception e) {
                    appointment = null;
                }

                if (appointment == null) {
                    return true;
                }

                if (!appointment.getSubject().equals(event.getTitle())) {
                    appointment.setSubject(event.getTitle());
                }

                if (!appointment.getBody().toString().equals(event.getDescription())) {
                    appointment.setBody(new MessageBody(event.getDescription()));
                }

                boolean waitForNotification = false;

                if (!appointment.getStart().equals(Date.from(event.getStart()))) {
                    appointment.setStart(Date.from(event.getStart()));
                    waitForNotification = true;
                }

                if (!appointment.getEnd().equals(Date.from(event.getEnd()))) {
                    appointment.setEnd(Date.from(event.getEnd()));
                    waitForNotification = true;
                }

                appointment.getRequiredAttendees().clear();

                for (EventAttendee attendee : attendees) {
                    if (attendee.getEmail() == null) {
                        continue;
                    }

                    appointment.getRequiredAttendees().add(new Attendee(attendee.getFullName(), attendee.getEmail()));
                }

                if (notify) {
                    appointment.update(ConflictResolutionMode.NeverOverwrite, SendInvitationsOrCancellationsMode.SendOnlyToAll);
                } else {
                    appointment.update(ConflictResolutionMode.NeverOverwrite, SendInvitationsOrCancellationsMode.SendToNone);
                }

                if (waitForNotification) {
                    AWAIT:
                    while (true) {
                        Thread.currentThread().sleep(1000);

                        for (ItemEvent ie : subscription.getEvents().getItemEvents()) {
                            EmailMessage em = EmailMessage.bind(exchange, ie.getItemId());

                            if (em instanceof MeetingResponse && em.getSender().getAddress().equals(space.getExchangeIntegrationEmail())) {
                                MeetingResponse mr = (MeetingResponse) em;

                                if (mr.getAssociatedAppointmentId().equals(appointment.getId())) {
                                    MeetingResponseType mrt = mr.getResponseType();

                                    if (mrt == MeetingResponseType.Accept) {
                                        break AWAIT;
                                    }

                                    if (mrt == MeetingResponseType.Decline) {
                                        return false;
                                    }
                                }
                            }
                        }

                        Thread.currentThread().sleep(2000);
                    }
                }
            } finally {
                if (subscription != null) {
                    try {
                        subscription.unsubscribe();
                    } finally {

                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return true;
    }

    public void cancelAppointment(Space space, Event event, boolean notify) {

        if (space.getExchangeIntegrationEmail() == null) {
            return;
        }

        try (ExchangeService exchange = new ExchangeService(ExchangeVersion.Exchange2010)) {
            exchange.setUrl(
                    new URI(EXCHANGE_URI));
            exchange.setCredentials(
                    new WebCredentials(EXCHANGE_LOGIN, EXCHANGE_PASSWORD));

            if (event.getOrganizerEmail() != null) {
                if (event.getOrganizerEmail().endsWith(EXCHANGE_DOMAIN)) {
                    exchange.setImpersonatedUserId(
                            new ImpersonatedUserId(ConnectingIdType.SmtpAddress, event.getOrganizerEmail()));
                }
            }

            Appointment appointment;

            try {
                appointment = Appointment.bind(exchange, new ItemId(event.getExchangeIntegrationId()));
            } catch (Exception e) {
                appointment = null;
            }

            if (appointment == null) {
                return;
            }

            appointment.cancelMeeting();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void addAttendee(Space space, Event event, EventAttendee attendee, boolean notify) {
        if (event.getExchangeIntegrationId() == null) {
            return;
        }

        // Work with integrated spaces only

        if (space.getExchangeIntegrationEmail() == null) {
            return;
        }

        if (attendee.getEmail() == null) {
            return;
        }

        try (ExchangeService exchange = new ExchangeService(ExchangeVersion.Exchange2010)) {
            exchange.setUrl(
                    new URI(EXCHANGE_URI));
            exchange.setCredentials(
                    new WebCredentials(EXCHANGE_LOGIN, EXCHANGE_PASSWORD));

            if (event.getOrganizerEmail() != null) {
                if (event.getOrganizerEmail().endsWith(EXCHANGE_DOMAIN)) {
                    exchange.setImpersonatedUserId(
                            new ImpersonatedUserId(ConnectingIdType.SmtpAddress, event.getOrganizerEmail()));
                }
            }

            Appointment appointment;

            try {
                appointment = Appointment.bind(exchange, new ItemId(event.getExchangeIntegrationId()));
            } catch (Exception e) {
                appointment = null;
            }

            if (appointment == null) {
                return;
            }

            appointment.getRequiredAttendees().add(new Attendee(attendee.getFullName(), attendee.getEmail()));

            if (notify) {
                appointment.update(ConflictResolutionMode.NeverOverwrite, SendInvitationsOrCancellationsMode.SendOnlyToChanged);
            } else {
                appointment.update(ConflictResolutionMode.NeverOverwrite, SendInvitationsOrCancellationsMode.SendToNone);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void removeAttendee(Space space, Event event, EventAttendee attendee, boolean notify) {
        if (event.getExchangeIntegrationId() == null) {
            return;
        }

        // Work with integrated spaces only

        if (space.getExchangeIntegrationEmail() == null) {
            return;
        }

        if (attendee.getEmail() == null) {
            return;
        }

        try (ExchangeService exchange = new ExchangeService(ExchangeVersion.Exchange2010)) {
            exchange.setUrl(
                    new URI(EXCHANGE_URI));
            exchange.setCredentials(
                    new WebCredentials(EXCHANGE_LOGIN, EXCHANGE_PASSWORD));

            if (event.getOrganizerEmail() != null) {
                if (event.getOrganizerEmail().endsWith(EXCHANGE_DOMAIN)) {
                    exchange.setImpersonatedUserId(
                            new ImpersonatedUserId(ConnectingIdType.SmtpAddress, event.getOrganizerEmail()));
                }
            }

            Appointment appointment;

            try {
                appointment = Appointment.bind(exchange, new ItemId(event.getExchangeIntegrationId()));
            } catch (Exception e) {
                appointment = null;
            }

            if (appointment == null) {
                return;
            }

            Attendee appointmentAttendee = null;

            for (Attendee a : appointment.getRequiredAttendees()) {
                if (a.getAddress().equals(attendee.getEmail())) {
                    appointmentAttendee = a;
                    break;
                }
            }

            if (appointmentAttendee == null) {
                return;
            }

            if (!appointment.getRequiredAttendees().remove(appointmentAttendee)) {
                return;
            }

            if (notify) {
                appointment.update(ConflictResolutionMode.NeverOverwrite, SendInvitationsOrCancellationsMode.SendOnlyToChanged);
            } else {
                appointment.update(ConflictResolutionMode.NeverOverwrite, SendInvitationsOrCancellationsMode.SendToNone);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static interface AppointmentEventHandler {
        void process(Space space, Event changed, List<EventAttendee> attendees);
    }

    public void poll(Iterable<Space> spaces, Date from, Date to, AppointmentEventHandler handler) {

        try (ExchangeService exchange = new ExchangeService(ExchangeVersion.Exchange2010)) {

            ExtendedPropertyDefinition corelationId = new ExtendedPropertyDefinition(EXCHANGE_EXTENDED_PROPERTY_S4M_CORELATION_ID, "S4MCorrelationId", MapiPropertyType.String);
            ExtendedPropertyDefinition organizerId = new ExtendedPropertyDefinition(EXCHANGE_EXTENDED_PROPERTY_S4M_ORGANIZER_ID, "S4MOrganizerId", MapiPropertyType.String);
            ExtendedPropertyDefinition organizerName = new ExtendedPropertyDefinition(EXCHANGE_EXTENDED_PROPERTY_S4M_ORGANIZER_NAME, "S4MOrganizerName", MapiPropertyType.String);
            ExtendedPropertyDefinition organizerEmail = new ExtendedPropertyDefinition(EXCHANGE_EXTENDED_PROPERTY_S4M_ORGANIZER_EMAIL, "S4MOrganizerEmail", MapiPropertyType.String);

            exchange.setUrl(
                    new URI(EXCHANGE_URI));
            exchange.setCredentials(
                    new WebCredentials(EXCHANGE_LOGIN, EXCHANGE_PASSWORD));

            for (Space space : spaces) {
                if (space.getExchangeIntegrationEmail() == null) {
                    continue;
                }

                exchange.setImpersonatedUserId(
                        new ImpersonatedUserId(ConnectingIdType.SmtpAddress, space.getExchangeIntegrationEmail()));

                SearchFilter filter = new SearchFilter.SearchFilterCollection(LogicalOperator.And,
                        new SearchFilter.IsGreaterThanOrEqualTo(AppointmentSchema.LastModifiedTime, from),
                        new SearchFilter.IsLessThan(AppointmentSchema.LastModifiedTime, to),
                        new SearchFilter.IsGreaterThanOrEqualTo(AppointmentSchema.End, Date.from(Instant.now())));

                ItemView view = new ItemView(64);
                view.setPropertySet(new PropertySet(BasePropertySet.IdOnly));

                FindItemsResults<Item> appointmentResults = exchange.findItems(WellKnownFolderName.Calendar, filter, view);

                while (true) {
                    for (Item i : appointmentResults.getItems()) {

                        if (!(i instanceof Appointment)) {
                            continue;
                        }

                        Appointment appointment = (Appointment) i;

                        // Organizer field loaded correctly only when we load item manually, not via findItem.
                        appointment.load();

                        String icaluid = appointment.getICalUid();

                        try {
                            exchange.setImpersonatedUserId(
                                    new ImpersonatedUserId(ConnectingIdType.SmtpAddress, appointment.getOrganizer().getAddress()));

                            FindItemsResults<Item> originalResults = exchange.findItems(WellKnownFolderName.Calendar,
                                    new SearchFilter.IsEqualTo(
                                            new ExtendedPropertyDefinition(DefaultExtendedPropertySet.Meeting, 0x03, MapiPropertyType.Binary),
                                            Base64.encodeBase64String(Hex.decodeHex(icaluid.toCharArray()))
                                    ),
                                    new ItemView(1));

                            // We need only 1 item here if any available

                            if (originalResults.isMoreAvailable()) {
                                for (Item j : originalResults.getItems()) {
                                    if (!(j instanceof Appointment)) {
                                        continue;
                                    }

                                    Appointment original = (Appointment) j;

                                    PropertySet propertySet = new PropertySet(
                                            BasePropertySet.FirstClassProperties,
                                            corelationId,
                                            organizerId,
                                            organizerName,
                                            organizerEmail);
                                    propertySet.setRequestedBodyType(BodyType.Text);

                                    original.load(propertySet);

                                    Event event = new Event();
                                    event.setSpaceId(space.getId());
                                    event.setTitle(original.getSubject());
                                    event.setDescription(original.getBody().toString());
                                    event.setCreated(original.getDateTimeCreated().toInstant());
                                    event.setStart(original.getStart().toInstant());
                                    event.setEnd(original.getEnd().toInstant());

                                    OutParam<String> out = new OutParam<String>();

                                    if (original.getExtendedProperties().tryGetValue(String.class, corelationId, out)) {
                                        // Event came from S4M
                                        event.setExchangeIntegrationCorelation(out.getParam());
                                    }
                                    else {
                                        // Event came from exchange
                                        event.setExchangeIntegrationCorelation(UUID.randomUUID().toString());

                                        original.setExtendedProperty(corelationId, event.getExchangeIntegrationCorelation());
                                        original.update(ConflictResolutionMode.AlwaysOverwrite, SendInvitationsOrCancellationsMode.SendToNone);
                                    }

                                    if (original.getExtendedProperties().tryGetValue(String.class, organizerId, out)) {
                                        event.setOrganizerUserId(Long.valueOf(out.getParam()));
                                    }

                                    if (original.getExtendedProperties().tryGetValue(String.class, organizerName, out)) {
                                        event.setOrganizerFullName(out.getParam());
                                    }
                                    else {
                                        event.setOrganizerFullName(original.getOrganizer().getName());
                                    }

                                    if (original.getExtendedProperties().tryGetValue(String.class, organizerEmail, out)) {
                                        event.setOrganizerEmail(out.getParam());
                                    } else {
                                        event.setOrganizerEmail(original.getOrganizer().getAddress());
                                    }

                                    event.setExchangeIntegrationId(original.getId().toString());

                                    if (original.getIsCancelled()) {
                                        event.setStatus("cancelled");
                                    }

                                    List<EventAttendee> attendees = new LinkedList<EventAttendee>();

                                    for (Attendee k : original.getRequiredAttendees()) {
                                        EventAttendee attendee = new EventAttendee();

                                        attendee.setFullName(k.getName());
                                        attendee.setEmail(k.getAddress());

                                        attendees.add(attendee);
                                    }

                                    handler.process(space, event, attendees);
                                }
                            }
                        } finally {
                            exchange.setImpersonatedUserId(
                                    new ImpersonatedUserId(ConnectingIdType.SmtpAddress, space.getExchangeIntegrationEmail()));
                        }
                    }

                    if (appointmentResults.getNextPageOffset() == null) {
                        break;
                    }

                    view.setOffset(appointmentResults.getNextPageOffset());

                    appointmentResults = exchange.findItems(WellKnownFolderName.Calendar, filter, view);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
