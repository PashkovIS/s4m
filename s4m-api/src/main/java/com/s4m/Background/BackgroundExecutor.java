package com.s4m.Background;

/**
 * Created by nikolay on 3/22/17.
 */

import org.springframework.stereotype.Service;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Service
public class BackgroundExecutor {
    private Executor executor = Executors.newFixedThreadPool(2);

    public void execute (Runnable runnable)
    {
        executor.execute(runnable);
    }
}
