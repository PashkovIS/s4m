package com.s4m.Background;

import com.s4m.Entities.Mail.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by nikolay on 3/19/17.
 */
@Component
@Conditional(EventTaskActivator.class)
public class MailTask {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${app.mailing.TimeoutMinutes}")
    private int TASK_TIMEOUT_MINUTES = 5;
    private int TASK_TIMEOUT_SECONDS = TASK_TIMEOUT_MINUTES * 60;

    @Autowired
    private MailService mailService;

    @Scheduled(/*initialDelay = 5 * 60 * 1000,*/ fixedDelay = 10 * 1000)
    private void unbook() {
        log.debug("Mailing scheduled task started");

        mailService.sendAll();

        log.debug("Mailing scheduled task done");
    }
}
