package com.s4m;

import com.s4m.API.DeviceApi.Auth.DeviceAuthService;
import com.s4m.API.WebApi.Auth.UserAuthService;
import com.s4m.Entities.Device.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationFilter extends OncePerRequestFilter {
    @Autowired
    private DeviceAuthService deviceAuthService;

    @Autowired
    private UserAuthService userAuthService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        Boolean isDeviceApi = request.getRequestURI().startsWith("/device-api");
        Boolean isWebApi = request.getRequestURI().startsWith("/web-api");
        if (isDeviceApi) {
            // Enable CORS for debugging
            String allowedHeaders = "X-Auth,DNT,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range";
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Access-Control-Allow-Credentials", "true");
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS");
            response.addHeader("Access-Control-Allow-Headers", allowedHeaders);
            if (request.getMethod().equals("OPTIONS")) {
                response.addHeader("Access-Control-Max-Age", "1728000");
                response.addHeader("Content-Type", "text/plain charset=UTF-8");
                response.addHeader("Content-Length", "0");
                response.setStatus(204);
                return;
            }

            deviceAuthService.authenticateRequest(request);
        } else if (isWebApi) {
            userAuthService.authenticateRequest(request);
        }

        filterChain.doFilter(request, response);
    }
}
