package ecstaj;

public final class ArgumentRangeException extends ArgumentInvalidException {
    private static final long serialVersionUID = -5391683599285921272L;

    public ArgumentRangeException(String argument) {
        super(argument, "Argument value is out of range");
    }

    public ArgumentRangeException(String argument, Throwable cause) {
        super(argument, "Argument value is out of range", null, cause);
    }

    public ArgumentRangeException(String argument, String hint) {
        super(argument, "Argument value is out of range", hint);
    }

    public ArgumentRangeException(String argument, String hint, Throwable cause) {
        super(argument, "Argument value is out of range", hint, cause);
    }
}

