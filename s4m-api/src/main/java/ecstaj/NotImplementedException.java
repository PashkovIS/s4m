package ecstaj;

public final class NotImplementedException extends RuntimeException {
    private static final long serialVersionUID = 357097427059089534L;

    public NotImplementedException() {
        super("Method or operation not implemented");
    }

    public NotImplementedException(String message) {
        super(message);
    }
}

