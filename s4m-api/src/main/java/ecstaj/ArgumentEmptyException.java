package ecstaj;

public final class ArgumentEmptyException extends ArgumentInvalidException {
    private static final long serialVersionUID = -893748310279131835L;

    public ArgumentEmptyException(String argument) {
        super(argument, "Argument is empty", (String) null);
    }

    public ArgumentEmptyException(String argument, String hint) {
        super(argument, "Argument is empty", hint);
    }
}

