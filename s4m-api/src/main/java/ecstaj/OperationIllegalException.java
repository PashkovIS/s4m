package ecstaj;

public final class OperationIllegalException extends RuntimeException {
    private static final long serialVersionUID = -6061795231327694445L;

    public OperationIllegalException() {
        super("Operation illegal");
    }

    public OperationIllegalException(String message) {
        super(message);
    }

    public OperationIllegalException(String message, Throwable cause) {
        super(message, cause);
    }
}

