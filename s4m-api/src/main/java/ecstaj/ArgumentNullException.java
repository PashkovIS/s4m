package ecstaj;

public final class ArgumentNullException extends ArgumentInvalidException {
    private static final long serialVersionUID = 4471396837744990178L;

    public ArgumentNullException(String argument) {
        super(argument, "Argument is null");
    }

    public ArgumentNullException(String argument, String hint) {
        super(argument, "Argument is null", hint);
    }
}

