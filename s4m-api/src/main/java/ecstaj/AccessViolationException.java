package ecstaj;

public final class AccessViolationException extends RuntimeException {
    private static final long serialVersionUID = 4042065155684676405L;

    public AccessViolationException(String message) {
        super(message);
    }

    public AccessViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}

