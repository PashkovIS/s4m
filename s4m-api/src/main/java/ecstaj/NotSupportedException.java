package ecstaj;

public final class NotSupportedException extends RuntimeException {
    private static final long serialVersionUID = 1725293110953075155L;

    public NotSupportedException(String message) {
        super(message);
    }
}

