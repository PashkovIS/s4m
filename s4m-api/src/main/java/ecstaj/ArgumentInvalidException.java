package ecstaj;

public class ArgumentInvalidException extends RuntimeException {
    private static final long serialVersionUID = -5391683599285921272L;

    private static String format(String argument, String message, String hint) {
        if (argument == null) {
            throw new ArgumentNullException("argument");
        }

        if (message == null) {
            if (hint == null) {
                return String.format("Argument value invalid: '%s'.", argument);
            } else {
                return String.format("Argument value invalid: '%s'. %s.", argument, hint);
            }
        } else {
            if (hint == null) {
                return String.format("%s: '%s'", message, argument);
            } else {
                return String.format("%s: '%s'. %s.", message, argument, hint);
            }
        }
    }

    public ArgumentInvalidException(String argument) {
        super(format(argument, null, null));
    }

    public ArgumentInvalidException(String argument, String message) {
        super(format(argument, message, null));
    }

    public ArgumentInvalidException(String argument, String message, String hint) {
        super(format(argument, message, hint));
    }

    public ArgumentInvalidException(String argument, String message, String hint, Throwable cause) {
        super(format(argument, message, hint), cause);
    }
}
