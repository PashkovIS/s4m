package ecstaj;

public final class ArgumentFormatException extends ArgumentInvalidException {
    private static final long serialVersionUID = -5014849951836174636L;

    public ArgumentFormatException(String argument) {
        super(argument, "Argument has incorrect format");
    }

    public ArgumentFormatException(String argument, Throwable cause) {
        super(argument, "Argument has incorrect format", null, cause);
    }

    public ArgumentFormatException(String argument, String hint) {
        super(argument, "Argument has incorrect format", hint);
    }

    public ArgumentFormatException(String argument, String hint, Throwable cause) {
        super(argument, "Argument has incorrect format", hint, cause);
    }
}

