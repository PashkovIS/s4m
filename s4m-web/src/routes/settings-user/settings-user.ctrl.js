import locationPopupTmpl from './location-popup/location-popup.html';
import locationPopupCtrl from './location-popup/location-popup.ctrl';
import imagePopupTmpl from './image-popup/image-popup.html';
import imagePopupCtrl from './image-popup/image-popup.ctrl';

const ctrl = ['$routeParams', 'apiSvc', 'contextSvc', 'popupSvc', '$uibModal', function ($routeParams, apiSvc, contextSvc, popupSvc, $uibModal) {
    const my = this;

    my.model = {
        userId: $routeParams.userId,
        user: null,
        roles: [],
        showAddAdmin: false,
        dict: {
            locations: {},
            organizations: {}
        }
    };

    function init() {
        apiSvc.user.get(my.model.userId).then(res => {
            my.model.user = res;
        });

        apiSvc.user.getRoles(my.model.userId).then(res => {
            my.model.roles = res;
            my.model.showAddAdmin = !my.model.roles.some(x => x.role === 'admin');
        });

        apiSvc.location.getList().then(res => {
            res.forEach(x => my.model.dict.locations[x.id] = x);
        });

        apiSvc.organization.getList().then(res => {
            res.forEach(x => my.model.dict.organizations[x.id] = x);
        });
    };

    init();

    function openLocationPopup() {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: locationPopupTmpl,
            controller: locationPopupCtrl,
            controllerAs: 'my'
        }).result;
    };

    my.addRoleAdmin = function () {
        apiSvc.user.setRole(my.model.userId, 'admin').then(resp => {
            my.model.roles.push(resp);
            my.model.showAddAdmin = false;
        });
    };

    my.addRoleOfficeManager = function () {
        openLocationPopup().then(locationId => {
            if (!locationId) return;
            if (my.model.roles.some(x => x.role === 'office manager' && x.location === locationId)) {
                popupSvc.error('Пользователь уже является офис-менеджером в данном местоположении');
                return;
            }

            apiSvc.user.setRole(my.model.userId, 'office manager', locationId).then(resp => {
                my.model.roles.push(resp);
            });
        });
    };

    my.deleteRole = function (x) {
        const index = my.model.roles.indexOf(x);
        popupSvc.confirm('Удалить роль у данного пользователя?', 'Удаление роли').then(() => {
            apiSvc.user.deleteRole(my.model.userId, x.id).then(() => {
                my.model.roles.splice(index, 1);
                if (x.role === 'admin') my.model.showAddAdmin = true;
            });
        });
    };

    my.getLoc = function (x) {
        if (!x.location) return;
        return my.model.dict.locations[x.location];
    };

    my.getOrg = function (x) {
        const loc = my.getLoc(x);
        if (!loc) return;
        return my.model.dict.organizations[loc.organizationId];
    };

    // Image --------------------------------------------------------------------------------------

    function openImagePopup(userId) {
        const windowClass = 'modal_' + new Date().getTime();
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: imagePopupTmpl,
            controller: imagePopupCtrl,
            controllerAs: 'my',
            resolve: {
                userId: () => userId,
                windowClass: () => windowClass
            },
            windowClass: windowClass
        }).result;
    };

    my.imageRemove = function () {
        popupSvc.confirm('Вы уверены, что хотите удалить изображение?', 'Удалить изображение', 'Да, удалить').then(() => {
            apiSvc.user.imageRemove(my.model.userId).then(() => {
                my.model.user.imageUrl = null;
            }).catch(() => popupSvc.error());
        });
    };

    my.imageAdd = function () {
        openImagePopup(my.model.userId).then(uploaded => {
            // Refresh space info
            if (uploaded) {
                return apiSvc.user.get(my.model.userId).then(res => {
                    my.model.user = res;
                });
            }
        });
    };
}];

export default ctrl;
