const ctrl = ['$uibModalInstance', 'apiSvc', '$routeParams', function ($uibModalInstance, apiSvc, $routeParams) {
    const my = this;

    my.model = {
        organizations: null,
        locations: null,
        organizationId: null,
        locationId: null
    }

    function init() {
        apiSvc.organization.getList().then(res => {
            my.model.organizations = res;
            const orgId = parseInt($routeParams.companyId);
            if (!orgId && res.length === 1) {
                my.changeOrganization(res[0].id);
            } else {
                my.changeOrganization(orgId);
            }
        });
    };

    init();

    my.ok = function () {
        $uibModalInstance.close(my.model.locationId);
    };

    my.cancel = function () {
        $uibModalInstance.close();
    };

    my.changeOrganization = function (newId) {
        my.model.locationId = null;
        my.model.locations = null;
        my.model.organizationId = newId;
        if (my.model.organizationId) {
            apiSvc.location.getListByOrganization(my.model.organizationId).then(res => {
                my.model.locations = res;

                if (res.length === 1) {
                    my.model.locationId = res[0].id;
                }
            });
        }
    };
}];

export default ctrl;
