import tmpl from './settings-user.html';
import ctrl from './settings-user.ctrl';

const route = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my',
    anonymous: false,
    accessRole: 'admin'
};

export default route;
