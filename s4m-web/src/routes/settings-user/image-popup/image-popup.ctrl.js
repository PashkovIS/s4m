const ctrl = ['$http', '$uibModalInstance', 'userId', 'windowClass', 'apiSvc', 'popupSvc', function ($http, $uibModalInstance, userId, windowClass, apiSvc, popupSvc) {
    const my = this;

    my.userId = userId;

    function upload() {
        const input = $('input[type="file"]', `.${windowClass}`);
        const file = input[0].files[0];

        return apiSvc.user.imageUpload(userId, file);
    };

    my.ok = function () {
        upload().then(() => {
            $uibModalInstance.close(true);
        }).catch(err => {
            popupSvc.error('Не удалось сохранить изображение');
        });
    };

    my.cancel = function () {
        $uibModalInstance.close(false);
    };
}];

export default ctrl;
