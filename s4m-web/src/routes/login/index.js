import tmpl from './login.html';
import ctrl from './login.ctrl';

const rt = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my',
    anonymous: true
};

export default rt;
