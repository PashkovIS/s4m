const ctrl = ['authSvc', 'popupSvc', '$location', function (authSvc, popupSvc, $location) {
    const my = this;

    my.model = {
        login: null,
        password: null
    };

    my.login = function () {
        authSvc.login(my.model.login, my.model.password).then(res => {
            if (!res.authorized) {
                popupSvc.error('Проверьте правильность указания логина и пароля', 'Ошибка авторизации');
            } else {
                const returnTo = $location.search().returnTo || '/';
                $location.path(returnTo).search("returnTo", null);
            }
        }).catch(() => popupSvc.error());
    };
}];

export default ctrl;
