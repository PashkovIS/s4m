import tmpl from './events.html';
import ctrl from './events.ctrl';

const rt = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my'
};

export default rt;
