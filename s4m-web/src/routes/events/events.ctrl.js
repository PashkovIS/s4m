const ctrl = ['contextSvc', 'apiSvc', '$uibModal', 'eventSvc', '$q', function (contextSvc, apiSvc, $uibModal, eventSvc, $q) {
    const my = this;

    my.model = {
        company: null,
        events: null,
        date: new Date(),
        dict: {
            spaces: {},
            locations: {}
        }
    };

    function init() {
        contextSvc.getCurrentCompany().then(company => {
            my.model.company = company;

            $q.all([
                apiSvc.space.getListByOrganization(my.model.company.id).then(spaces => {
                    spaces.forEach(x => my.model.dict.spaces[x.id] = x);
                }),
                apiSvc.location.getListByOrganization(my.model.company.id).then(locations => {
                    locations.forEach(x => my.model.dict.locations[x.id] = x);
                })
            ]).then(() => {
                my.search();
            });
        });
    };

    init();

    my.search = function () {
        const companyId = my.model.company.id;

        const d = my.model.date;
        if (!d) return;

        const dateFrom = new Date(d.getFullYear(), d.getMonth(), d.getDate());
        const dateTo = new Date(dateFrom.getTime());
        dateTo.setHours(dateTo.getHours() + 24);

        apiSvc.event.getListByOrganization(companyId, dateFrom.toISOString(), dateTo.toISOString()).then(res => {
            my.model.events = res;
        });
    };

    my.addItem = function () {
        eventSvc.create().then(event => {
            if (event) {
                my.model.events.push(event);
            }
        });
    };

    my.editItem = function (x) {
        eventSvc.edit(x.id).then(event => {
            if (event) {
                const index = my.model.events.indexOf(x);
                if (event === 'delete') {
                    my.model.events.splice(index, 1);
                } else {
                    my.model.events[index] = event;
                }
            }
        });
    };

    my.getSpace = function (event) {
        return my.model.dict.spaces[event.spaceId];
    };

    my.getLocation = function (event) {
        const space = my.model.dict.spaces[event.spaceId];
        return my.model.dict.locations[space.locationId];
    };
}];

export default ctrl;
