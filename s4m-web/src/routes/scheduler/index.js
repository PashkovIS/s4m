import tmpl from './scheduler.html';
import ctrl from './scheduler.ctrl';

const rt = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my'
};

export default rt;
