const ctrl = ['contextSvc', 'apiSvc', 'eventSvc', '$scope', '$compile', function (contextSvc, apiSvc, eventSvc, $scope, $compile) {
    const my = this;

    my.model = {
        company: null,
        filter: {
            start: null,
            end: null
        },
        eventSources: [[]],
        dict: {
            spaces: {}
        },
        uiConfig: {
            calendar: {
                editable: false,
                header: {},
                defaultView: 'timelineDay',
                nowIndicator: true,
                resources: [],
                slotWidth: 50,
                slotLabelFormat: 'HH:mm',
                scrollTime: '08:00',
                resourceLabelText: 'Переговорные',
                buttonText: {
                    today: 'Сегодня'
                },
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                monthNamesShort: ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
                viewRender: function (view) {
                    viewRender(view.type, view.start, view.end);
                },
                eventClick: function (calEvent) {
                    my.editItem(calEvent);
                },
                height: 'parent'
            }
        }
    };

    function viewRender(viewType, momentFrom, momentTo) {
        const from = momentFrom.toDate();
        const localFrom = new Date(from.getFullYear(), from.getMonth(), from.getDate());

        const to = momentTo.toDate();
        const localTo = new Date(to.getFullYear(), to.getMonth(), to.getDate());

        const start = localFrom.toISOString();
        const end = localTo.toISOString();

        if (my.model.filter.start !== start) {
            my.model.filter.start = start;
            my.model.filter.end = end;

            if (!my.model.company) {
                init().then(my.search);
            } else {
                my.search();
            }
        }
    };

    function init() {
        const from = new Date();
        const localFrom = new Date(from.getFullYear(), from.getMonth(), from.getDate());

        const localTo = new Date(localFrom.getTime());
        localTo.setHours(localTo.getHours() + 24);

        my.model.filter.start = localFrom.toISOString();
        my.model.filter.end = localTo.toISOString();

        const resources = [];
        return contextSvc.getCurrentCompany().then(company => {
            my.model.company = company;
            return apiSvc.location.getListByOrganization(my.model.company.id);
        }).then(locations => {
            locations.forEach(x => {
                resources.push({
                    id: `loc_${x.id}`,
                    title: x.name
                });
            });
        }).then(() => {
            return apiSvc.space.getListByOrganization(my.model.company.id);
        }).then(spaces => {
            spaces.forEach(x => {
                my.model.dict.spaces[x.id] = x;
                resources.push({
                    id: x.id,
                    title: x.name,
                    parentId: `loc_${x.locationId}`
                });
            });

            my.model.uiConfig.calendar.resources = resources;
        }).then(() => {
            my.search();
        });
    };

    init();

    my.search = function () {
        const companyId = my.model.company.id;
        return apiSvc.event.getListByOrganization(companyId, my.model.filter.start, my.model.filter.end).then(events => {
            my.model.eventSources[0] = events.map(x => ({
                id: x.id,
                resourceId: x.spaceId,
                title: x.title,
                start: new Date(x.start),
                end: new Date(x.end),
                spaceName: my.model.dict.spaces[x.spaceId].name,
                originalEvent: x
            }));
        }).then(() => {
            $('div.scheduler-cnt').fullCalendar('refetchEvents');
        });
    };

    my.editItem = function (calEvent) {
        eventSvc.edit(calEvent.originalEvent.id).then(event => {
            if (event) {
                const arr = my.model.eventSources[0];
                const index = arr.findIndex(x => x.id === calEvent.id);

                if (event === 'delete') {
                    arr.splice(index, 1);
                } else {
                    const e = arr[index];
                    e.title = event.title;
                    e.start = new Date(event.start);
                    e.end = new Date(event.end);
                    e.originalEvent = event;
                }

                my.model.eventSources[0] = arr.slice();
                $('div.scheduler-cnt').fullCalendar('refetchEvents');
            }
        });
    };
}];

export default ctrl;
