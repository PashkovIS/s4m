import tmpl from './settings-location.html';
import ctrl from './settings-location.ctrl';

const route = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my',
    fallbackUrl: '/c/:companyId/settings/locations',
    anonymous: false,
    accessRole: 'admin'
};

export default route;
