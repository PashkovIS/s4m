const ctrl = ['$routeParams', 'apiSvc', 'contextSvc', function ($routeParams, apiSvc, contextSvc) {
    const my = this;

    my.model = {
        params: $routeParams,
        company: null,
        location: null,
        spaces: null
    };

    function init() {
        contextSvc.getCurrentCompany().then(company => {
            my.model.company = company;

            apiSvc.location.get(my.model.params.locationId).then(res => {
                my.model.location = res;
            });

            apiSvc.space.getListByLocation(my.model.params.locationId).then(res => {
                my.model.spaces = res;
            });
        });
    };

    init();
}];

export default ctrl;
