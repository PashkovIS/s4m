const ctrl = ['contextSvc', '$location', function (contextSvc, $location) {
    const my = this;

    my.model = {
        showWarning: false,
        companies: null
    };

    function init() {
        contextSvc.getCompanyList().then(companies => {
            my.model.companies = companies;
            if (companies && companies.length) {
                const id = companies[0].id;
                $location.path(`/c/${id}`);
            } else if (contextSvc.userIs('admin')) {
                $location.path(`/create-company`);
            } else {
                my.model.showWarning = true;
            }
        });
    };

    init();
}];

export default ctrl;
