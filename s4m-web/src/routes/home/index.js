import tmpl from './home.html';
import ctrl from './home.ctrl';

const rt = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my'
};

export default rt;
