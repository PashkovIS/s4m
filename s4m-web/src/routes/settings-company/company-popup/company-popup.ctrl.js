const ctrl = ['$uibModalInstance', 'item', 'title', function ($uibModalInstance, item, title) {
    const my = this;

    my.item = item;
    my.title = title;
    
    my.ok = function () {
        $uibModalInstance.close(my.item);
    };

    my.cancel = function () {
        $uibModalInstance.close();
    };
}];

export default ctrl;
