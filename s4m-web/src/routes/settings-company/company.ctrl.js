import popupTmpl from './company-popup/company-popup.html';
import popupCtrl from './company-popup/company-popup.ctrl';
import imagePopupTmpl from './image-popup/image-popup.html';
import imagePopupCtrl from './image-popup/image-popup.ctrl';

const ctrl = ['contextSvc', 'apiSvc', '$uibModal', 'popupSvc', '$location', function (contextSvc, apiSvc, $uibModal, popupSvc, $location) {
    const my = this;

    my.model = {
        company: null,
        locations: null,
        spaces: null,
        resources: null
    };

    function init() {
        return contextSvc.getCurrentCompany().then(company => {
            my.model.company = company;

            apiSvc.location.getListByOrganization(company.id).then(resp => {
                my.model.locations = resp;
            });

            apiSvc.space.getListByOrganization(company.id).then(resp => {
                my.model.spaces = resp;
            });

            apiSvc.resource.getList().then(resp => {
                my.model.resources = resp;
            });
        });
    };

    init();

    function openPopup(item, title) {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: popupTmpl,
            controller: popupCtrl,
            controllerAs: 'my',
            resolve: {
                item: () => item,
                title: () => title
            }
        }).result;
    };

    my.editItem = function () {
        const tmp = JSON.parse(JSON.stringify(my.model.company)); // clone object
        openPopup(tmp, 'Править компанию').then(item => {
            if (!item) return;

            apiSvc.company.update(item.id, item).then(resp => {
                my.model.company = item;
                return contextSvc.refresh();
            }).catch(() => popupSvc.error());
        });
    };

    my.deleteCompany = function () {
        popupSvc.confirm('Вы уверены, что хотите удалить эту компанию безвозвратно?', 'Удаление компании', 'Да, удалить').then(() => {
            apiSvc.organization.remove(my.model.company.id).then(() => {
                contextSvc.refresh().then(() => {
                    $location.path('/');
                });
            }).catch(() => popupSvc.error());
        });
    };

    // Image --------------------------------------------------------------------------------------

    function openImagePopup(organizationId, logoType) {
        const windowClass = 'modal_' + new Date().getTime();
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: imagePopupTmpl,
            controller: imagePopupCtrl,
            controllerAs: 'my',
            resolve: {
                organizationId: () => organizationId,
                logoType: () => logoType,
                windowClass: () => windowClass
            },
            windowClass: windowClass
        }).result;
    };

    my.logoRemove = function () {
        popupSvc.confirm('Вы уверены, что хотите удалить изображение?', 'Удалить изображение', 'Да, удалить').then(() => {
            apiSvc.organization.logoRemove(my.model.company.id).then(() => {
                my.model.company.logoUrl = null;
                return contextSvc.refresh();
            }).catch(() => popupSvc.error());
        });
    };

    my.logoAdd = function () {
        openImagePopup(my.model.company.id, 'organizationLogo').then(uploaded => {
            // Refresh organization info
            if (uploaded) {
                return apiSvc.organization.get(my.model.company.id).then(res => {
                    my.model.company = res;
                    return contextSvc.refresh();
                });
            }
        });
    };

    my.roomDisplayLogoRemove = function () {
        popupSvc.confirm('Вы уверены, что хотите удалить изображение?', 'Удалить изображение', 'Да, удалить').then(() => {
            apiSvc.organization.logoRoomDisplayRemove(my.model.company.id).then(() => {
                my.model.company.roomDisplayLogoUrl = null;
                return contextSvc.refresh();
            }).catch(() => popupSvc.error());
        });
    };

    my.roomDisplayLogoAdd = function () {
        openImagePopup(my.model.company.id, 'organizationRoomDisplayLogo').then(uploaded => {
            // Refresh organization info
            if (uploaded) {
                return apiSvc.organization.get(my.model.company.id).then(res => {
                    my.model.company = res;
                    return contextSvc.refresh();
                });
            }
        });
    };
}];

export default ctrl;
