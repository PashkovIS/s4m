import tmpl from './company.html';
import ctrl from './company.ctrl';

const route = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my',
    anonymous: false,
    accessRole: 'admin'
};

export default route;
