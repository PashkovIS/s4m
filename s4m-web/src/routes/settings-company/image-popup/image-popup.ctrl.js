const ctrl = ['$http', '$uibModalInstance', 'organizationId', 'logoType', 'windowClass', 'apiSvc', 'popupSvc', function ($http, $uibModalInstance, organizationId, logoType, windowClass, apiSvc, popupSvc) {
    const my = this;

    my.organizationId = organizationId;
    my.logoType = logoType;

    function upload() {
        const input = $('input[type="file"]', `.${windowClass}`);
        const file = input[0].files[0];

        switch (logoType) {
            case 'organizationLogo': return apiSvc.organization.logoUpload(organizationId, file); break;
            case 'organizationRoomDisplayLogo': return apiSvc.organization.logoRoomDisplayUpload(organizationId, file); break;
        }
    };

    my.ok = function () {
        upload().then(() => {
            $uibModalInstance.close(true);
        }).catch(err => {
            popupSvc.error('Не удалось сохранить изображение');
        });
    };

    my.cancel = function () {
        $uibModalInstance.close(false);
    };
}];

export default ctrl;
