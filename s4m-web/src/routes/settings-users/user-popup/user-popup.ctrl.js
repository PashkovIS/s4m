const ctrl = ['$uibModalInstance', 'item', 'title', function ($uibModalInstance, item, title) {
    const my = this;

    my.item = item;
    my.title = title;
    
    my.ok = function () {
        $uibModalInstance.close(my.item);
    };

    my.cancel = function () {
        $uibModalInstance.close();
    };

    my.delete = function () {
        $uibModalInstance.close('delete');
    };
}];

export default ctrl;
