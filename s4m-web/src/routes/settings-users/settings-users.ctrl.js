import userPopupTmpl from './user-popup/user-popup.html';
import userPopupCtrl from './user-popup/user-popup.ctrl';

const ctrl = ['apiSvc', '$uibModal', '$routeParams', 'popupSvc', function (apiSvc, $uibModal, $routeParams, popupSvc) {
    const my = this;

    my.model = {
        companyId: $routeParams.companyId,
        users: null
    };

    function init() {
        apiSvc.user.getList().then(resp => {
            my.model.users = resp;
        });
    };

    init();

    function openUserPopup(item, title) {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: userPopupTmpl,
            controller: userPopupCtrl,
            controllerAs: 'my',
            resolve: {
                item: () => item,
                title: () => title
            }
        }).result;
    };

    my.addItem = function () {
        openUserPopup({}, 'Создать пользователя').then(item => {
            if (!item) return;

            apiSvc.user.create(item).then(resp => {
                my.model.users.push(resp);
            }).catch(() => popupSvc.error());
        });
    };

    my.editItem = function (x) {
        const index = my.model.users.indexOf(x);
        apiSvc.user.get(x.id).then(itemToEdit => {
            openUserPopup(itemToEdit, 'Править пользователя').then(item => {
                if (!item) return;
                if (item === 'delete') {
                    popupSvc.confirm('Вы уверены, что хотите удалить этого пользователя безвозвратно?', 'Удаление пользователя', 'Да, удалить').then(() => {
                        apiSvc.user.remove(x.id).then(() => {
                            my.model.users.splice(index, 1);
                        }).catch(() => popupSvc.error());
                    });
                } else {
                    apiSvc.user.update(item.id, item).then(resp => {
                        my.model.users[index] = item;
                    }).catch(() => popupSvc.error());
                }
            });
        });
    };
}];

export default ctrl;
