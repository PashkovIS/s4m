import popupTmpl from './space-popup/space-popup.html';
import popupCtrl from './space-popup/space-popup.ctrl';

const ctrl = ['$scope', 'apiSvc', '$uibModal', 'contextSvc', 'popupSvc', function ($scope, apiSvc, $uibModal, contextSvc, popupSvc) {
    const my = this;

    my.model = {
        company: null,
        spaces: null,
        locations: null,
        spaceTypes: null
    };

    function init() {
        contextSvc.getCurrentCompany().then(company => {
            my.model.company = company;

            apiSvc.space.getListByOrganization(company.id).then(resp => {
                my.model.spaces = resp;
            });

            apiSvc.location.getListByOrganization(company.id).then(resp => {
                my.model.locations = resp;
            });
        });

        apiSvc.spaceType.getList().then(resp => {
            my.model.spaceTypes = resp;
        });
    };

    init();

    function openPopup(item, title) {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: popupTmpl,
            controller: popupCtrl,
            controllerAs: 'my',
            resolve: {
                item: () => item,
                title: () => title,
                locations: () => my.model.locations,
                spaceTypes: () => my.model.spaceTypes
            }
        }).result;
    };

    my.addItem = function () {
        openPopup({}, 'Создать переговорную').then(item => {
            if (!item) return;

            apiSvc.space.create(item.locationId, item).then(resp => {
                my.model.spaces.push(resp);
            }).catch(() => popupSvc.error());
        });
    };

    my.editItem = function (x) {
        const index = my.model.spaces.indexOf(x);
        apiSvc.space.get(x.id).then(itemToEdit => {
            openPopup(itemToEdit, 'Править переговорную').then(item => {
                if (!item) return;
                if (item === 'delete') {
                    popupSvc.confirm('Вы уверены, что хотите удалить эту переговорную безвозвратно?', 'Удаление переговорной', 'Да, удалить').then(() => {
                        apiSvc.space.remove(x.id).then(() => {
                            my.model.spaces.splice(index, 1);
                        }).catch(() => popupSvc.error());
                    });
                } else {
                    apiSvc.space.update(item.id, item).then(resp => {
                        my.model.spaces[index] = resp;
                    }).catch(() => popupSvc.error());
                }
            });
        });

    };
}];

export default ctrl;
