import tmpl from './settings-spaces.html';
import ctrl from './settings-spaces.ctrl';

const route = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my',
    anonymous: false,
    accessRole: 'admin'
};

export default route;
