const ctrl = ['$uibModalInstance', 'item', 'title', 'locations', 'spaceTypes', function ($uibModalInstance, item, title, locations, spaceTypes) {
    const my = this;

    my.item = item;
    my.title = title;
    my.ref = {
        sizes: [
            { code: 1, name: 'Микро (1 м)' },
            { code: 2, name: 'Очень маленькая (2 м)' },
            { code: 3, name: 'Маленькая (3 м)' },
            { code: 7, name: 'Средняя (7 м)' },
            { code: 14, name: 'Большая (14 м)' },
            { code: 28, name: 'Очень большая (28 м)' }
        ],
        locations: locations,
        spaceTypes: spaceTypes
    };

    my.ok = function () {
        if (my.item.locationId) {
            $uibModalInstance.close(my.item);
        }
    };

    my.cancel = function () {
        $uibModalInstance.close();
    };

    my.delete = function () {
        $uibModalInstance.close('delete');
    };
}];

export default ctrl;
