import tmpl from './settings-space.html';
import ctrl from './settings-space.ctrl';

const route = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my',
    fallbackUrl: '/c/:companyId/settings/spaces',
    anonymous: false,
    accessRole: 'admin'
};

export default route;
