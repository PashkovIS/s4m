import devicePopupTmpl from './device-popup/device-popup.html';
import devicePopupCtrl from './device-popup/device-popup.ctrl';
import imagePopupTmpl from './image-popup/image-popup.html';
import imagePopupCtrl from './image-popup/image-popup.ctrl';

const ctrl = ['$scope', '$routeParams', 'apiSvc', 'popupSvc', '$uibModal', function ($scope, $routeParams, apiSvc, popupSvc, $uibModal) {
    const my = this;

    my.model = {
        params: $routeParams,
        space: null,
        devices: null,
        settings: null,
        statusBarColors: [
            { id: 'black', name: 'Чёрная' },
            { id: 'white', name: 'Белая' }
        ]
    };

    function init() {
        apiSvc.space.get(my.model.params.spaceId).then(res => {
            my.model.space = res;
        });

        apiSvc.device.getList(my.model.params.spaceId).then(res => {
            my.model.devices = res;
        });

        apiSvc.spaceSettings.get(my.model.params.spaceId).then(res => {
            my.model.settings = res;
            if (!my.model.settings.headerColor) {
                my.model.settings.headerColor = 'transparent';
            }
            if (!my.model.settings.statusBarColor) {
                my.model.settings.statusBarColor = 'transparent';
            }
        });
    };

    init();

    function openDevicePopup(item, title) {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: devicePopupTmpl,
            controller: devicePopupCtrl,
            controllerAs: 'my',
            resolve: {
                item: () => item,
                title: () => title
            }
        }).result;
    };

    my.activateDevice = function () {
        openDevicePopup({}, 'Активировать устройство').then(item => {
            if (!item) return;

            apiSvc.device.activate(my.model.params.spaceId, item.name, item.pin).then(resp => {
                my.model.devices.push(resp);
            }).catch(err => {
                if (err.data.status === 404) {
                    popupSvc.error('Проверьте правильность ввода ПИН-кода. Возможно, истёк срок активации', 'Не удалось активировать устройство');
                } else {
                    popupSvc.error();
                }
            });
        });
    };

    my.editDevice = function (x) {
        const index = my.model.devices.indexOf(x);
        apiSvc.device.get(x.id).then(itemToEdit => {
            openDevicePopup(itemToEdit, 'Править устройство').then(item => {
                if (!item) return;
                if (item === 'delete') {
                    popupSvc.confirm('Вы уверены, что хотите отвязать это устройство?', 'Отвязать устройство', 'Да, отвязать').then(() => {
                        apiSvc.device.remove(x.id).then(() => {
                            my.model.devices.splice(index, 1);
                        }).catch(() => popupSvc.error());
                    });
                } else {
                    apiSvc.device.rename(item.id, item.name).then(resp => {
                        my.model.devices[index] = resp;
                    }).catch(() => popupSvc.error());
                }
            });
        });
    };

    my.saveSettings = function () {
        if (!my.model.settings.hideMeetingAttendees) {
            my.model.settings.keepMeetingOrganizerVisible = false;
        }
        apiSvc.spaceSettings.update(my.model.params.spaceId, my.model.settings);
    };

    // Image --------------------------------------------------------------------------------------

    function openImagePopup(spaceId) {
        const windowClass = 'modal_' + new Date().getTime();
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: imagePopupTmpl,
            controller: imagePopupCtrl,
            controllerAs: 'my',
            resolve: {
                spaceId: () => spaceId,
                windowClass: () => windowClass
            },
            windowClass: windowClass
        }).result;
    };

    my.imageRemove = function () {
        popupSvc.confirm('Вы уверены, что хотите удалить изображение?', 'Удалить изображение', 'Да, удалить').then(() => {
            apiSvc.space.imageRemove(my.model.params.spaceId).then(() => {
                my.model.space.imageUrl = null;
            }).catch(() => popupSvc.error());
        });
    };

    my.imageAdd = function () {
        openImagePopup(my.model.params.spaceId).then(uploaded => {
            // Refresh space info
            if (uploaded) {
                return apiSvc.space.get(my.model.params.spaceId).then(res => {
                    my.model.space = res;
                });
            }
        });
    };
}];

export default ctrl;
