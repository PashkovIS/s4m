const ctrl = ['$http', '$uibModalInstance', 'spaceId', 'windowClass', 'apiSvc', 'popupSvc', function ($http, $uibModalInstance, spaceId, windowClass, apiSvc, popupSvc) {
    const my = this;

    my.spaceId = spaceId;

    function upload() {
        const input = $('input[type="file"]', `.${windowClass}`);
        const file = input[0].files[0];

        return apiSvc.space.imageUpload(spaceId, file);
    };

    my.ok = function () {
        upload().then(() => {
            $uibModalInstance.close(true);
        }).catch(err => {
            popupSvc.error('Не удалось сохранить изображение');
        });
    };

    my.cancel = function () {
        $uibModalInstance.close(false);
    };
}];

export default ctrl;
