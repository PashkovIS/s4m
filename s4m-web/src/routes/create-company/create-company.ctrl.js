const ctrl = ['apiSvc', 'contextSvc', '$location', 'popupSvc', function (apiSvc, contextSvc, $location, popupSvc) {
    const my = this;

    my.model = {
        name: null
    };

    my.create = function () {
        apiSvc.organization.create({ name: my.model.name }).then(res => {
            return contextSvc.refresh().then(() => {
                $location.path(`/c/${res.id}/settings`);
            });
        }).catch(() => popupSvc.error());
    };
}];

export default ctrl;
