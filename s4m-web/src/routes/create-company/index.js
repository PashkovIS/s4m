import tmpl from './create-company.html';
import ctrl from './create-company.ctrl';

const rt = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my'
};

export default rt;
