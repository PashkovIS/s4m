import moment from 'moment';

const ctrl = ['$routeParams', 'apiSvc', 'popupSvc', 'eventSvc', function ($routeParams, apiSvc, popupSvc, eventSvc) {
    const my = this;

    const now = new Date();
    my.model = {
        companyId: $routeParams.companyId,
        filter: {
            date: now,
            time: `${now.getHours()}:${now.getMinutes()}`,
            duration: 30,
            locations: [],
            spaceTypes: []
        },
        dict: {
            locations: {},
            spaceTypes: {}
        },
        spaces: null,
        isSearched: false,
        locations: [],
        spaceTypes: [],
        tags: [],
        duration: [
            { value: 10, label: '10 минут' },
            { value: 20, label: '20 минут' },
            { value: 30, label: '30 минут' },
            { value: 40, label: '40 минут' },
            { value: 50, label: '50 минут' },
            { value: 60, label: '1 час' },
            { value: 70, label: '1 час 10 минут' },
            { value: 80, label: '1 час 20 минут' },
            { value: 90, label: '1 час 30 минут' },
            { value: 100, label: '1 час 40 минут' },
            { value: 110, label: '1 час 50 минут' },
            { value: 120, label: '2 часа' },
            { value: 150, label: '2 часа 30 минут' },
            { value: 180, label: '3 часа' },
            { value: 210, label: '3 часа 30 минут' },
            { value: 240, label: '4 часа' },
            { value: 270, label: '4 часа 30 минут' },
            { value: 300, label: '5 часов' },
            { value: 330, label: '5 часов 30 минут' },
            { value: 360, label: '6 часов' },
            { value: 390, label: '6 часов 30 минут' },
            { value: 420, label: '7 часов' },
            { value: 450, label: '7 часов 30 минут' },
            { value: 480, label: '8 часов' },
            { value: 510, label: '8 часов 30 минут' },
            { value: 540, label: '9 часов' },
            { value: 570, label: '9 часов 30 минут' },
            { value: 600, label: '10 часов' },
            { value: 630, label: '10 часов 30 минут' },
            { value: 660, label: '11 часов' },
            { value: 690, label: '11 часов 30 минут' },
            { value: 720, label: '12 часов' }
        ],
        capacities: [
            3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
        ],
        times: []
    };

    function init() {
        apiSvc.location.getListByOrganization(my.model.companyId).then(res => {
            my.model.locations = res;
            my.model.locations.forEach(x => my.model.dict.locations[x.id] = x);
        });

        apiSvc.spaceType.getList().then(res => {
            my.model.spaceTypes = res;
            my.model.spaceTypes.forEach(x => my.model.dict.spaceTypes[x.id] = x);
        });

        my.search();
    };

    my.search = function () {
        if (my.model.filter.capacity) {
            const name = `${my.model.filter.capacity} чел`;
            const tag = my.model.tags.find(x => x.type === 'capacity');
            if (tag) {
                tag.name = name;
            } else {
                my.model.tags.push({ name: name, type: 'capacity' })
            }
        } else {
            const index = my.model.tags.findIndex(x => x.type === 'capacity');
            if (index > -1) my.model.tags.splice(index, 1);
        }

        const d = my.model.filter.date;
        const t = my.model.filter.time;
        const tps = t.split(':');
        const hours = parseInt(tps[0]);
        const minutes = parseInt(tps[1]);
        const filter = {
            organizationId: my.model.companyId,
            dateTime: new Date(d.getFullYear(), d.getMonth(), d.getDate(), hours, minutes, 0).toISOString(),
            duration: my.model.filter.duration,
            capacity: my.model.filter.capacity,
            locations: my.model.filter.locations,
            spaceTypes: my.model.filter.spaceTypes
        };

        apiSvc.space.search(filter).then(res => {
            my.model.spaces = res;
            my.model.isSearched = true;
        });
    };

    function addFilter(tag) {
        if (!my.model.tags.some(x => x.type === tag.type && x.id === tag.id)) {
            my.model.tags.push(tag);
            switch (tag.type) {
                case 'location': my.model.filter.locations.push(tag.id); break;
                case 'spaceType': my.model.filter.spaceTypes.push(tag.id); break;
            }

            my.search();
        }
    };

    my.addFilterByLocation = function (loc) {
        const tag = {
            id: loc.id,
            name: loc.name,
            type: 'location'
        };

        addFilter(tag);
    };

    my.addFilterBySpaceType = function (spaceType) {
        const tag = {
            id: spaceType.id,
            name: spaceType.name,
            type: 'spaceType'
        };

        addFilter(tag);
    };

    my.removeFilter = function (tag) {
        const index = my.model.tags.indexOf(tag);
        my.model.tags.splice(index, 1);

        switch (tag.type) {
            case 'location':
                const locIndex = my.model.filter.locations.indexOf(tag.id);
                my.model.filter.locations.splice(locIndex, 1);
                break;
            case 'spaceType':
                const spaceTypeIndex = my.model.filter.spaceTypes.indexOf(tag.id);
                my.model.filter.spaceTypes.splice(spaceTypeIndex, 1);
                break;
            case 'capacity':
                my.model.filter.capacity = undefined;
                break;
        }

        my.search();
    };

    my.clearFilter = function () {
        my.model.tags = [];
        my.model.filter.locations = [];
        my.model.filter.spaceTypes = [];
        my.model.filter.capacity = undefined;
        my.search();
    };

    my.bookSpace = function (x) {
        const d = my.model.filter.date;
        const tps = my.model.filter.time.split(':');
        const h = parseInt(tps[0]);
        const m = parseInt(tps[1]);
        const start = new Date(d.getFullYear(), d.getMonth(), d.getDate(), h, m, 0);
        const end = new Date(start.getTime());
        end.setMinutes(end.getMinutes() + my.model.filter.duration);

        const event = {
            start: start,
            end: end,
            spaceId: x.id
        };

        eventSvc.create(event).then(event => {
            if (event) {
                const dt = moment(event.start).format('DD.MM.YYYY в HH:mm');
                const msg = `Запланирована встреча на ${dt} в переговорной ${x.name}`;
                popupSvc.alert(msg, 'Переговорная забронирована');
            }
        });
    };

    my.setTimes = function (opened, date) {
        if (!opened) return;

        let times = [];
        const start = new Date(1970, 0, 1);
        const end = new Date(1970, 0, 2);
        const stepInMinutes = 30;
        for (let t = start; t < end; t.setMinutes(t.getMinutes() + stepInMinutes)) {
            const time = moment(t).format('HH:mm');
            times.push({ value: time, name: time });
        }

        const isToday = moment(date).format('YYYY-MM-DD') === moment().format('YYYY-MM-DD');
        if (isToday) {
            const nowTime = moment().format('HH:mm');
            times = times.filter(x => x.value > nowTime);
            times.unshift({ value: nowTime, name: `${nowTime} (сейчас)` });
        } else {
            times.find(x => x.value === '09:00').scrollTo = true;
        }

        my.model.times = times;
    };

    init();
}];

export default ctrl;
