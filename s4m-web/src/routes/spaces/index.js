import tmpl from './spaces.html';
import ctrl from './spaces.ctrl';

const rt = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my'
};

export default rt;
