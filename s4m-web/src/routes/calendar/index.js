import tmpl from './calendar.html';
import ctrl from './calendar.ctrl';

const rt = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my'
};

export default rt;
