const ctrl = ['contextSvc', 'apiSvc', 'eventSvc', '$scope', '$compile', function (contextSvc, apiSvc, eventSvc, $scope, $compile) {
    const my = this;

    my.model = {
        company: null,
        filter: {
            start: null,
            end: null
        },
        eventSources: [[]],
        dict: {
            spaces: {}
        },
        uiConfig: {
            calendar: {
                editable: false,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                defaultView: 'month',
                firstDay: 1,
                navLinks: true,
                timeFormat: 'HH:mm',
                columnFormat: 'ddd DD.MM',
                slotLabelFormat: 'HH:mm',
                allDayText: 'Весь день',
                scrollTime: '08:00:00',
                buttonText: {
                    today: 'Сегодня',
                    month: 'Месяц',
                    week: 'Неделя',
                    day: 'День',
                    list: 'Список'
                },
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                monthNamesShort: ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
                nowIndicator: true,
                eventRender: eventRender,
                views: {
                    month: {
                        eventLimit: 3,
                        selectable: false
                    }
                },
                eventLimitText: 'ещё',
                viewRender: function (view) {
                    viewRender(view.type, view.start, view.end);
                },
                eventClick: function (calEvent) {
                    my.editItem(calEvent);
                }
            }
        }
    };

    function viewRender(viewType, momentFrom, momentTo) {
        const from = momentFrom.toDate();
        const localFrom = new Date(from.getFullYear(), from.getMonth(), from.getDate());

        const to = momentTo.toDate();
        const localTo = new Date(to.getFullYear(), to.getMonth(), to.getDate());

        my.model.filter.start = localFrom.toISOString();
        my.model.filter.end = localTo.toISOString();

        if (!my.model.company) {
            init().then(my.search);
        } else {
            my.search();
        }
    };

    function eventRender(event, element) {
        const scope = $scope.$new(true);
        scope.event = event;
        element.find(".fc-content").append('<div class="space-name">{{event.spaceName}}</div>');
        element.attr({
            "uib-popover-template": "'templates/calendar/popover/event.html'",
            "popover-placement": "auto top-left",
            "popover-trigger": "{outsideClick: 'outsideClick', mouseenter: 'mouseleave'}",
            "popover-class": "event-popover",
            "popover-append-to-body": "true",
            "popover-popup-delay": "200"
        });

        $compile(element)(scope);
    };

    function init() {
        return contextSvc.getCurrentCompany().then(company => {
            my.model.company = company;

            return apiSvc.space.getListByOrganization(my.model.company.id).then(spaces => {
                spaces.forEach(x => my.model.dict.spaces[x.id] = x);
            });
        });
    };

    my.search = function () {
        const companyId = my.model.company.id;

        my.model.eventSources[0] = [];
        return apiSvc.event.getMyListByOrganization(companyId, my.model.filter.start, my.model.filter.end).then(events => {
            my.model.eventSources[0] = events.map(x => ({
                id: x.id,
                title: x.title,
                start: new Date(x.start),
                end: new Date(x.end),
                spaceName: my.model.dict.spaces[x.spaceId].name,
                originalEvent: x
            }));
        });
    };

    my.editItem = function (calEvent) {
        eventSvc.edit(calEvent.originalEvent.id).then(event => {
            if (event) {
                const arr = my.model.eventSources[0];
                const index = arr.findIndex(x => x.id === calEvent.id);

                if (event === 'delete') {
                    arr.splice(index, 1);
                } else {
                    const e = arr[index];
                    e.title = event.title;
                    e.start = new Date(event.start);
                    e.end = new Date(event.end);
                    e.originalEvent = event;
                }

                my.model.eventSources[0] = arr.slice();
            }
        });
    };
}];

export default ctrl;
