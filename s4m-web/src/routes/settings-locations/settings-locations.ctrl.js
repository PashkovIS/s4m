import popupTmpl from './location-popup/location-popup.html';
import popupCtrl from './location-popup/location-popup.ctrl';

const ctrl = ['apiSvc', '$uibModal', 'contextSvc', 'popupSvc', function (apiSvc, $uibModal, contextSvc, popupSvc) {
    const my = this;

    my.model = {
        company: null,
        locations: null,
        timezones: null
    };

    function init() {
        contextSvc.getCurrentCompany().then(company => {
            my.model.company = company;

            apiSvc.location.getListByOrganization(company.id).then(resp => {
                my.model.locations = resp;
            });
        });

        apiSvc.timezone.getList().then(res => {
            my.model.timezones = res;
        });
    };

    init();

    function openPopup(item, title) {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: popupTmpl,
            controller: popupCtrl,
            controllerAs: 'my',
            resolve: {
                item: () => item,
                title: () => title,
                timezones: () => my.model.timezones
            }
        }).result;
    };

    my.addItem = function () {
        openPopup({}, 'Создать расположение').then(item => {
            if (!item) return;

            apiSvc.location.create(my.model.company.id, item).then(resp => {
                my.model.locations.push(resp);
            }).catch(() => popupSvc.error());
        });
    };

    my.editItem = function (x) {
        const index = my.model.locations.indexOf(x);
        apiSvc.location.get(x.id).then(itemToEdit => {
            openPopup(itemToEdit, 'Править расположение').then(item => {
                if (!item) return;
                if (item === 'delete') {
                    popupSvc.confirm('Вы уверены, что хотите удалить это место расположения безвозвратно?', 'Удаление расположения', 'Да, удалить').then(() => {
                        apiSvc.location.remove(x.id).then(() => {
                            my.model.locations.splice(index, 1);
                        }).catch(() => popupSvc.error());
                    });
                } else {
                    apiSvc.location.update(item.id, item).then(resp => {
                        my.model.locations[index] = resp;
                    }).catch(() => popupSvc.error());
                }
            });
        });
    };
}];

export default ctrl;
