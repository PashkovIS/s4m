const ctrl = ['$uibModalInstance', 'item', 'title', 'timezones', function ($uibModalInstance, item, title, timezones) {
    const my = this;

    my.item = item;
    my.title = title;
    my.timezones = timezones;
    
    my.ok = function () {
        $uibModalInstance.close(my.item);
    };

    my.cancel = function () {
        $uibModalInstance.close();
    };

    my.delete = function () {
        $uibModalInstance.close('delete');
    };
}];

export default ctrl;
