import popupTmpl from './space-type-popup/space-type-popup.html';
import popupCtrl from './space-type-popup/space-type-popup.ctrl';

const ctrl = ['apiSvc', '$uibModal', 'contextSvc', 'popupSvc', function (apiSvc, $uibModal, contextSvc, popupSvc) {
    const my = this;

    my.model = {
        spaceTypes: null
    };

    function init() {
        apiSvc.spaceType.getList().then(res => {
            my.model.spaceTypes = res;
        });
    };

    init();

    function openPopup(item, title) {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: popupTmpl,
            controller: popupCtrl,
            controllerAs: 'my',
            resolve: {
                item: () => item,
                title: () => title
            }
        }).result;
    };

    my.addItem = function () {
        openPopup({}, 'Создать тип пространства').then(item => {
            if (!item) return;

            apiSvc.spaceType.create(item).then(resp => {
                my.model.spaceTypes.push(resp);
            }).catch(() => popupSvc.error());
        });
    };

    my.editItem = function (x) {
        const index = my.model.spaceTypes.indexOf(x);
        apiSvc.spaceType.get(x.id).then(itemToEdit => {
            openPopup(itemToEdit, 'Править тип пространства').then(item => {
                if (!item) return;
                if (item === 'delete') {
                    popupSvc.confirm('Вы уверены, что хотите удалить этот тип пространства?', 'Удаление типа пространства', 'Да, удалить').then(() => {
                        apiSvc.spaceType.remove(x.id).then(() => {
                            my.model.spaceTypes.splice(index, 1);
                        }).catch(() => popupSvc.error());
                    });
                } else {
                    apiSvc.spaceType.update(item.id, item).then(resp => {
                        my.model.spaceTypes[index] = resp;
                    }).catch(() => popupSvc.error());
                }
            });
        });
    };
}];

export default ctrl;
