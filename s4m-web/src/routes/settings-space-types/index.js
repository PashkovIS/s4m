import tmpl from './settings-space-types.html';
import ctrl from './settings-space-types.ctrl';

const route = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my',
    anonymous: false,
    accessRole: 'admin'
};

export default route;
