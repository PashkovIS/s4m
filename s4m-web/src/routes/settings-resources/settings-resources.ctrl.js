import popupTmpl from './resource-popup/resource-popup.html';
import popupCtrl from './resource-popup/resource-popup.ctrl';

const ctrl = ['$scope', 'apiSvc', '$uibModal', 'contextSvc', 'popupSvc', function ($scope, apiSvc, $uibModal, contextSvc, popupSvc) {
    const my = this;

    my.model = {
        resources: []
    };

    function init() {
        apiSvc.resource.getList().then(resp => {
            my.model.resources = resp;
        });
    };

    init();

    function openPopup(item, title) {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: popupTmpl,
            controller: popupCtrl,
            controllerAs: 'my',
            resolve: {
                item: () => item,
                title: () => title
            }
        }).result;
    };

    my.addItem = function () {
        openPopup({}, 'Создать ресурс').then(result => {
            if (!result) return;

            apiSvc.resource.create(result.item).then(resp => {
                if (result.file) {
                    return apiSvc.resource.imageUpload(resp.id, result.file).then(() => {
                        return apiSvc.resource.get(resp.id);
                    }).then(res => {
                        my.model.resources.push(res);
                    });
                } else {
                    my.model.resources.push(resp);
                }
            }).catch(() => popupSvc.error());
        });
    };

    my.editItem = function (x) {
        const index = my.model.resources.indexOf(x);
        apiSvc.resource.get(x.id).then(itemToEdit => {
            openPopup(itemToEdit, 'Править ресурс').then(result => {
                if (!result) return;
                if (result === 'delete') {
                    popupSvc.confirm('Вы уверены, что хотите удалить этот ресурс безвозвратно?', 'Удаление ресурса', 'Да, удалить').then(() => {
                        apiSvc.resource.remove(x.id).then(() => {
                            my.model.resources.splice(index, 1);
                        }).catch(() => popupSvc.error());
                    });
                } else {
                    apiSvc.resource.update(result.item.id, result.item).then(resp => {
                        if (result.file) {
                            return apiSvc.resource.imageUpload(resp.id, result.file);
                        } else if (result.deleteFile) {
                            return apiSvc.resource.imageRemove(resp.id);
                        }
                    }).then(() => {
                        return apiSvc.resource.get(result.item.id)
                    }).then(resp => {
                        my.model.resources[index] = resp;
                    }).catch(() => popupSvc.error());
                }
            });
        });
    };
}];

export default ctrl;
