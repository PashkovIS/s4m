import tmpl from './settings-resources.html';
import ctrl from './settings-resources.ctrl';

const route = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my',
    anonymous: false,
    accessRole: 'admin'
};

export default route;
