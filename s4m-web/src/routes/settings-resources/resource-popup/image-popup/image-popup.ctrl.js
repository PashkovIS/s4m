const ctrl = ['$uibModalInstance', 'windowClass', function ($uibModalInstance, windowClass) {
    const my = this;

    my.ok = function () {
        const input = $('input[type="file"]', `.${windowClass}`);
        const file = input[0].files[0];
        $uibModalInstance.close(file);
    };

    my.cancel = function () {
        $uibModalInstance.close(false);
    };
}];

export default ctrl;
