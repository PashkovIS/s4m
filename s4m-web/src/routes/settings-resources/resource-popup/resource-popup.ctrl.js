import attributePopupTmpl from './attribute-popup/attribute-popup.html';
import attributePopupCtrl from './attribute-popup/attribute-popup.ctrl';
import imagePopupTmpl from './image-popup/image-popup.html';
import imagePopupCtrl from './image-popup/image-popup.ctrl';

const ctrl = ['$uibModal', '$uibModalInstance', 'popupSvc', 'item', 'title', function ($uibModal, $uibModalInstance, popupSvc, item, title) {
    const my = this;

    my.item = item;
    my.title = title;
    my.deleteFile = false;
    my.file = null;

    my.ok = function () {
        $uibModalInstance.close({
            item: my.item,
            deleteFile: my.deleteFile,
            file: my.file
        });
    };

    my.cancel = function () {
        $uibModalInstance.close();
    };

    my.delete = function () {
        $uibModalInstance.close('delete');
    };

    function openAttributePopup(item, title) {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: attributePopupTmpl,
            controller: attributePopupCtrl,
            controllerAs: 'my',
            resolve: {
                item: () => item,
                title: () => title
            }
        }).result;
    };

    my.addItem = function () {
        openAttributePopup(null, 'Создать атрибут').then(item => {
            if (!item) return;
            if (!my.item.attributes) my.item.attributes = [];
            my.item.attributes.push(item);
        });
    };

    my.editItem = function (x) {
        const index = my.item.attributes.indexOf(x);
        const tmp = JSON.parse(JSON.stringify(x)); // clone object
        openAttributePopup(tmp, 'Править атрибут').then(item => {
            if (!item) return;
            if (item === 'delete') {
                popupSvc.confirm('Вы уверены, что хотите удалить этот атрибут?', 'Удаление атрибута', 'Да, удалить').then(() => {
                    my.item.attributes.splice(index, 1);
                });
            } else {
                my.item.attributes[index] = item;
            }
        });
    };

    // Image --------------------------------------------------------------------------------------

    function openImagePopup() {
        const windowClass = 'modal_' + new Date().getTime();
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: imagePopupTmpl,
            controller: imagePopupCtrl,
            controllerAs: 'my',
            resolve: {
                windowClass: () => windowClass
            },
            windowClass: windowClass
        }).result;
    };

    my.imageRemove = function () {
        my.deleteFile = true;
        my.file = null;
    };

    my.imageAdd = function () {
        openImagePopup().then(file => {
            if (file) {
                my.deleteFile = false;
                my.file = file;
            }
        });
    };
}];

export default ctrl;
