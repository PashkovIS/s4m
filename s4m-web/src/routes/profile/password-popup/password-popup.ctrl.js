const ctrl = ['$uibModalInstance', function ($uibModalInstance) {
    const my = this;

    my.model = {
        oldPassword: null,
        newPassword: null,
        newPasswordRepeat: null
    }

    my.ok = function () {
        $uibModalInstance.close({
            oldPassword: my.model.oldPassword,
            newPassword: my.model.newPassword
        });
    };

    my.cancel = function () {
        $uibModalInstance.close();
    };

    my.isOk = function () {
        const m = my.model;
        return !!m.oldPassword && !!m.newPassword && (m.newPassword === m.newPasswordRepeat);
    };
}];

export default ctrl;
