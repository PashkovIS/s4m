import passwordPopupTmpl from './password-popup/password-popup.html';
import passwordPopupCtrl from './password-popup/password-popup.ctrl';

const ctrl = ['contextSvc', 'apiSvc', 'popupSvc', '$uibModal', function (contextSvc, apiSvc, popupSvc, $uibModal) {
    const my = this;

    my.model = {
        user: contextSvc.getCurrentUser(),
        roles: contextSvc.getCurrentUserRoles(),
        dict: {
            locations: {},
            organizations: {}
        }
    };

    function init() {
        apiSvc.location.getList().then(res => {
            res.forEach(x => my.model.dict.locations[x.id] = x);
        });

        apiSvc.organization.getList().then(res => {
            res.forEach(x => my.model.dict.organizations[x.id] = x);
        });
    };

    init();

    my.getLoc = function (x) {
        if (!x.location) return;
        return my.model.dict.locations[x.location];
    };

    my.getOrg = function (x) {
        const loc = my.getLoc(x);
        if (!loc) return;
        return my.model.dict.organizations[loc.organizationId];
    };

    function openPasswordPopup() {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: passwordPopupTmpl,
            controller: passwordPopupCtrl,
            controllerAs: 'my'
        }).result;
    };

    my.changePassword = function () {
        openPasswordPopup().then(res => {
            if (!res) return;
            apiSvc.user.changePassword(my.model.user.id, res.oldPassword, res.newPassword)
                .then(() => popupSvc.alert('Пароль успешно изменён', 'Смена пароля'))
                .catch(err => {
                    if (err === 'escape key press') return;

                    if(err.data.status === 403) {
                        popupSvc.error('Проверьте правильность ввода старого пароля', 'Не удалось изменить пароль');
                    } else {
                        popupSvc.error('При смене пароля произошла непредвиденная ошибка', 'Не удалось изменить пароль');
                    }
                });
        });
    };
}];

export default ctrl;
