import tmpl from './profile.html';
import ctrl from './profile.ctrl';

const rt = {
    template: tmpl,
    controller: ctrl,
    controllerAs: 'my'
};

export default rt;
