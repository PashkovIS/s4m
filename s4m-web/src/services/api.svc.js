var svc = ['$http', '$window', function ($http, $window) {
    const baseUrl = '/api/web-api';

    function imageUpload(url, file) {
        const formData = new FormData();
        formData.append("file", file);
        return $http({
            method: 'POST',
            url: url,
            headers: { 'Content-Type': undefined },
            data: formData
        }).then(function (result) {
            return result.data;
        });
    };

    this.organization = {
        getList: () => $http.get(`${baseUrl}/organizations`).then(resp => resp.data),
        create: data => $http.post(`${baseUrl}/organizations`, data).then(resp => resp.data),
        get: id => $http.get(`${baseUrl}/organizations/${id}`).then(resp => resp.data),
        update: (id, data) => $http.put(`${baseUrl}/organizations/${id}`, data).then(resp => resp.data),
        remove: id => $http.delete(`${baseUrl}/organizations/${id}`),
        logoUpload: (id, file) => imageUpload(`${baseUrl}/organizations/${id}/logo`, file),
        logoRemove: id => $http.delete(`${baseUrl}/organizations/${id}/logo`),
        logoFullUpload: (id, file) => imageUpload(`${baseUrl}/organizations/${id}/logo-full`, file),
        logoFullRemove: id => $http.delete(`${baseUrl}/organizations/${id}/logo-full`),
        logoRoomDisplayUpload: (id, file) => imageUpload(`${baseUrl}/organizations/${id}/logo-room-display`, file),
        logoRoomDisplayRemove: id => $http.delete(`${baseUrl}/organizations/${id}/logo-room-display`),
        
    };

    this.location = {
        getList: () => $http.get(`${baseUrl}/locations`).then(resp => resp.data),
        getListByOrganization: organizationId => $http.get(`${baseUrl}/organizations/${organizationId}/locations`).then(resp => resp.data),
        create: (organizationId, data) => $http.post(`${baseUrl}/organizations/${organizationId}/locations`, data).then(resp => resp.data),
        get: id => $http.get(`${baseUrl}/locations/${id}`).then(resp => resp.data),
        update: (id, data) => $http.put(`${baseUrl}/locations/${id}`, data).then(resp => resp.data),
        remove: id => $http.delete(`${baseUrl}/locations/${id}`)
    };

    this.space = {
        getList: () => $http.get(`${baseUrl}/spaces`).then(resp => resp.data),
        getListByLocation: locationId => $http.get(`${baseUrl}/locations/${locationId}/spaces`).then(resp => resp.data),
        getListByOrganization: organizationId => $http.get(`${baseUrl}/organizations/${organizationId}/spaces`).then(resp => resp.data),
        create: (locationId, data) => $http.post(`${baseUrl}/locations/${locationId}/spaces`, data).then(resp => resp.data),
        get: id => $http.get(`${baseUrl}/spaces/${id}`).then(resp => resp.data),
        update: (id, data) => $http.put(`${baseUrl}/spaces/${id}`, data).then(resp => resp.data),
        remove: id => $http.delete(`${baseUrl}/spaces/${id}`),
        imageRemove: id => $http.delete(`${baseUrl}/spaces/${id}/image`),
        imageUpload: (id, file) => imageUpload(`${baseUrl}/spaces/${id}/image`, file),
        search: (fd) => {
            const filter = [];
            filter.push(`organizationId=${fd.organizationId}`);
            filter.push(`startDateTime=${encodeURIComponent(fd.dateTime)}`);
            filter.push(`durationInMinutes=${fd.duration}`);
            if (fd.locations) {
                for (let locationId of fd.locations) {
                    filter.push(`locationId=${locationId}`);
                }
            }
            if (fd.capacity) {
                filter.push(`capacity=${fd.capacity}`);
            }
            if (fd.spaceTypes) {
                for (let spaceTypeId of fd.spaceTypes) {
                    filter.push(`spaceTypeId=${spaceTypeId}`);
                }
            }

            const filterStr = filter.join('&');
            return $http.get(`${baseUrl}/spaces/search?${filterStr}`).then(resp => resp.data);
        }
    };

    this.spaceSettings = {
        get: id => $http.get(`${baseUrl}/spaces/${id}/settings`).then(resp => resp.data),
        update: (id, data) => $http.put(`${baseUrl}/spaces/${id}/settings`, data).then(resp => resp.data)
    };

    this.event = {
        getListBySpace: spaceId => $http.get(`${baseUrl}/spaces/${spaceId}/events`).then(resp => resp.data),
        getListByLocation: locationId => $http.get(`${baseUrl}/locations/${locationId}/events`).then(resp => resp.data),
        getListByOrganization: (organizationId, dateFrom, dateTo) => $http.get(`${baseUrl}/organizations/${organizationId}/events?dateFrom=${dateFrom}&dateTo=${dateTo}`).then(resp => resp.data),
        getMyListByOrganization: (organizationId, dateFrom, dateTo) => $http.get(`${baseUrl}/organizations/${organizationId}/events/my?dateFrom=${dateFrom}&dateTo=${dateTo}`).then(resp => resp.data),
        create: (spaceId, data) => $http.post(`${baseUrl}/spaces/${spaceId}/events`, data).then(resp => resp.data),
        get: id => $http.get(`${baseUrl}/events/${id}`).then(resp => resp.data),
        update: (id, data) => $http.put(`${baseUrl}/events/${id}`, data).then(resp => resp.data),
        remove: id => $http.delete(`${baseUrl}/events/${id}`),
        getAttendees: id => $http.get(`${baseUrl}/events/${id}/attendees`).then(resp => resp.data),
        saveResources: (id, data) => $http.put(`${baseUrl}/events/${id}/resources`, data)
    };

    this.device = {
        getList: spaceId => $http.get(`${baseUrl}/spaces/${spaceId}/devices`).then(resp => resp.data),
        activate: (spaceId, name, pin) => $http.post(`${baseUrl}/devices/activate`, { spaceId: spaceId, deviceName: name, activationPin: pin }).then(resp => resp.data),
        get: id => $http.get(`${baseUrl}/devices/${id}`).then(resp => resp.data),
        rename: (id, name) => $http.put(`${baseUrl}/devices/${id}`, { deviceName: name }).then(resp => resp.data),
        remove: id => $http.delete(`${baseUrl}/devices/${id}`)
    };

    this.user = {
        getList: () => $http.get(`${baseUrl}/users`).then(resp => resp.data),
        search: q => $http.get(`${baseUrl}/users/search?q=${encodeURIComponent(q)}`).then(resp => resp.data),
        create: data => $http.post(`${baseUrl}/users`, data).then(resp => resp.data),
        get: id => $http.get(`${baseUrl}/users/${id}`).then(resp => resp.data),
        update: (id, data) => $http.put(`${baseUrl}/users/${id}`, data).then(resp => resp.data),
        remove: id => $http.delete(`${baseUrl}/users/${id}`),
        setPassword: (id, password) => $http.post(`${baseUrl}/users/${id}/password`, { password: password }).then(resp => resp.data),
        changePassword: (id, oldPassword, newPassword) => $http.put(`${baseUrl}/users/${id}/password`, { oldPassword: oldPassword, newPassword: newPassword }).then(resp => resp.data),
        getRoles: id => $http.get(`${baseUrl}/users/${id}/roles`).then(resp => resp.data),
        setRole: (id, role, locationId) => $http.post(`${baseUrl}/users/${id}/roles`, { role: role, location: locationId }).then(resp => resp.data),
        deleteRole: (id, roleId) => $http.delete(`${baseUrl}/users/${id}/roles/${roleId}`).then(resp => resp.data),
        imageRemove: id => $http.delete(`${baseUrl}/users/${id}/image`),
        imageUpload: (id, file) => imageUpload(`${baseUrl}/users/${id}/image`, file)
    };

    this.resource = {
        getList: () => $http.get(`${baseUrl}/resources`).then(resp => resp.data),
        create: data => $http.post(`${baseUrl}/resources`, data).then(resp => resp.data),
        get: id => $http.get(`${baseUrl}/resources/${id}`).then(resp => resp.data),
        update: (id, data) => $http.put(`${baseUrl}/resources/${id}`, data).then(resp => resp.data),
        remove: id => $http.delete(`${baseUrl}/resources/${id}`),
        imageRemove: id => $http.delete(`${baseUrl}/resources/${id}/image`),
        imageUpload: (id, file) => imageUpload(`${baseUrl}/resources/${id}/image`, file)
    };

    this.timezone = {
        getList: () => $http.get(`${baseUrl}/dict/timezones`).then(resp => resp.data)
    };

    this.spaceType = {
        getList: () => $http.get(`${baseUrl}/space-types`).then(resp => resp.data),
        create: (data) => $http.post(`${baseUrl}/space-types`, data).then(resp => resp.data),
        get: id => $http.get(`${baseUrl}/space-types/${id}`).then(resp => resp.data),
        update: (id, data) => $http.put(`${baseUrl}/space-types/${id}`, data).then(resp => resp.data),
        remove: id => $http.delete(`${baseUrl}/space-types/${id}`)
    };
}];

export default svc;
