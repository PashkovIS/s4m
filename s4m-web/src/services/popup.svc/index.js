import popupTmpl from './popup/popup.html';
import popupCtrl from './popup/popup.ctrl';

var svc = ['$uibModal', function ($uibModal) {
    function openPopup(config) {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: popupTmpl,
            controller: popupCtrl,
            controllerAs: 'my',
            resolve: {
                config: () => config
            }
        }).result;
    };

    this.alert = function (text, title) {
        return openPopup({
            title: title,
            text: text,
            okText: 'OK',
            showOk: true
        });
    };

    this.error = function (text, title) {
        return openPopup({
            title: title || 'Ошибка',
            text: text || 'Произошла непредвиденная ошибка',
            okText: 'OK',
            showOk: true,
            iconClass: 'fa fa-times-circle-o'
        });
    };

    this.confirm = function (text, title, okText, cancelText) {
        return openPopup({
            title: title || 'Вы уверены?',
            text: text,
            okText: okText || 'Подтвердить',
            showOk: true,
            cancelText: cancelText || 'Отмена',
            showCancel: true
        });
    };
}];

export default svc;
