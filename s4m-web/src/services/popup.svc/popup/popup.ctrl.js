const ctrl = ['$uibModalInstance', 'config', function ($uibModalInstance, config) {
    const my = this;

    my.config = config;
    
    my.ok = function () {
        $uibModalInstance.close();
    };

    my.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}];

export default ctrl;
