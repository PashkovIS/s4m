var svc = [
  "$http",
  function($http) {
    function login(login, password) {
      return $http
        .post("/api/web-api/auth/login", { login: login, password: password })
        .then(resp => resp.data);
    }

    function logout() {
      return $http.post("/api/web-api/auth/logout").then(resp => resp.data);
    }

    function check() {
      return $http.post("/api/web-api/auth/check").then(resp => resp.data);
    }

    this.login = login;
    this.logout = logout;
    this.check = check;
  }
];

export default svc;
