import popupTmpl from './event-popup/event-popup.html';
import popupCtrl from './event-popup/event-popup.ctrl';

var svc = ['$uibModal', 'apiSvc', '$routeParams', 'popupSvc', 'contextSvc', function ($uibModal, apiSvc, $routeParams, popupSvc, contextSvc) {
    function openPopup(item, title, spaces, isReadOnly, isFieldsRestricted, location) {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: popupTmpl,
            controller: popupCtrl,
            controllerAs: 'my',
            windowClass: isFieldsRestricted ? 'window-event-small' : 'window-event',
            backdrop: 'static',
            keyboard: false,
            resolve: {
                item: () => item,
                title: () => title,
                spaces: () => {
                    if (spaces) return spaces;

                    const companyId = $routeParams.companyId;
                    const locationId = $routeParams.locationId;
                    if (locationId) {
                        return apiSvc.space.getListByLocation(locationId);
                    } else if (companyId) {
                        return apiSvc.space.getListByOrganization(companyId);
                    } else {
                        return apiSvc.space.getList();
                    }
                },
                isReadOnly: () => isReadOnly,
                isFieldsRestricted: () => isFieldsRestricted,
                location: () => location
            }
        }).result;
    };

    function create(eventDefaults, spaces) {
        if (!eventDefaults) eventDefaults = {};

        return openPopup(eventDefaults, 'Запланировать встречу', spaces, false, false, null);
    };

    function edit(eventId, spaces) {
        let itemToEdit;
        return apiSvc.event.get(eventId).then(event => {
            itemToEdit = event;
            return apiSvc.space.get(event.spaceId);
        }).then(space => apiSvc.location.get(space.locationId)).then(location => {
            const currentUser = contextSvc.getCurrentUser();
            const canEdit = (
                currentUser.id === itemToEdit.organizerUserId
                || currentUser.id === itemToEdit.createdByUserId
                || contextSvc.userIs('admin')
                || contextSvc.userIs('office manager', location.id)
            );
            const isReadWrite = (itemToEdit.status === 'scheduled' || itemToEdit.status === 'started') && canEdit;

            const isFieldsRestricted = (
                itemToEdit.privateEvent
                && currentUser.id !== itemToEdit.organizerUserId
                && currentUser.id !== itemToEdit.createdByUserId
                && !contextSvc.userIs('admin')
            );

            return openPopup(itemToEdit, 'Просмотр встречи', spaces, !isReadWrite, isFieldsRestricted, location);
        });
    };

    this.create = create;
    this.edit = edit;
}];

export default svc;
