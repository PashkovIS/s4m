import moment from 'moment-timezone';
import resourcesPopupTmpl from './resources-popup.html';
import resourcesPopupCtrl from './resources-popup.ctrl';

const ctrl = ['$uibModal', '$uibModalInstance', 'item', 'title', '$filter', 'spaces', 'apiSvc', 'contextSvc', 'popupSvc', 'isReadOnly', 'isFieldsRestricted', 'location', function ($uibModal, $uibModalInstance, item, title, $filter, spaces, apiSvc, contextSvc, popupSvc, isReadOnly, isFieldsRestricted, location) {
    const my = this;

    my.item = item;
    my.title = title;
    my.spaces = spaces;

    my.model = {
        isLoading: false,
        isDateOpened: false,
        dateFrom: item.start ? new Date(item.start) : new Date(),
        dateTo: item.end ? new Date(item.end) : new Date(),
        timeFrom: item.start ? moment(item.start).format('HH:mm') : '10:00',
        timeTo: item.end ? moment(item.end).format('HH:mm') : '10:30',
        attendees: [],
        organizerIsAttendee: false,
        location: location,
        locationTimezoneOffset: null,
        locationTimezoneAbbr: null,
        currentTimezoneId: null,
        currentTimezoneOffset: null,
        currentTimezoneAbbr: null,
        isReadOnly: isReadOnly,
        isFieldsRestricted: isFieldsRestricted,
        isPrivateEventEditable: false,
        statusName: getStatusName(item.status),
        statusClass: getStatusClass(item.status),
        times: []
    };

    function getStatusName(status) {
        switch (status) {
            case 'scheduled': return 'Запланирована';
            case 'started': return 'Идёт';
            case 'finished': return 'Закончена';
            case 'cancelled': return 'Отменена';
        }
        return 'новая';
    }

    function getStatusClass(status) {
        switch (status) {
            case 'scheduled': return 'label-primary';
            case 'started': return 'label-success';
            case 'finished': return 'label-default';
            case 'cancelled': return 'label-warning';
        }
        return 'label-info';
    }

    function init() {
        const currentUser = contextSvc.getCurrentUser();
        if (my.item.id) {
            apiSvc.event.getAttendees(my.item.id).then(res => {
                my.model.attendees = res || [];
                my.model.organizerIsAttendee = !!my.model.attendees.some(x => x.email === my.item.organizerEmail);
            });
            calcTimezone();

            my.model.isPrivateEventEditable = currentUser.id === my.item.organizerUserId
                || currentUser.id === my.item.createdByUserId
                || contextSvc.userIs('admin');
        } else {
            my.model.isPrivateEventEditable = true;
            my.item.organizerUserId = currentUser.id;
            my.item.organizerFullName = currentUser.fullName;
            my.item.organizerEmail = currentUser.email;
            my.item.organizerImageUrl = currentUser.imageUrl;

            if (!location && my.item.spaceId) {
                loadLocation(my.item.spaceId).then(location => {
                    my.model.location = location
                    calcTimezone();
                });
            }
        }
    };

    init();

    function loadLocation(spaceId) {
        return apiSvc.space.get(my.item.spaceId).then(space => apiSvc.location.get(space.locationId));
    };

    function calcTimezone() {
        if (!my.item.spaceId || !my.model.dateFrom || !my.model.timeFrom) {
            my.model.locationOffsetStr = null;
            return;
        }

        const dt = getISODateTime(my.model.dateFrom, my.model.timeFrom, my.model.location.timezoneId);
        my.model.locationTimezoneOffset = moment.tz(dt, my.model.location.timezoneId).format('Z');
        my.model.locationTimezoneAbbr = moment.tz(dt, my.model.location.timezoneId).zoneAbbr();
        if (my.model.locationTimezoneAbbr.startsWith('+') || my.model.locationTimezoneAbbr.startsWith('-')) {
            my.model.locationTimezoneAbbr = '';
        }

        my.model.currentTimezoneId = moment.tz.guess();
        my.model.currentTimezoneOffset = moment.tz(dt, my.model.currentTimezoneId).format('Z');
        my.model.currentTimezoneAbbr = moment.tz(dt, my.model.currentTimezoneId).zoneAbbr();
        if (my.model.currentTimezoneAbbr.startsWith('+') || my.model.currentTimezoneAbbr.startsWith('-')) {
            my.model.currentTimezoneAbbr = '';
        }
    };

    my.onDateFromChange = function () {
        calcTimezone();
    };

    my.onTimeFromChange = function () {
        calcTimezone();
    };

    my.onSpaceChange = function () {
        loadLocation(my.item.spaceId).then(location => {
            my.model.location = location
            calcTimezone();
        });
    };

    function onError(err) {
        my.model.isLoading = false;
        if (err === 'escape key press') return;

        if (err.status === 400 && err.data && err.data.message === 'Conflict') {
            popupSvc.error('Это время уже занято в выбранной переговорной', 'Не удалось запланировать встречу');
        } else {
            popupSvc.error();
        }
    };

    my.ok = function () {
        if (!my.model.location) return;
        my.item.start = getISODateTime(my.model.dateFrom, my.model.timeFrom, my.model.location.timezoneId);
        my.item.end = getISODateTime(my.model.dateTo, my.model.timeTo, my.model.location.timezoneId);
        my.item.attendees = my.model.attendees;

        my.model.isLoading = true;
        if (!my.item.id) {
            apiSvc.event.create(my.item.spaceId, my.item).then(newItem => {
                $uibModalInstance.close(newItem);
            }).catch(onError);
        } else {
            apiSvc.event.update(my.item.id, my.item).then(updatedItem => {
                $uibModalInstance.close(updatedItem);
            }).catch(onError);
        }
    };

    my.cancel = function () {
        $uibModalInstance.close();
    };

    my.delete = function () {
        my.model.isLoading = true;
        popupSvc.confirm('Вы уверены, что хотите удалить эту встречу безвозвратно?', 'Удаление встречи', 'Да, удалить').then(() => {
            return apiSvc.event.remove(my.item.id).then(() => {
                $uibModalInstance.close('delete');
            }).catch(onError);
        });
    };

    function getISODateTime(date, time, timezoneId) {
        if (!date || !time || !timezoneId) return null;

        const tt = time.split(':')
        const dt = {
            year: date.getFullYear(),
            month: date.getMonth(),
            day: date.getDate(),
            hour: parseInt(tt[0]),
            minute: parseInt(tt[1])
        };

        const isoWithTz = moment.tz(dt, timezoneId).format();
        const iso = new Date(isoWithTz).toISOString();

        return iso;
    };

    my.getUsers = function (searchValue) {
        return apiSvc.user.search(searchValue).then(res => {
            if (!res || !res.length) {
                return [{
                    displayName: searchValue,
                    user: {
                        email: searchValue
                    }
                }];
            }

            return res.map(x => ({
                displayName: `${x.fullName} (${x.email})`,
                user: x
            }));
        });
    };

    function isEmail(str) {
        if (!str) return false;
        const ps = str.split('@');
        return ps.length === 2 && !!ps[0] && !!ps[1];
    };

    my.addAttendee = function (newAttendee, $event) {
        if (newAttendee.user.id || isEmail(newAttendee.user.email)) {
            if (!my.model.attendees.some(x => x.email === newAttendee.user.email)) {
                my.model.attendees.push({
                    userId: newAttendee.user.id,
                    fullName: newAttendee.user.fullName,
                    email: newAttendee.user.email,
                    imageUrl: newAttendee.user.imageUrl
                });
                my.model.organizerIsAttendee = !!my.model.attendees.some(x => x.email === my.item.organizerEmail);
            }
            my.model.newAttendeeText = '';
            $event.stopPropagation();
        } else {
            my.model.newAttendeeText = $event.currentTarget.value;
            $event.preventDefault();
            $event.stopPropagation();
        }
    };

    my.removeAttendee = function (x) {
        const index = my.model.attendees.indexOf(x);
        my.model.attendees.splice(index, 1);
        my.model.organizerIsAttendee = !!my.model.attendees.some(x => x.email === my.item.organizerEmail);
    };

    my.setOrganizer = function (x, $event) {
        if (x.user.id || isEmail(x.user.email)) {
            my.item.organizerEmail = x.user.email;
            my.item.organizerFullName = x.user.fullName;
            my.item.organizerUserId = x.user.id;
            my.item.organizerImageUrl = x.user.imageUrl;
            my.model.newOrganizerText = '';
            my.model.organizerIsAttendee = !!my.model.attendees.some(x => x.email === my.item.organizerEmail);
            $event.stopPropagation();
        } else {
            my.model.newOrganizerText = $event.currentTarget.value;
            $event.preventDefault();
            $event.stopPropagation();
        }
    };

    my.onOrganizerIsAttendeeChange = function (isAttendee) {
        if (isAttendee) {
            const index = my.model.attendees.findIndex(x => x.email === my.item.organizerEmail);
            if (index < 0 && isEmail(my.item.organizerEmail)) {
                my.model.attendees.push({
                    userId: my.item.organizerUserId,
                    fullName: my.item.organizerFullName,
                    email: my.item.organizerEmail,
                    imageUrl: my.item.organizerImageUrl
                });
            }
        } else {
            const index = my.model.attendees.findIndex(x => x.email === my.item.organizerEmail);
            if (index > -1) my.model.attendees.splice(index, 1);
        }
    };

    my.setTimes = function (opened, date, addNow) {
        if (!opened) return;

        let times = [];
        const start = new Date(1970, 0, 1);
        const end = new Date(1970, 0, 2);
        const stepInMinutes = 30;
        for (let t = start; t < end; t.setMinutes(t.getMinutes() + stepInMinutes)) {
            const time = moment(t).format('HH:mm');
            times.push({ value: time, name: time });
        }

        const isToday = moment(date).format('YYYY-MM-DD') === moment().format('YYYY-MM-DD');
        if (isToday) {
            const nowTime = moment().format('HH:mm');
            times = times.filter(x => x.value > nowTime);
            if (addNow) {
                times.unshift({ value: nowTime, name: `${nowTime} (сейчас)` });
            }
        } else {
            times.find(x => x.value === '09:00').scrollTo = true;
        }

        my.model.times = times;
    };

    // Resources

    function openResourcesPopup() {
        return $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: resourcesPopupTmpl,
            controller: resourcesPopupCtrl,
            controllerAs: 'my',
            windowClass: 'window-resources',
            resolve: {
                item: () => my.item,
                isReadOnly: () => isReadOnly
            }
        }).result;
    };

    my.showResources = function () {
        openResourcesPopup();
    };
}];

export default ctrl;
