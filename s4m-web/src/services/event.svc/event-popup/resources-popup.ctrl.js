const ctrl = ['$uibModalInstance', 'item', 'apiSvc', 'popupSvc', 'isReadOnly', function ($uibModalInstance, item, apiSvc, popupSvc, isReadOnly) {
    const my = this;

    my.item = item;

    my.model = {
        isReadOnly: isReadOnly,
        resourceCatalog: null,
        resources: [],
        dict: {
            resources: {}
        },
        isCatalogLoaded: false,
        newCount: 0
    };

    function init() {
        if (my.item.resources) {
            my.model.resources = JSON.parse(JSON.stringify(my.item.resources));
            my.model.newCount = my.model.resources.filter(x => x.isNew).length;
        }

        apiSvc.resource.getList().then(res => {
            my.model.resourceCatalog = res || [];

            my.model.resourceCatalog.forEach(x => {
                my.model.dict.resources[x.code] = x;
            });

            my.model.isCatalogLoaded = true;
        });
    };

    init();

    my.ok = function () {
        if (my.item.id) {
            apiSvc.event.saveResources(my.item.id, my.model.resources).then(() => {
                my.model.resources.forEach(x => x.isNew = undefined);
                my.item.resources = my.model.resources;
                popupSvc.alert('Зарегистрирован заказ на обслуживание встречи', 'Заказ зарегистрирован');
                $uibModalInstance.close();
            }).catch(err => {
                popupSvc.error('Не удалось сделать заказ');
            });
        } else { // New event - resources will be saved with event
            my.item.resources = my.model.resources;
            $uibModalInstance.close();
        }
        
    };

    my.cancel = function () {
        $uibModalInstance.close();
    };

    my.getImageUrl = function (resourceCode) {
        const res = my.model.dict.resources[resourceCode];
        return res ? res.imageUrl : null;
    };

    my.addResource = function (x) {
        if (isReadOnly) return;
        const r = JSON.parse(JSON.stringify(x));
        my.model.resources.unshift({
            name: r.name,
            code: r.code,
            attributes: r.attributes,
            isNew: true
        });
        my.model.newCount += 1;
    };

    my.removeResource = function (x) {
        const index = my.model.resources.indexOf(x);
        if (index > -1) {
            my.model.resources.splice(index, 1);
        }

        my.model.newCount -= 1;
    };
}];

export default ctrl;