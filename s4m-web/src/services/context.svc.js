var svc = ['apiSvc', '$q', '$routeParams', '$rootScope', function (apiSvc, $q, $routeParams, $rootScope) {
    const COOKIENAME = {
        currentCompanyId: 'currentCompanyId'
    };

    const context = {
        companies: [],
        currentCompany: null,
        currentUser: null,
        currentUserRoles: null,
    };

    let rolesMap = {};

    function loadCompanyList() {
        return apiSvc.organization.getList().then(resp => {
            context.companies = resp;
            return resp;
        });
    };

    function getCompanyList() {
        if (context.companies.length) return $q.resolve(context.companies);

        return loadCompanyList();
    };

    function getCurrentCompanyId() {
        return Number($routeParams.companyId) || null;
    };

    function getCurrentCompany() {
        const id = getCurrentCompanyId();
        if (!id) return $q.resolve(null);

        if (context.currentCompany && context.currentCompany.id === id) return $q.resolve(context.currentCompany);

        return getCompanyList().then(list => {
            if (!list || !list.length) return null;

            const res = list.filter(x => x.id === id);
            if (!res.length) return null;

            context.currentCompany = res[0];
            return context.currentCompany;
        });
    };

    function refresh() {
        return loadCompanyList().then(res => {
            context.currentCompany = null;
            $rootScope.$broadcast('contextRefresh');
            return res;
        });
    };

    function setCurrentUser(user) {
        context.currentUser = user;
    };

    function getCurrentUser() {
        return context.currentUser;
    };

    function setCurrentUserRoles(roles) {
        context.currentUserRoles = roles;
        rolesMap = {};
        if (roles && roles.length) {
            for(let r of roles ) {
                const key = r.role + (!r.location ? '' : '/' + r.location);
                rolesMap[key] = true;
            }
        }
    };

    function getCurrentUserRoles() {
        return context.currentUserRoles;
    };

    function userIs(role, locationId) {
        const key = role + (!locationId ? '' : '/' + locationId);
        return rolesMap[key];
    }

    this.getCompanyList = getCompanyList;
    this.getCurrentCompanyId = getCurrentCompanyId;
    this.getCurrentCompany = getCurrentCompany;
    this.refresh = refresh;
    this.getCurrentUser = getCurrentUser;
    this.setCurrentUser = setCurrentUser;
    this.getCurrentUserRoles = getCurrentUserRoles;
    this.setCurrentUserRoles = setCurrentUserRoles;
    this.userIs = userIs;
}];

export default svc;
