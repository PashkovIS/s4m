import angular from 'angular';

import apiSvc from './api.svc';
import authSvc from './auth.svc';
import popupSvc from './popup.svc';
import contextSvc from './context.svc';
import eventSvc from './event.svc';

export default angular.module('s4m-web.svcs', [])
    .service('apiSvc', apiSvc)
    .service('authSvc', authSvc)
    .service('popupSvc', popupSvc)
    .service('contextSvc', contextSvc)
    .service('eventSvc', eventSvc);
