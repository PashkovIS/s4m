import angular from 'angular';

import scrollTo from './scrollTo';
import toNumber from './toNumber';
import toTime from './toTime';

export default angular.module('s4m-web.dirs', [])
    .directive('scrollTo', scrollTo)
    .directive('toNumber', toNumber)
    .directive('toTime', toTime);
