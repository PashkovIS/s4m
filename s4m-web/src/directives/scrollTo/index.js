const dir = function () {
    return function (scope, element, attrs) {
        scope.$watch(attrs.scrollTo, function (value) {
            if (value) {
                var pos = $(element).position().top + $(element).parent().scrollTop();
                $(element).parent().scrollTop(pos);
            }
        });
    }
};

export default dir;
