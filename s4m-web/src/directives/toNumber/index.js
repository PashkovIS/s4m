const dir = function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                if (val === null || val === undefined || val === '' || val === '-') return null;
                return parseFloat(val);
            });
            ngModel.$formatters.push(function (val) {
                if (val === null || val === undefined || val === '') return '';
                return String(val);
            });
        }
    };
};

export default dir;
