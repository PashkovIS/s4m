import moment from 'moment';

export default function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            $(element).blur(function () {
                const formatedValue = ngModel ? format(ngModel.$modelValue) : '';
                ngModel.$setViewValue(formatedValue);
                ngModel.$render();
            });

            /*
            Parse rules:
            '1' => '01:00'
            '12' => '12:00'
            '123' => '01:23'
            '1234' => '12:34'
            */
            function parse(val) {
                if (ngModel.$isEmpty(val)) return null;
                if (val.length < 3) val += '00';
                if (val.length < 4) val = '0' + val;
                const m = moment(val, 'HH:mm');
                return m.isValid() ? m.format('HH:mm') : null;
            };

            function format(val) {
                if (ngModel.$isEmpty(val)) return '';
                const m = moment(val, 'HH:mm');
                return m.isValid() ? m.format('HH:mm') : '';
            };

            ngModel.$parsers.push(parse);
            ngModel.$formatters.push(format);
        }
    };
};
