import tmpl from './c-top-links-company.html';
import ctrl from './cTopLinksCompany.ctrl';

const cmp = function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
        },
        controller: ctrl,
        controllerAs: 'my',
        template: tmpl
    };
};

export default cmp;
