const ctrl = ['contextSvc', '$location', '$routeParams', '$route', function (contextSvc, $location, $routeParams, $route) {
    const my = this;

    my.model = {
        companies: null,
        currentCompany: null,
        companyPlaceholder: 'Выберите компанию',
        showCreate: ($location.path() !== '/create-company' && contextSvc.userIs('admin'))
    };

    function init() {
        contextSvc.getCompanyList().then(resp => {
            my.model.companies = resp;
        });
    };

    init();

    my.addItem = function () {
        $location.path('/create-company');
    };

    my.changeCompany = function (companyId) {
        if (!$routeParams.companyId) {
            $location.path(`/c/${companyId}`);
        } else {
            $route.updateParams({ companyId: companyId });
        }
    };
}];

export default ctrl;
