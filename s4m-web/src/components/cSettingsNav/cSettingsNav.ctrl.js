const ctrl = ['$scope', '$routeParams', '$location', function ($scope, $routeParams, $location) {
    const my = this;

    my.model = {
        pages: []
    };

    function init() {
        const id = $routeParams.companyId;
        if (id) {
            my.model.pages = [
                { location: `/c/${$routeParams.companyId}/settings`, title: 'Компания' },
                { location: `/c/${$routeParams.companyId}/settings/locations`, title: 'Расположения' },
                { location: `/c/${$routeParams.companyId}/settings/spaces`, title: 'Переговорные' },
                { location: `/c/${$routeParams.companyId}/settings/users`, title: 'Пользователи' },
                { location: `/c/${$routeParams.companyId}/settings/resources`, title: 'Ресурсы' },
                { location: `/c/${$routeParams.companyId}/settings/space-types`, title: 'Типы пространств' }
            ];
        } else {
            my.model.pages = [
                { location: `/settings/users`, title: 'Пользователи' },
                { location: `/settings/resources`, title: 'Ресурсы' },
                { location: `/settings/space-types`, title: 'Типы пространств' }
            ];
        }
    };

    init();

    $scope.$on('$routeChangeSuccess', function () {
        const path = $location.path();
        for (let p of my.model.pages) {
            p.isActive = (path === p.location);
        }
    });
}];

export default ctrl;
