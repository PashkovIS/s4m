import tmpl from './c-settings-nav.html';
import ctrl from './cSettingsNav.ctrl';

const cmp = function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
        },
        controller: ctrl,
        controllerAs: 'my',
        template: tmpl
    };
};

export default cmp;
