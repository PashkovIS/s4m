import tmpl from './c-top-links-user.html';
import ctrl from './cTopLinksUser.ctrl';

const cmp = function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
        },
        controller: ctrl,
        controllerAs: 'my',
        template: tmpl
    };
};

export default cmp;
