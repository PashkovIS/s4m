const ctrl = ['$scope', 'contextSvc', 'authSvc', '$location', function ($scope, contextSvc, authSvc, $location) {
    const my = this;
    my.companyId = contextSvc.getCurrentCompanyId();
    my.showSettings = contextSvc.userIs('admin');

    $scope.$on('$routeChangeSuccess', function () {
        my.companyId = contextSvc.getCurrentCompanyId();
    });

    my.logout = function () {
        authSvc.logout().then(() => {
            $location.path("/login");
        });
    };
}];

export default ctrl;
