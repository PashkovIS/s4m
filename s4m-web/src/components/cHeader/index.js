import tmpl from './c-header.html';
import ctrl from './cHeader.ctrl';

const cmp = function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
        },
        controller: ctrl,
        controllerAs: 'my',
        template: tmpl
    };
};

export default cmp;
