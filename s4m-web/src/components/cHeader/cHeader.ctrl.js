const ctrl = ['$scope', 'contextSvc', '$location', function ($scope, contextSvc, $location) {
    const my = this;
    my.model = {
        companyId: contextSvc.getCurrentCompanyId(),
        company: null,
        pages: null
    };

    function init() {
        my.model.pages = [];
        if (my.model.companyId) {
            my.model.pages.push({ location: `/c/${my.model.companyId}`, title: 'Переговорные' });
            my.model.pages.push({ location: `/c/${my.model.companyId}/scheduler`, title: 'Расписание' });
            my.model.pages.push({ location: `/c/${my.model.companyId}/events`, title: 'Встречи' });
            my.model.pages.push({ location: `/c/${my.model.companyId}/calendar`, title: 'Ваш календарь' });   
        }

        const path = $location.path();
        for(let p of my.model.pages) {
            p.isActive = (path === p.location);
        }

        contextSvc.getCurrentCompany().then(res => {
            my.model.company = res;
        });
    };

    init();

    $scope.$on('$routeChangeSuccess', function () {
        my.model.companyId = contextSvc.getCurrentCompanyId();
        init();
    });

    $scope.$on('contextRefresh', function () {
        my.model.companyId = contextSvc.getCurrentCompanyId();
        init();
    });
}];

export default ctrl;
