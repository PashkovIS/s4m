import angular from 'angular';

import cHeader from './cHeader';
import cSettingsNav from './cSettingsNav';
import cTopLinksCompany from './cTopLinksCompany';
import cTopLinksUser from './cTopLinksUser';

export default angular.module('s4m-web.cmps', [])
    .directive('cHeader', cHeader)
    .directive('cSettingsNav', cSettingsNav)
    .directive('cTopLinksCompany', cTopLinksCompany)
    .directive('cTopLinksUser', cTopLinksUser);
