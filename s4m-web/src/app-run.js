import calendarEventPopoverTmpl from './templates/calendar/popover/event.html';
import eventsSpacePopoverTmpl from './templates/events/popover/space.html';

const appRun = ['$rootScope', '$location', 'authSvc', '$q', 'contextSvc', '$templateCache', function ($rootScope, $location, authSvc, $q, contextSvc, $templateCache) {
    $templateCache.put('templates/calendar/popover/event.html', calendarEventPopoverTmpl);
    $templateCache.put('templates/events/popover/space.html', eventsSpacePopoverTmpl);

    $rootScope.$on("$routeChangeStart", function (event, to, from) {
        if (!!to.params.companyId && !!from && from.params.companyId && to.params.companyId !== from.params.companyId && to.$$route.fallbackUrl) {
            const urlTmpl = to.$$route.fallbackUrl;
            const newPath = urlTmpl.replace(':companyId', to.params.companyId);
            $location.path(newPath);
            return;
        }

        if (!to.anonymous) {
            to.resolve = to.resolve || {};
            if (!to.resolve.authorizationResolver) {
                to.resolve.authorizationResolver = function () {
                    return authSvc.check().then(res => {
                        if (!res.authorized) {
                            contextSvc.setCurrentUser(null);
                            contextSvc.setCurrentUserRoles(null);
                            throw { unauthorized: true };
                        } else {
                            contextSvc.setCurrentUser(res.user);
                            contextSvc.setCurrentUserRoles(res.roles);

                            if (to.accessRole === 'admin') {
                                if(!contextSvc.userIs(to.accessRole)) {
                                    console.log(`Current user has not enough privileges to view ${$location.path()}`);
                                    $location.path("/")
                                }
                            }
                        }
                    }).catch(err => {
                        throw err;
                    });
                };
            }
        }
    });

    $rootScope.$on("$routeChangeError", function (event, to, from, error) {
        if (error.unauthorized) {
            if (to.originalPath !== '/') {
                let originalPath = to.originalPath;
                Object.keys(to.params).forEach(x => {
                    originalPath = originalPath.replace(new RegExp(`:${x}$`), to.params[x]).replace(new RegExp(`:${x}\/`, 'g'), to.params[x] + '/');
                });
                
                $location.path("/login").search("returnTo", originalPath);
            } else {
                $location.path("/login");
            }
        }
    });
}];

export default appRun;
