import routeHome from './routes/home';
import routeLogin from './routes/login';
import routeCreateCompany from './routes/create-company';
import routeProfile from './routes/profile';
import routeSettingsCompany from './routes/settings-company';
import routeSettingsSpaces from './routes/settings-spaces';
import routeSettingsSpace from './routes/settings-space';
import routeSettingsLocations from './routes/settings-locations';
import routeSettingsLocation from './routes/settings-location';
import routeSettingsResources from './routes/settings-resources';
import routeSettingsUsers from './routes/settings-users';
import routeSettingsUser from './routes/settings-user';
import routeSettingsSpaceTypes from './routes/settings-space-types';
import routeEvents from './routes/events';
import routeCalendar from './routes/calendar';
import routeScheduler from './routes/scheduler';
import routeSpaces from './routes/spaces';

const appConfig = ['$routeProvider', '$locationProvider', 'uibDatepickerConfig', function ($routeProvider, $locationProvider, uibDatepickerConfig) {
    $routeProvider
        .when('/', routeHome)
        .when('/login', routeLogin)
        .when('/create-company', routeCreateCompany)
        .when('/profile', routeProfile)
        .when('/settings/users', routeSettingsUsers)
        .when('/settings/users/:userId', routeSettingsUser)
        .when('/settings/resources', routeSettingsResources)
        .when('/settings/space-types', routeSettingsSpaceTypes)
        .when('/c/:companyId', routeSpaces)
        .when('/c/:companyId/settings', routeSettingsCompany)
        .when('/c/:companyId/settings/spaces', routeSettingsSpaces)
        .when('/c/:companyId/settings/spaces/:spaceId', routeSettingsSpace)
        .when('/c/:companyId/settings/locations', routeSettingsLocations)
        .when('/c/:companyId/settings/locations/:locationId', routeSettingsLocation)
        .when('/c/:companyId/settings/resources', routeSettingsResources)
        .when('/c/:companyId/settings/users', routeSettingsUsers)
        .when('/c/:companyId/settings/users/:userId', routeSettingsUser)
        .when('/c/:companyId/settings/space-types', routeSettingsSpaceTypes)
        .when('/c/:companyId/events', routeEvents)
        .when('/c/:companyId/calendar', routeCalendar)
        .when('/c/:companyId/scheduler', routeScheduler)
        .otherwise({ redirectTo: '/' });

    $locationProvider.html5Mode(true);
    uibDatepickerConfig.showWeeks = false;
    uibDatepickerConfig.startingDay = 1;
}];

export default appConfig;
