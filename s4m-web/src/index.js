import angular from 'angular';
import ngRoute from 'angular-route';
import ngCookies from 'angular-cookies';
import ngSanitize from 'angular-sanitize';

import 'jquery';
import 'bootstrap-loader/extractStyles';
import 'angular-ui-bootstrap';
import 'font-awesome/scss/font-awesome.scss';
import 'angular-i18n/angular-locale_ru-ru';

import '../node_modules/fullcalendar/dist/fullcalendar.css'
import '../node_modules/fullcalendar-scheduler/dist/scheduler.css'
import 'fullcalendar'
import 'fullcalendar-scheduler'
import 'angular-ui-calendar';

import appConfig from './app-config';
import appRun from './app-run';
import './components';
import './directives';
import './services';
import './styles/styles.scss'

export const module = angular.module('s4m-web', [
    ngRoute,
    ngCookies,
    ngSanitize,
    'ui.calendar',
    'ui.bootstrap',
    's4m-web.cmps',
    's4m-web.dirs',
    's4m-web.svcs'
]).config(appConfig).run(appRun);
