const NODE_ENV = process.env.NODE_ENV || "development";
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");

const PATHS = {
  source: path.resolve(__dirname, "./src"),
  dist: path.resolve(__dirname, "./dist"),
  output: path.resolve(__dirname, "./dist/assets"),
  publicPath: "/assets/",
  indexHtmlTemplate: path.resolve(__dirname, "./src/index.ejs")
};

module.exports = {
  context: PATHS.source,
  entry: {
    main: "./index"
  },
  output: {
    path: PATHS.output,
    publicPath: PATHS.publicPath,
    filename: "app.js"
  },
  watchOptions: {
    aggregateTimeout: 100
  },
  devtool: NODE_ENV === "development" ? "sourcemap" : null,
  plugins: [
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV)
    }),
    new webpack.NoErrorsPlugin(),
    new ExtractTextPlugin("styles.css"),
    new HtmlWebpackPlugin({
      filename: "../index.html",
      title: "Auro Room",
      hash: true,
      template: PATHS.indexHtmlTemplate
    }),
    new webpack.ProvidePlugin({
      jQuery: "jquery",
      $: "jquery",
      jquery: "jquery",
      "window.jQuery": "jquery",
      moment: "moment"
    })
  ],
  resolveLoader: {
    modulesDirectories: ["node_modules"],
    extensions: ["", ".js"]
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: ["ng-annotate", "babel?presets[]=es2015"],
        include: PATHS.source
      },
      {
        test: /\.html$/,
        loader: "ng-cache?prefix=[dir]/[dir]",
        include: PATHS.source
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract([
          "css?sourceMap", //&modules&camelCase&localIdentName=[name]__[local]___[hash:base64:5]
          "sass?sourceMap"
        ])
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract(["css?sourceMap"])
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=10000&minetype=application/font-woff"
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader"
      },
      { test: /bootstrap\/dist\/js\/umd\//, loader: "imports?jQuery=jquery" },
      {
        test: /(\.jpg|\.png)$/,
        loader: "file?name=[path][name].[ext]"
      },
      { test: /\.json$/, loader: "json-loader" }
    ]
  },
  devServer: {
    port: 9090,
    historyApiFallback: true,
    contentBase: PATHS.dist,
    proxy: {
      "/api/**": {
        target: "http://localhost:30001",
        secure: false,
        pathRewrite: { "^/api": "" }
      },
      "/content/**": {
        target: "http://localhost:30001",
        secure: false
      }
    }
  }
};

if (NODE_ENV === "production") {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true,
        unsafe: true
      }
    })
  );
}
