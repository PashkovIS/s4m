# S4M UI

## Description

This is Portal UI for Auro Room - service for office meeting planning

## How to build and run Docker Image

`docker build -t s4m/ui .`  
`docker run -p 80:80 s4m/ui`
